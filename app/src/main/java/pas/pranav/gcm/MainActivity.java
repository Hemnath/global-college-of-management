package pas.pranav.gcm;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.google.android.gcm.GCMRegistrar;

import java.util.ArrayList;
import java.util.List;

import pas.pranav.gcm.admin.AdminMainActivity;
import pas.pranav.gcm.teacher.TeacherMainActivity;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    Button btnFacebook;
    Button btnTwitter;
    Button btnYoutube;

    private FloatingActionButton fab1;
    private FloatingActionButton fab2;
    private FloatingActionButton fab3;

    private List<FloatingActionMenu> menus = new ArrayList<>();
    private Handler mUiHandler = new Handler();

    android.os.AsyncTask<Void, Void, Void> mRegisterTask;
    pas.pranav.gcm.Controller aController;
    private Button btninstagram;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(pas.pranav.gcm.R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(pas.pranav.gcm.R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(pas.pranav.gcm.R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                pas.pranav.gcm.R.string.navigation_drawer_open,
                pas.pranav.gcm.R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(pas.pranav.gcm.R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        FloatingActionMenu menuDown = (FloatingActionMenu) findViewById(R.id.menu_down);

        Button btnAbout = (Button) findViewById(pas.pranav.gcm.R.id.btnAbout);
        Button btnPrograms = (Button) findViewById(pas.pranav.gcm.R.id.btnProgram);
        Button btnPublication = (Button) findViewById(pas.pranav.gcm.R.id.btnPublication);
        Button btnContact = (Button) findViewById(pas.pranav.gcm.R.id.btnNewsEvents);

        btnFacebook = (Button) findViewById(pas.pranav.gcm.R.id.btnFacebook);
        btnTwitter = (Button) findViewById(pas.pranav.gcm.R.id.btnTwitter);
        btnYoutube = (Button) findViewById(pas.pranav.gcm.R.id.btnYoutube);
        btninstagram = (Button) findViewById(R.id.btninstagram);

        menus.add(menuDown);
        menuDown.hideMenuButton(false);
        int delay = 400;
        for (final FloatingActionMenu menu : menus) {
            mUiHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    menu.showMenuButton(true);
                }
            }, delay);
            delay += 150;
        }

        fab1 = (FloatingActionButton) findViewById(R.id.btnFab1);
        fab2 = (FloatingActionButton) findViewById(R.id.btnFab2);
        fab3 = (FloatingActionButton) findViewById(R.id.btnFab3);
        fab1.setOnClickListener(clickListener);
        fab2.setOnClickListener(clickListener);
        fab3.setOnClickListener(clickListener);

        menuDown.setClosedOnTouchOutside(true);
        btnAbout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this, About.class);
                Bundle bundle = new Bundle();
                bundle.putString("uid", "");
                bundle.putString("token", "");
                bundle.putString("userType", "");
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
        btnPrograms.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(pas.pranav.gcm.MainActivity.this, pas.pranav.gcm.Programs_List.class);
                startActivity(intent);
            }
        });
        btnPublication.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, pas.pranav.gcm.Publications.class));
            }
        });
        btnContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, pas.pranav.gcm.News_Events.class));
            }
        });
        btnYoutube.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, YouTube.class));
            }
        });
        btnFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, Facebook.class));
            }
        });
        btnTwitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, Twitter.class));
            }
        });
        btninstagram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("https://www.instagram.com/globalcollegeofmanagement/"));
                startActivity(i);
            }
        });

        aController = new Controller();
        GCMRegistrar.checkDevice(this);
        GCMRegistrar.checkManifest(this);
        registerReceiver(mHandleMessageReceiver,
                new android.content.IntentFilter(pas.pranav.gcm.Config.DISPLAY_MESSAGE_ACTION));
        final String regId = GCMRegistrar.getRegistrationId(this);
        System.out.println(" printing regId: " + regId);
        if (regId.equals("")) {
            GCMRegistrar.register(this, pas.pranav.gcm.Config.GOOGLE_SENDER_ID);
        } else {
            if (GCMRegistrar.isRegisteredOnServer(this)) {
                return;
            } else {
                final android.content.Context context = this;
                mRegisterTask = new android.os.AsyncTask<Void, Void, Void>() {
                    @Override
                    protected Void doInBackground(Void... params) {
                        aController.register(context, regId);
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void result) {
                        mRegisterTask = null;
                    }
                };
                // execute AsyncTask
                mRegisterTask.execute(null, null, null);
            }

        }
    }

    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String text = "";
            Intent intent = null;
            switch (v.getId()) {
                case R.id.btnFab1:
                    intent = new Intent(MainActivity.this, Map.class);
                    startActivity(intent);
                    break;
                case R.id.btnFab2:
                    intent = new Intent(MainActivity.this, Website.class);
                    startActivity(intent);
                    break;
                case R.id.btnFab3:
                    intent = new Intent(MainActivity.this, Contact.class);
                    startActivity(intent);
                    break;
            }

        }
    };

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(pas.pranav.gcm.R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            Intent launchNextActivity = new Intent(MainActivity.this, StartScreen.class);
            launchNextActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            launchNextActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            launchNextActivity.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            launchNextActivity.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(launchNextActivity);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(pas.pranav.gcm.R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == pas.pranav.gcm.R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == pas.pranav.gcm.R.id.nav_home) {
            startActivity(new android.content.Intent(MainActivity.this, MainActivity.class));
        } else if (id == pas.pranav.gcm.R.id.nav_notice) {
            Intent intent = new Intent(MainActivity.this, Notifications.class);
            startActivity(intent);
        }  else if (id == pas.pranav.gcm.R.id.nav_login) {
            Intent intent = new Intent(MainActivity.this,MIS.class);
            startActivity(intent);
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(pas.pranav.gcm.R.id.action_settings);
        MenuItem item1 = menu.findItem(R.id.action_signOut);
        item.setVisible(false);
        item1.setVisible(false);
        return true;
    }

    // Create a broadcast receiver to get message and show on screen
    private final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(android.content.Context context, Intent intent) {
            String newMessage = intent.getExtras().getString(Config.EXTRA_MESSAGE);
            // Waking up mobile if it is sleeping
            aController.acquireWakeLock(getApplicationContext());
            // Display message on the screen
            //lblMessage.append(newMessage + "\n");
            // Toast.makeText(getApplicationContext(), "Got Message: " + newMessage, Toast.LENGTH_LONG).show();
            // Releasing wake lock
            aController.releaseWakeLock();
        }
    };

    @Override
    protected void onDestroy() {
        // Cancel AsyncTask
        if (mRegisterTask != null) {
            mRegisterTask.cancel(true);
        }
        try {
            // Unregister Broadcast Receiver
            unregisterReceiver(mHandleMessageReceiver);
            // Clear internal resources.
            GCMRegistrar.onDestroy(this);

        } catch (Exception e) {
            android.util.Log.e("UnRegister Receiver", "> " + e.getMessage());
        }
        super.onDestroy();
    }

}
















