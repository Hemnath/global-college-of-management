package pas.pranav.gcm;

import android.content.Intent;
import android.support.v7.widget.Toolbar;
import android.view.View;

public class Website extends android.support.v7.app.AppCompatActivity {
    android.webkit.WebView webView;

    String website_url = "http://globalcollege.edu.np";
    @Override protected void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(pas.pranav.gcm.R.layout.web_view);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Website.this, MainActivity.class));
            }
        });

        webView = (android.webkit.WebView) findViewById(pas.pranav.gcm.R.id.webView);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setSupportZoom(true);
        webView.setWebViewClient(new android.webkit.WebViewClient());
        webView.loadUrl(website_url);


    }

    @Override
    public void onBackPressed() {
        if (webView.canGoBack()) {
            webView.goBack();
        } else {
            Intent launchNextActivity = new Intent(Website.this, MainActivity.class);
            launchNextActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            launchNextActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            launchNextActivity.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(launchNextActivity);
        }
    }

}

