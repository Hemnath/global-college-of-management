package pas.pranav.gcm.teacher;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import pas.pranav.gcm.Attendance_Calendar;
import pas.pranav.gcm.CalendarView;
import pas.pranav.gcm.MIS;
import pas.pranav.gcm.MainActivity;
import pas.pranav.gcm.R;
import pas.pranav.gcm.helpers.API_URL;
import pas.pranav.gcm.helpers.JSONParserCustom;
import pas.pranav.gcm.helpers.MainGridViewAdapter;

public class TeacherMainActivity extends AppCompatActivity {

    private GridView gridview;
    MainGridViewAdapter gr;

    Intent myIntent;
    private String userType;
    private String deviceId;
    String token;

    public static final String strUserName = "userKey";
    public static final String strPassword = "passwordKey";
    public static final String strToken = "token";
    public static final String strDeviceId = "deviceId";

    SharedPreferences.Editor editor;
    SharedPreferences sharedpreferences;
    private TextView tvId;
    private ImageView ivLogo;

    private API_URL appURL;
    private String url;
    private int intUserType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        sharedpreferences = getSharedPreferences(MIS.MyPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();

        deviceId = sharedpreferences.getString(strDeviceId, null);
        token = sharedpreferences.getString(strToken, null);

        tvId = (TextView) findViewById(pas.pranav.gcm.R.id.tvRecord);
        ivLogo = (ImageView) findViewById(pas.pranav.gcm.R.id.ivLogo);

        tvId.setText("Welcome " + sharedpreferences.getString(strUserName, null).replace("@globalcollege.edu.np", "").replace(".", " "));

        gridview = (GridView) findViewById(R.id.gridview);
        gr = new MainGridViewAdapter(this);
        gridview.setAdapter(gr);

        gr.addItem("Message", R.drawable.message);
        gr.addItem("Attendance", R.drawable.attendance);
        gr.addItem("Calendar", R.drawable.calender);
        gr.addItem("Notice board", R.drawable.notice_board);
        gr.addItem("Teacher Subject Class", R.drawable.teachersubject);

        appURL = new API_URL();
        url = appURL.ServerUrlTeacher + appURL.MyProfile;
        new jsonMyProfile().execute(url);

        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bundle bundle = new Bundle();
                switch (position) {
                    case 0:
                        myIntent = new Intent(TeacherMainActivity.this, TeacherMessage.class);
                        startActivity(myIntent);
                        break;
                    case 1:
                        myIntent = new Intent(TeacherMainActivity.this, Attendance_Calendar.class);
                        startActivity(myIntent);
                        break;
                    case 2:
                        myIntent = new Intent(TeacherMainActivity.this, CalendarView.class);
                        startActivity(myIntent);
                        break;
                    case 3:
                        myIntent = new Intent(TeacherMainActivity.this, TeacherNotice.class);
                        startActivity(myIntent);
                        break;
                    case 4:
                        myIntent = new Intent(TeacherMainActivity.this, TeacherSubject.class);
                        startActivity(myIntent);
                        break;
                }
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(pas.pranav.gcm.R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(pas.pranav.gcm.R.id.action_settings);
        item.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_signOut) {
            editor.clear();
            editor.commit();
            myIntent = new Intent(this, MIS.class);
            startActivity(myIntent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        myIntent = new Intent(this, MainActivity.class);
        startActivity(myIntent);
    }

    private class jsonMyProfile extends android.os.AsyncTask<String, String, org.json.JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected org.json.JSONObject doInBackground(String... args) {

            JSONParserCustom jParser = new JSONParserCustom();
            org.json.JSONObject json = jParser.getJSONFromUrl(url, deviceId, token);

            return json;
        }

        @Override
        protected void onPostExecute(org.json.JSONObject json) {
            try {
                String jsonCard = json.optString("MyProfile");

                JSONObject jsonObject = new JSONObject(jsonCard);

                String Name = jsonObject.optString("Name").toString();
                String StudentImageUrl = jsonObject.optString("PhotoUrl").toString();

                StudentImageUrl =  StudentImageUrl.replace(" ","%20");
                tvId.setText("Welcome " + Name);

                if (!StudentImageUrl.equalsIgnoreCase("") || !StudentImageUrl.isEmpty()) {
                    Picasso.get().load(StudentImageUrl)
                            .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                            .error(R.drawable.gcm_logo)
                            .into(ivLogo);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

}
