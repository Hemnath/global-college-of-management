package pas.pranav.gcm.teacher;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import pas.pranav.gcm.R;

/**
 * Created by ik890 on 7/10/2016.
 */
public class TeacherMessageItem extends AppCompatActivity {
    String c_date;
    String summary;
    String description;
    TextView txtC_Date, txtSummary, txtDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notice_message_item);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        android.content.Intent i = getIntent();
        c_date = i.getStringExtra("c_date");
        summary = i.getStringExtra("summary");
        description = i.getStringExtra("description");

        txtC_Date = (TextView) findViewById(R.id.tvDate);
        txtSummary = (TextView) findViewById(R.id.tvSummary);
        txtDescription = (TextView) findViewById(R.id.tvDescription);

        txtC_Date.setText(c_date);
        txtSummary.setText(android.text.Html.fromHtml(summary));
        txtDescription.setText(android.text.Html.fromHtml(description));

    }
}
