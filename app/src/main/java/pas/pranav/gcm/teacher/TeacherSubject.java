package pas.pranav.gcm.teacher;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.GridView;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import pas.pranav.gcm.MIS;
import pas.pranav.gcm.R;
import pas.pranav.gcm.helpers.API_URL;
import pas.pranav.gcm.helpers.JSONParserCustom;
import pas.pranav.gcm.helpers.MessageViewAdapter;
import pas.pranav.gcm.helpers.TeacherSubjectAdapter;

public class TeacherSubject extends AppCompatActivity {

    private GridView gridView;
    private TeacherSubjectAdapter teacherSubjectAdapter;

    private String url, deviceId, token;

    public static final String strUserName = "userKey";
    public static final String strPassword = "passwordKey";
    public static final String strToken = "token";
    public static final String strDeviceId = "deviceId";

    SharedPreferences.Editor editor;
    SharedPreferences sharedpreferences;

    private ProgressDialog progressBar;
    private API_URL appURL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher_subject);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sharedpreferences = getSharedPreferences(MIS.MyPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();

        deviceId= sharedpreferences.getString(strDeviceId, null);
        token = sharedpreferences.getString(strToken, null);

        gridView=(GridView) findViewById(R.id.itemGridView);
        teacherSubjectAdapter=new TeacherSubjectAdapter(this);
        gridView.setAdapter(teacherSubjectAdapter);

        appURL =new API_URL();
        url = appURL.ServerUrlTeacher+appURL.TeacherSubjectClass;

       new menuJSONParse().execute();

    }

    private class menuJSONParse extends android.os.AsyncTask<String, String, org.json.JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar = new ProgressDialog(TeacherSubject.this);
            progressBar.setMessage("Downloading...");
            progressBar.show();

        }

        @Override
        protected org.json.JSONObject doInBackground(String... args) {

            JSONParserCustom jParser = new JSONParserCustom();
            org.json.JSONObject json = jParser.getJSONFromUrl(url, deviceId, token);

            return json;
        }

        @Override
        protected void onPostExecute(org.json.JSONObject json) {
            String data = "";
            try {
                if (progressBar.isShowing()) {
                    progressBar.dismiss();
                }
                JSONArray jsonArray = json.optJSONArray("List");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    String academicYearTitle = jsonObject.optString("AcademicYearTitle").toString();
                    String classDates = jsonObject.optString("ClassDates").toString();
                    String classDays = jsonObject.optString("ClassDays").toString();
                    String classTimes = jsonObject.optString("ClassTimes").toString();
                    String courseTitle = jsonObject.optString("CourseTitle").toString();

                    String isCompleted = jsonObject.optString("IsCompleted").toString();
                    String partId = jsonObject.optString("PartId").toString();
                    String section = jsonObject.optString("Section").toString();
                    String shiftTitle = jsonObject.optString("ShiftTitle").toString();
                    String subjectTitle = jsonObject.optString("SubjectTitle").toString();
                    String yearAlias = jsonObject.optString("YearAlias").toString();
                    String yearId = jsonObject.optString("YearId").toString();

                  teacherSubjectAdapter.addItem(academicYearTitle, classDates, classDays, classTimes, courseTitle, isCompleted, partId, section, shiftTitle, subjectTitle, yearAlias, yearId);
                }
                teacherSubjectAdapter.notifyDataSetChanged();

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }
}
