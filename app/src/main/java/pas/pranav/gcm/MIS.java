package pas.pranav.gcm;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import pas.pranav.gcm.admin.AdminMainActivity;
import pas.pranav.gcm.helpers.API_URL;
import pas.pranav.gcm.helpers.CommonGetSet;
import pas.pranav.gcm.helpers.ServerString;
import pas.pranav.gcm.masters.Masters_Programs;
import pas.pranav.gcm.teacher.TeacherMainActivity;

public class MIS extends AppCompatActivity {

    AutoCompleteTextView etUsername;
    EditText etPassword;
    Button login;

    String username;
    String password;
    String userId = "";
    String tokenId = "";
    String userType = "";

    public static final String MyPREFERENCES = "prefs";
    SharedPreferences sharedpreferences;

    public static final String strUserName = "userKey";
    public static final String strPassword = "passwordKey";
    public static final String strToken = "token";
    public static final String strDeviceId = "deviceId";
    public static final String strDate = "tokenDate";


    android.content.SharedPreferences.Editor edit;

    pas.pranav.gcm.helpers.API_URL url;
    ProgressDialog progressBar;

    ServerString serverString;
    CommonGetSet getSet;
    String android_id;
    private Intent intent;
    private Date cDate;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(pas.pranav.gcm.R.layout.mis);
        Toolbar toolbar = (Toolbar) findViewById(pas.pranav.gcm.R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(pas.pranav.gcm.MIS.this, MainActivity.class));
            }
        });

        android_id = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);

        etUsername = (AutoCompleteTextView) findViewById(pas.pranav.gcm.R.id.etUser);
        etPassword = (EditText) findViewById(pas.pranav.gcm.R.id.etPasswd);
        login = (Button) findViewById(pas.pranav.gcm.R.id.btnLogin);

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        edit = sharedpreferences.edit();

        Calendar c = Calendar.getInstance();
        cDate = c.getTime();
        Log.e("HERE", "HERE: " + cDate.toString());

        url = new API_URL();

        if (sharedpreferences.contains(strToken)) {

            username = sharedpreferences.getString(strUserName, null);
            password = sharedpreferences.getString(strPassword, null);
            tokenId = sharedpreferences.getString(strToken, null);
            String UserType = sharedpreferences.getString("UserType", null);
            String tDate = sharedpreferences.getString(strDate, "Tue Aug 23 14:43:55 GMT+05:45 2015");

            int dateDfrce = dayDifference(cDate.toString(), tDate);
            if (isConnected()) {
                if (dateDfrce <= 27) {
                    int userTyp = Integer.parseInt(UserType);
                    if (userTyp == 1) {
                        intent = new Intent(MIS.this, AdminMainActivity.class);
                    } else if (userTyp == 2) {
                        intent = new Intent(MIS.this, Loggedin_Mis.class);
                    } else if (userTyp == 3) {
                        intent = new Intent(MIS.this, Loggedin_Mis.class);
                    } else if (userTyp == 4) {
                        intent = new Intent(MIS.this, TeacherMainActivity.class);
                    }
                    startActivity(intent);
                } else {
                    edit.clear();
                    edit.commit();
                    showAlertDialog(MIS.this, "Secure Login",
                            "Your Session has timed out. Please Login again to continue.", false);
                }
            } else {
                showAlertDialog(MIS.this, "No Internet Connection",
                        "You don't have internet connection.", false);
            }

        }

        login.setOnClickListener(loginListener);

    }

    int dayDifference(String CurrentDate, String FinalDate) {
        String dayDifference = "";
        try {
            Date date1;
            Date date2;
            SimpleDateFormat dates = new SimpleDateFormat("EEE MMM dd kk:mm:ss zzz yyyy");

            date1 = dates.parse(CurrentDate);
            date2 = dates.parse(FinalDate);

            long difference = Math.abs(date1.getTime() - date2.getTime());
            long differenceDates = difference / (24 * 60 * 60 * 1000);

            dayDifference = Long.toString(differenceDates);


        } catch (Exception exception) {
            Log.e("DIDN'T WORK", "exception " + exception);
        }
        return Integer.parseInt(dayDifference);
    }

    public View.OnClickListener loginListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Pattern pattern = Pattern.compile("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
            Matcher matcher = pattern.matcher(etUsername.getText());

            serverString = new ServerString();
            if (!(etUsername.getText().toString().replace(" ", "").equalsIgnoreCase(""))) {
                if (!(etPassword.getText().toString().replace(" ", "").equalsIgnoreCase(""))) {
                    serverString.setUsername(etUsername.getText().toString());
                    serverString.setPassword(etPassword.getText().toString());

                    if (isConnected()) {
                        processLogin();
                    } else {
                        showAlertDialog(MIS.this, "No Internet Connection",
                                "You don't have internet connection.", false);
                    }

                } else {
                    etPassword.setError("Password Empty");
                    etPassword.requestFocus();
                }
            } else {
                etUsername.setError("UserName Empty");
                etUsername.requestFocus();
            }
        }
    };

    public void processLogin() {
        progressBar = new ProgressDialog(MIS.this);
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressBar.setMessage("Authenticating...");
        progressBar.setProgress(0);
        progressBar.show();

        new HttpAsyncTask().execute(url.ServerUrlPublic + url.UserLogin);
    }

    @Override
    public void onBackPressed() {
        Intent launchNextActivity = new Intent(MIS.this, MainActivity.class);
        launchNextActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        launchNextActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        launchNextActivity.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(launchNextActivity);
    }

    boolean isConnected() {
        ConnectivityManager cm = (ConnectivityManager) this.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
        return isConnected;
    }

    public void showAlertDialog(Context context, String title, String message, Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setIcon((status) ? R.drawable.error : R.drawable.error);
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alertDialog.show();
    }

    public String POST(String url, ServerString serverString) {

        InputStream inputStream = null;
        String result = "";
        edit = sharedpreferences.edit();

        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url);
            String json = "";

            // 3. build jsonObject
            JSONObject jsonObject = new JSONObject();

            jsonObject.accumulate("UserName", serverString.getUsername());
            jsonObject.accumulate("Password", serverString.getPassword());
            jsonObject.accumulate(" DeviceId", android_id);

            json = jsonObject.toString();

            StringEntity se = new StringEntity(json);
            httpPost.setEntity(se);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");

            HttpResponse httpResponse = httpclient.execute(httpPost);

            inputStream = httpResponse.getEntity().getContent();

            if (inputStream != null)
                result = convertInputStreamToString(inputStream);

            System.out.println(" Result of json : " + result);


            return result;

        } catch (Exception e) {
        }

        return result;

    }

    private class HttpAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            serverString.setUsername(serverString.getUsername());
            serverString.setPassword(serverString.getPassword());

            return POST(urls[0], serverString);
        }

        @Override
        protected void onPostExecute(final String result) {
            try {
                JSONObject json = new JSONObject(result);
                String jsonStatus = json.optString("ActionStatus");

                JSONObject jsonObject = new JSONObject(jsonStatus);

                String Message = jsonObject.optString("Message").toString();
                String MsgType = jsonObject.optString("MsgType").toString();

                if (Message.equalsIgnoreCase("Success.") && MsgType.equalsIgnoreCase("Success")) {
                    tokenId = json.optString("Token").toString();

                    String UserName = json.optString("UserName").toString();
                    String UserType = json.optString("UserTypeRecordId").toString();

                    if (!tokenId.isEmpty()) {

                        edit.putString(strUserName, serverString.getUsername());
                        edit.putString(strPassword, serverString.getPassword());
                        edit.putString("UserType", UserType);
                        edit.putString(strToken, tokenId);
                        edit.putString(strDeviceId, android_id);
                        edit.putString(strDate, cDate.toString());
                        edit.commit();

                        int userTyp = Integer.parseInt(UserType);
                        if (userTyp == 1) {
                            intent = new Intent(MIS.this, AdminMainActivity.class);
                        } else if (userTyp == 2) {
                            intent = new Intent(MIS.this, Loggedin_Mis.class);
                        } else if (userTyp == 3) {
                            intent = new Intent(MIS.this, Loggedin_Mis.class);
                        } else if (userTyp == 4) {
                            intent = new Intent(MIS.this, TeacherMainActivity.class);
                        }

                        if (progressBar.isShowing()) {
                            progressBar.dismiss();
                        }
                        startActivity(intent);
                    }

                } else {
                    //showProgress(false);
                    if (progressBar.isShowing()) {
                        progressBar.dismiss();
                    }
                    etPassword.setError("Incorrect Password");
                    etUsername.setError("Incorrect UserName");
                    etUsername.requestFocus();
                }
                //output.setText(data);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder out = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            out.append(line);
        }
        reader.close();
        return out.toString();
    }

}
