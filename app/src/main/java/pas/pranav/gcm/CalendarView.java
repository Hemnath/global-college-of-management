package pas.pranav.gcm;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.MenuItem;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import pas.pranav.gcm.admin.AdminMainActivity;
import pas.pranav.gcm.helpers.API_URL;
import pas.pranav.gcm.helpers.CalendarAdapter;
import pas.pranav.gcm.helpers.JSONParserCustom;
import pas.pranav.gcm.teacher.TeacherMainActivity;

public class CalendarView extends android.support.v7.app.AppCompatActivity {

    private ListView listview;
    private CalendarAdapter calendarAdapter;

    private ProgressDialog progressBar;
    private String url, deviceId, token;

    API_URL appURL;

    public static final String strUserName = "userKey";
    public static final String strPassword = "passwordKey";
    public static final String strToken = "token";
    public static final String strDeviceId = "deviceId";

    SharedPreferences.Editor editor;
    SharedPreferences sharedpreferences;

    String userType = "";
    private int intUserType;
    private Intent intent;

    @Override
    protected void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calender);
        android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(pas.pranav.gcm.R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sharedpreferences = getSharedPreferences(MIS.MyPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();

        deviceId = sharedpreferences.getString(strDeviceId, null);
        token = sharedpreferences.getString(strToken, null);
        userType = sharedpreferences.getString("UserType", null);
        intUserType = Integer.parseInt(userType);

        listview = (ListView) findViewById(R.id.listview);
        calendarAdapter = new CalendarAdapter(this);
        listview.setAdapter(calendarAdapter);

        appURL = new API_URL();
        url="";
        if (intUserType == 1) {
            url = appURL.ServerUrlAdmin+appURL.Calender;
        } else if (intUserType == 2) {
            url = appURL.ServerUrlStudent+appURL.Calender;
        } else if (intUserType == 3) {
            url = appURL.ServerUrlParent+appURL.Calender;
        } else if (intUserType == 4) {
            url = appURL.ServerUrlTeacher+appURL.Calender;
        }
        new menuJSONParse().execute(url);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            intent = new Intent(CalendarView.this, Loggedin_Mis.class);
            switch (intUserType) {

                case 1:
                    intent = new Intent(CalendarView.this, AdminMainActivity.class);
                    break;
                case 2:
                    intent = new Intent(CalendarView.this, Loggedin_Mis.class);
                    break;
                case 3:
                    intent = new Intent(CalendarView.this, Loggedin_Mis.class);
                    break;
                case 4:
                    intent = new Intent(CalendarView.this, TeacherMainActivity.class);
                    break;


            }
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }
    private class menuJSONParse extends android.os.AsyncTask<String, String, org.json.JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar = new ProgressDialog(CalendarView.this);
            progressBar.setMessage("Downloading...");
            progressBar.show();

        }

        @Override
        protected org.json.JSONObject doInBackground(String... args) {

            JSONParserCustom jParser = new JSONParserCustom();
            org.json.JSONObject json = jParser.getJSONFromUrl(url, deviceId, token);

            return json;
        }

        @Override
        protected void onPostExecute(org.json.JSONObject json) {
            String data = "";
            try {
                if (progressBar.isShowing()) {
                    progressBar.dismiss();
                }
                JSONArray jsonArray = json.optJSONArray("List");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    String backgroundColor = jsonObject.getString("backgroundColor");
                    String start = jsonObject.optString("start").toString();
                    String end = jsonObject.optString("end").toString();
                    String title = jsonObject.optString("title").toString();
                    String borderColor = jsonObject.optString("borderColor").toString();
                    String strUrl = jsonObject.optString("url").toString();

                    calendarAdapter.addItem(backgroundColor, borderColor, end, start, title, strUrl);
                }
                calendarAdapter.notifyDataSetChanged();

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }
}
