package pas.pranav.gcm.bachelors;

/**
 * Created by Edwin on 15/02/2015.
 */
public class Bachelors_ClassHours extends android.support.v4.app.Fragment {

    String url = "http://202.166.207.142/demo/global/api/app/child_menu/parent/9/id/22";
    org.json.JSONArray user = null;

    android.webkit.WebView contents;

    @Override
    public android.view.View onCreateView(android.view.LayoutInflater inflater,  android.view.ViewGroup container,  android.os.Bundle savedInstanceState) {
        android.view.View v = inflater.inflate(pas.pranav.gcm.R.layout.plus2_tab4,container,false);
        contents = (android.webkit.WebView) v.findViewById(pas.pranav.gcm.R.id.wvPlus2ClassHours);
        System.out.println("about plus two called");
        new JSONParse().execute();

        return v;
    }

    private class JSONParse extends android.os.AsyncTask<String, String, org.json.JSONObject> {
        private android.app.ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            System.out.println("inside pre execute");
            //super.onPreExecute();
            //            contents = (TextView) getView().findViewById(pas.pranav.gcm.R.id.wvAboutContents);
            //            pDialog = new android.app.ProgressDialog(getActivity());
            //            pDialog.setMessage("Getting Data...");
            //            pDialog.setIndeterminate(false);
            //            pDialog.setCancelable(true);
            //            pDialog.show();

        }

        @Override
        protected org.json.JSONObject doInBackground(String... args) {
            pas.pranav.gcm.helpers.JSONParser jParser = new pas.pranav.gcm.helpers.JSONParser();

            // Getting JSON from URL
            org.json.JSONObject json = jParser.getJSONFromUrl(url);
            System.out.println("inside background");
            return json;
        }
        @Override
        protected void onPostExecute(org.json.JSONObject json) {

            //            Handler handler = new Handler();
            //                        handler.postDelayed(new Runnable() {
            //                            public void run() {
            //                                pDialog.dismiss();
            //                            }}, 3000);
            try {
                // Getting JSON Array
                user = json.getJSONArray("child_menu");
                org.json.JSONObject c = user.getJSONObject(0);

                // Storing  JSON item in a Variable
                String description = c.getString("description");
                System.out.println("description: " + description);
                //Set JSON Data in TextView
                //contents.setMovementMethod(new android.text.method.ScrollingMovementMethod().getInstance());
                //title.setText(android.text.Html.fromHtml(menu_title));
                contents.loadData(description, "text/html", "utf-8");
            } catch (org.json.JSONException e) {
                e.printStackTrace();
            }

        }
    }
}















//    public android.view.View onCreateView(android.view.LayoutInflater inflater, @android.support.annotation.Nullable android.view.ViewGroup container, @android.support.annotation.Nullable android.os.Bundle savedInstanceState) {
//        android.view.View v = inflater.inflate(pas.pranav.gcm.R.layout.plus2_tab1,container,false);
//        return v;
//    }
//
//}
