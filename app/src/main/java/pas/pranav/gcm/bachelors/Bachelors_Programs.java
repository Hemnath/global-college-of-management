package pas.pranav.gcm.bachelors;


public class Bachelors_Programs extends android.support.v7.app.AppCompatActivity {

    // Declaring Your View and Variables

    android.support.v7.widget.Toolbar toolbar;
    android.support.v4.view.ViewPager pager;
    Bachelors_ViewPagerAdapter adapter;
    pas.pranav.gcm.SlidingTabLayout tabs;
    CharSequence[] Titles = {"About the program", "Admission Procedure", "Courses","Class Hours", "Teaching Faculties", "Fees", "Scholarships", "Contacts"};
    int Numboftabs = 8;

    @Override protected void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(pas.pranav.gcm.R.layout.programs);

        // Creating The Toolbar and setting it as the Toolbar for the activity

        toolbar = (android.support.v7.widget.Toolbar) findViewById(pas.pranav.gcm.R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new android.view.View.OnClickListener() {
            @Override public void onClick(android.view.View v) {
                startActivity(new android.content.Intent(Bachelors_Programs.this, pas.pranav.gcm.Programs_List.class));
            }
        });


        // Creating The Plus2_ViewPagerAdapter and Passing Fragment Manager, Titles for the Tabs and Number Of Tabs.
        adapter = new pas.pranav.gcm.bachelors.Bachelors_ViewPagerAdapter(getSupportFragmentManager(), Titles, Numboftabs);

        // Assigning ViewPager View and setting the adapter
        pager = (android.support.v4.view.ViewPager) findViewById(pas.pranav.gcm.R.id.pager);
        pager.setAdapter(adapter);

        // Assiging the Sliding Tab Layout View
        tabs = (pas.pranav.gcm.SlidingTabLayout) findViewById(pas.pranav.gcm.R.id.tabs);
        tabs.setDistributeEvenly(true); // To make the Tabs Fixed set this true, This makes the tabs Space Evenly in Available width

        // Setting Custom Color for the Scroll bar indicator of the Tab View
        tabs.setCustomTabColorizer(new pas.pranav.gcm.SlidingTabLayout.TabColorizer() {
            @Override public int getIndicatorColor(int position) {
                return getResources().getColor(pas.pranav.gcm.R.color.selector);
            }
        });

        // Setting the ViewPager For the SlidingTabsLayout
        tabs.setViewPager(pager);

    }

    @Override public boolean onCreateOptionsMenu(android.view.Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(pas.pranav.gcm.R.menu.main, menu);
        return true;
    }

    @Override public boolean onOptionsItemSelected(android.view.MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == pas.pranav.gcm.R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}