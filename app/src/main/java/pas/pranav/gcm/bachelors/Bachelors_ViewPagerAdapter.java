package pas.pranav.gcm.bachelors;

public class Bachelors_ViewPagerAdapter extends android.support.v4.app.FragmentStatePagerAdapter {

    CharSequence Titles[]; // This will Store the Titles of the Tabs which are Going to be passed when Plus2_ViewPagerAdapter is created
    int NumbOfTabs; // Store the number of tabs, this will also be passed when the Plus2_ViewPagerAdapter is created

    // Build a Constructor and assign the passed Values to appropriate values in the class
    public Bachelors_ViewPagerAdapter(android.support.v4.app.FragmentManager fm, CharSequence mTitles[], int mNumbOfTabsumb) {
        super(fm);

        this.Titles = mTitles;
        this.NumbOfTabs = mNumbOfTabsumb;

    }

    //This method return the fragment for the every position in the View Pager
    @Override public android.support.v4.app.Fragment getItem(int position) {

        if (position == 0){
            pas.pranav.gcm.bachelors.Bachelors_About tab1 = new pas.pranav.gcm.bachelors.Bachelors_About();
            return tab1;
        } else if (position == 1){
            pas.pranav.gcm.bachelors.Bachelors_Admission tab2 = new pas.pranav.gcm.bachelors.Bachelors_Admission();
            return tab2;
        } else if (position == 2) {
            pas.pranav.gcm.bachelors.Bachelors_Courses tab3 = new pas.pranav.gcm.bachelors.Bachelors_Courses();
            return tab3;
        } else if (position == 3) {
            pas.pranav.gcm.bachelors.Bachelors_ClassHours tab4 = new pas.pranav.gcm.bachelors.Bachelors_ClassHours();
            return tab4;
        } else if (position == 4) {
            pas.pranav.gcm.bachelors.Bachelors_Faculties tab5 = new pas.pranav.gcm.bachelors.Bachelors_Faculties();
            return tab5;
        } else if(position == 5){
            pas.pranav.gcm.bachelors.Bachelors_Fees tab6 = new pas.pranav.gcm.bachelors.Bachelors_Fees();
            return tab6;
        } else if(position == 6){
            pas.pranav.gcm.bachelors.Bachelors_Scholarship tab6 = new pas.pranav.gcm.bachelors.Bachelors_Scholarship();
            return tab6;
        } else {
            pas.pranav.gcm.bachelors.Bachelors_Contact ta7 = new pas.pranav.gcm.bachelors.Bachelors_Contact();
            return ta7;
        }
    }

    // This method return the titles for the Tabs in the Tab Strip

    @Override public CharSequence getPageTitle(int position) {
        return Titles[position];
    }

    // This method return the Number of tabs for the tabs Strip

    @Override public int getCount() {
        return NumbOfTabs;
    }
}