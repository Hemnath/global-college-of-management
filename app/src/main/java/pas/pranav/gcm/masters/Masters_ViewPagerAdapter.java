package pas.pranav.gcm.masters;

public class Masters_ViewPagerAdapter extends android.support.v4.app.FragmentStatePagerAdapter {

    CharSequence Titles[]; // This will Store the Titles of the Tabs which are Going to be passed when Plus2_ViewPagerAdapter is created
    int NumbOfTabs; // Store the number of tabs, this will also be passed when the Plus2_ViewPagerAdapter is created

    // Build a Constructor and assign the passed Values to appropriate values in the class
    public Masters_ViewPagerAdapter(android.support.v4.app.FragmentManager fm, CharSequence mTitles[], int mNumbOfTabsumb) {
        super(fm);

        this.Titles = mTitles;
        this.NumbOfTabs = mNumbOfTabsumb;

    }

    //This method return the fragment for the every position in the View Pager
    @Override public android.support.v4.app.Fragment getItem(int position) {

        if (position == 0){
            Masters_About tab1 = new Masters_About();
            return tab1;
        } else if (position == 1){
            Masters_Admission tab2 = new Masters_Admission();
            return tab2;
        } else if (position == 2) {
            Masters_Courses tab3 = new Masters_Courses();
            return tab3;
        } else if (position == 3) {
            Masters_ClassHours tab4 = new Masters_ClassHours();
            return tab4;
        } else if (position == 4) {
            Masters_Faculties tab5 = new Masters_Faculties();
            return tab5;
        } else if(position == 5){
            Masters_Fees tab6 = new Masters_Fees();
            return tab6;
        } else if(position == 6){
            Masters_Scholarship tab6 = new Masters_Scholarship();
            return tab6;
        } else {
            Masters_Contact ta7 = new Masters_Contact();
            return ta7;
        }
    }

    // This method return the titles for the Tabs in the Tab Strip

    @Override public CharSequence getPageTitle(int position) {
        return Titles[position];
    }

    // This method return the Number of tabs for the tabs Strip

    @Override public int getCount() {
        return NumbOfTabs;
    }
}