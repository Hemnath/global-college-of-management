package pas.pranav.gcm.plus2;

import android.os.Bundle;
import android.view.View;
import android.support.v4.app.Fragment;
import android.webkit.WebView;

import org.json.JSONArray;
import pas.pranav.gcm.helpers.JSONParser;

/**
 * Created by Edwin on 15/02/2015.
 */
public class Plus2_About extends Fragment {

    JSONArray user = null;
    WebView contents;

    @Override
    public View onCreateView(android.view.LayoutInflater inflater,  android.view.ViewGroup container,  Bundle savedInstanceState) {
        View v = inflater.inflate(pas.pranav.gcm.R.layout.plus2_tab1,container,false);
        contents = (WebView) v.findViewById(pas.pranav.gcm.R.id.wvAboutContents);
        new JSONParse().execute();

        return v;
    }

    private class JSONParse extends android.os.AsyncTask<String, String, org.json.JSONObject> {
        private android.app.ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {

        }
        @Override
        protected org.json.JSONObject doInBackground(String... args) {
            JSONParser jParser = new JSONParser();
            pas.pranav.gcm.helpers.API_URL url = new pas.pranav.gcm.helpers.API_URL();
            org.json.JSONObject json = jParser.getJSONFromUrl(url.aboutPlus2Url);
            System.out.println("inside background");
            return json;
        }
        @Override
        protected void onPostExecute(org.json.JSONObject json) {

            try {
                user = json.getJSONArray("child_menu");
                org.json.JSONObject c = user.getJSONObject(0);
                String description = c.getString("description");
                contents.loadData(description, "text/html", "utf-8");
            } catch (org.json.JSONException e) {
                e.printStackTrace();
            }

        }
    }
}

