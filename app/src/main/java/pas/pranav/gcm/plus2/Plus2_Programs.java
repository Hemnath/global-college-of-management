package pas.pranav.gcm.plus2;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.content.Intent;

public class Plus2_Programs extends AppCompatActivity {

    // Declaring Your View and Variables

    Toolbar toolbar;
    ViewPager pager;
    Plus2_ViewPagerAdapter adapter;
    pas.pranav.gcm.SlidingTabLayout tabs;
    CharSequence[] Titles = {"About the program", "Admission Procedure", "Courses","Class Hours", "Teaching Faculties", "Fees", "Scholarships", "Contacts"};
    int Numboftabs = 8;

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(pas.pranav.gcm.R.layout.programs);

        toolbar = (Toolbar) findViewById(pas.pranav.gcm.R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new android.view.View.OnClickListener() {
            @Override public void onClick(android.view.View v) {
                startActivity(new Intent(Plus2_Programs.this, pas.pranav.gcm.Programs_List.class));
            }
        });

        adapter = new Plus2_ViewPagerAdapter(getSupportFragmentManager(), Titles, Numboftabs);

        // Assigning ViewPager View and setting the adapter
        pager = (ViewPager) findViewById(pas.pranav.gcm.R.id.pager);
        pager.setAdapter(adapter);

        tabs = (pas.pranav.gcm.SlidingTabLayout) findViewById(pas.pranav.gcm.R.id.tabs);
        tabs.setDistributeEvenly(true);

        tabs.setCustomTabColorizer(new pas.pranav.gcm.SlidingTabLayout.TabColorizer() {
            @Override public int getIndicatorColor(int position) {
                return getResources().getColor(pas.pranav.gcm.R.color.selector);
            }
        });

        tabs.setViewPager(pager);

    }

}