package pas.pranav.gcm.plus2;

public class Plus2_ViewPagerAdapter extends android.support.v4.app.FragmentStatePagerAdapter {

    CharSequence Titles[];
    int NumbOfTabs;

    public Plus2_ViewPagerAdapter(android.support.v4.app.FragmentManager fm, CharSequence mTitles[], int mNumbOfTabsumb) {
        super(fm);

        this.Titles = mTitles;
        this.NumbOfTabs = mNumbOfTabsumb;

    }

    @Override
    public android.support.v4.app.Fragment getItem(int position) {

        if (position == 0) {
            pas.pranav.gcm.plus2.Plus2_About tab1 = new pas.pranav.gcm.plus2.Plus2_About();
            return tab1;
        } else if (position == 1) {
            pas.pranav.gcm.plus2.Plus2_Admission tab2 = new pas.pranav.gcm.plus2.Plus2_Admission();
            return tab2;
        } else if (position == 2) {
            pas.pranav.gcm.plus2.Plus2_Courses tab3 = new pas.pranav.gcm.plus2.Plus2_Courses();
            return tab3;
        } else if (position == 3) {
            pas.pranav.gcm.plus2.Plus2_ClassHours tab4 = new pas.pranav.gcm.plus2.Plus2_ClassHours();
            return tab4;
        } else if (position == 4) {
            pas.pranav.gcm.plus2.Plus2_Faculties tab5 = new pas.pranav.gcm.plus2.Plus2_Faculties();
            return tab5;
        } else if (position == 5) {
            pas.pranav.gcm.plus2.Plus2_Fees tab6 = new pas.pranav.gcm.plus2.Plus2_Fees();
            return tab6;
        } else if (position == 6) {
            pas.pranav.gcm.plus2.Plus2_Scholarship tab6 = new pas.pranav.gcm.plus2.Plus2_Scholarship();
            return tab6;
        } else {
            pas.pranav.gcm.plus2.Plus2_Contact ta7 = new pas.pranav.gcm.plus2.Plus2_Contact();
            return ta7;
        }
    }


    @Override
    public CharSequence getPageTitle(int position) {

        return Titles[position];
    }


    @Override
    public int getCount() {
        return NumbOfTabs;
    }
}