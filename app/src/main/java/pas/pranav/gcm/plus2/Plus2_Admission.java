package pas.pranav.gcm.plus2;

import android.os.Bundle;
import android.view.View;
import android.support.v4.app.Fragment;
import android.webkit.WebView;

import org.json.JSONArray;
import pas.pranav.gcm.helpers.JSONParser;

/**
 * Created by Edwin on 15/02/2015.
 */
public class Plus2_Admission extends Fragment {


    JSONArray user = null;

    WebView contents;

    @Override
    public View onCreateView(android.view.LayoutInflater inflater,  android.view.ViewGroup container,  Bundle savedInstanceState) {
        View v = inflater.inflate(pas.pranav.gcm.R.layout.plus2_tab2,container,false);
        contents = (WebView) v.findViewById(pas.pranav.gcm.R.id.wvAdmissionPlus2);
        System.out.println("about plus two called");
        new JSONParse().execute();

        return v;
    }

    private class JSONParse extends android.os.AsyncTask<String, String, org.json.JSONObject> {
        private android.app.ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            System.out.println("inside pre execute");

        }

        @Override
        protected org.json.JSONObject doInBackground(String... args) {
            JSONParser jParser = new JSONParser();
            pas.pranav.gcm.helpers.API_URL url = new pas.pranav.gcm.helpers.API_URL();
            // Getting JSON from URL
            org.json.JSONObject json = jParser.getJSONFromUrl(url.admissionPlus2Url);
            System.out.println("inside background");
            return json;
        }
        @Override
        protected void onPostExecute(org.json.JSONObject json) {

            try {
                // Getting JSON Array
                user = json.getJSONArray("child_menu");
                org.json.JSONObject c = user.getJSONObject(0);

                // Storing  JSON item in a Variable
                String description = c.getString("description");
                System.out.println("description: " + description);
                //Set JSON Data in TextView
                //contents.setMovementMethod(new android.text.method.ScrollingMovementMethod().getInstance());
                //title.setText(android.text.Html.fromHtml(menu_title));
                contents.loadData(description, "text/html", "utf-8");
            } catch (org.json.JSONException e) {
                e.printStackTrace();
            }

        }
    }
}















//    public android.view.View onCreateView(android.view.LayoutInflater inflater, @android.support.annotation.Nullable android.view.ViewGroup container, @android.support.annotation.Nullable android.os.Bundle savedInstanceState) {
//        android.view.View v = inflater.inflate(pas.pranav.gcm.R.layout.plus2_tab1,container,false);
//        return v;
//    }
//
//}
