package pas.pranav.gcm;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;

/**
 * Created by dev on 10/30/15.
 */

public class YouTube extends AppCompatActivity {
    WebView webView;
    String url = "https://www.youtube.com/user/globalcollegeofm";
    @Override protected void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(pas.pranav.gcm.R.layout.youtube);

        webView = (WebView) findViewById(pas.pranav.gcm.R.id.wvYoutube);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setSupportZoom(true);
        webView.setWebViewClient(new android.webkit.WebViewClient());
        webView.loadUrl(url);

    }

    @Override
    public void onBackPressed() {
        if (webView.canGoBack()) {
            webView.goBack();
        } else {
            Intent launchNextActivity = new Intent(YouTube.this, MainActivity.class);
            launchNextActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            launchNextActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            launchNextActivity.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(launchNextActivity);
        }
    }

}
