package pas.pranav.gcm;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ImageButton;
import android.view.View;
import android.content.Intent;

import pas.pranav.gcm.bachelors.Bachelors_Programs;
import pas.pranav.gcm.masters.*;
import pas.pranav.gcm.plus2.Plus2_Programs;

/**
 * Created by dev on 10/27/15.
 */
public class Programs_List extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(pas.pranav.gcm.R.layout.program_buttons);

        Toolbar toolbar = (Toolbar) findViewById(pas.pranav.gcm.R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Programs_List.this, MainActivity.class));
            }
        });

        ImageButton btnPlusTwo = (ImageButton) findViewById(pas.pranav.gcm.R.id.btnPlusTwo);
        ImageButton btnBachelors = (ImageButton) findViewById(pas.pranav.gcm.R.id.btnBachelors);
        ImageButton btnMasters = (ImageButton) findViewById(pas.pranav.gcm.R.id.btnMasters);


        btnPlusTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isConnected()) {
                    startActivity(new Intent(Programs_List.this, Plus2_Programs.class));
                } else {
                    showAlertDialog(Programs_List.this, "No Internet Connection",
                            "You don't have internet connection.", false);
                }

            }
        });

        btnBachelors.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isConnected()) {
                    startActivity(new Intent(Programs_List.this, Bachelors_Programs.class));
                } else {
                    showAlertDialog(Programs_List.this, "No Internet Connection",
                            "You don't have internet connection.", false);
                }
            }
        });

        btnMasters.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isConnected()) {
                    startActivity(new Intent(Programs_List.this, Masters_Programs.class));
                } else {
                    showAlertDialog(Programs_List.this, "No Internet Connection",
                            "You don't have internet connection.", false);
                }
            }
        });


    }

    @Override
    public void onBackPressed() {
        Intent launchNextActivity = new Intent(Programs_List.this, MainActivity.class);
        launchNextActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        launchNextActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        launchNextActivity.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(launchNextActivity);
    }

    boolean isConnected() {
        ConnectivityManager cm = (ConnectivityManager) this.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
        return isConnected;
    }

    public void showAlertDialog(Context context, String title, String message, Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();

        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setIcon((status) ? R.drawable.error : R.drawable.error);
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alertDialog.show();
    }
}
