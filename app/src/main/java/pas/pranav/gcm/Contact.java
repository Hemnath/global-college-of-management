package pas.pranav.gcm;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

public class Contact extends AppCompatActivity {
    private static Button college, princ, vicePrinc, programDir, programCoord;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Contact.this, MainActivity.class));
            }
        });

        college = (Button) findViewById(R.id.first);
        princ = (Button) findViewById(R.id.princNo);
        vicePrinc = (Button) findViewById(R.id.viceprincNo);
        programDir = (Button) findViewById(R.id.programDirNo);
        programCoord = (Button) findViewById(R.id.programCoordNo);

        college.setOnClickListener(mListener);
        princ.setOnClickListener(mListener);
        vicePrinc.setOnClickListener(mListener);
        programDir.setOnClickListener(mListener);
        programCoord.setOnClickListener(mListener);
    }

    private View.OnClickListener mListener = new View.OnClickListener() {
        String call;

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.first:
                    call = college.getText().toString();
                case R.id.princNo:
                    call = princ.getText().toString();
                case R.id.viceprincNo:
                    call = vicePrinc.getText().toString();
                case R.id.programDirNo:
                    call = programDir.getText().toString();
                case R.id.programCoordNo:
                    call = programCoord.getText().toString();
            }
            Intent callIntent = new android.content.Intent(Intent.ACTION_CALL);
            callIntent.setData(android.net.Uri.parse("tel:"+call));
            if (ActivityCompat.checkSelfPermission(Contact.this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            startActivity(callIntent);
        }

    };

    @Override
    public void onBackPressed() {
        Intent launchNextActivity = new Intent(Contact.this, MainActivity.class);
        launchNextActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        launchNextActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        launchNextActivity.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(launchNextActivity);
    }
}
