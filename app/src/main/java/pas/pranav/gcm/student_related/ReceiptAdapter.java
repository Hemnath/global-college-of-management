package pas.pranav.gcm.student_related;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import pas.pranav.gcm.R;

/**
 * Created by ik890 on 8/8/2016.
 */
public class ReceiptAdapter extends BaseAdapter {
    private Context mContext;
    private List<Item> items = new ArrayList<Item>();
    private LayoutInflater inflater;

    public ReceiptAdapter(Context context) {
        inflater = LayoutInflater.from(context);
        mContext = context;
    }

    public boolean addItem(String narration, String accountNo, String amountPaid, String discountAmount, String currentBalance, String previousBalance, String receiptDate) {

        items.add(new Item( narration,  accountNo,  amountPaid,  discountAmount,  currentBalance,  previousBalance, receiptDate));
        return true;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = view;
        final Item item = (Item) getItem(i);
        TextView narration, accountNo, amountPaid, currentBalance, discountAmount,receiptDate;

        if (v == null) {
            v = inflater.inflate(R.layout.receipt_adapter, viewGroup, false);
            v.setTag(R.id.narration, v.findViewById(R.id.narration));
            v.setTag(R.id.accountNo, v.findViewById(R.id.accountNo));
            v.setTag(R.id.amountPaid, v.findViewById(R.id.amountPaid));
            v.setTag(R.id.discountAmount, v.findViewById(R.id.discountAmount));
            v.setTag(R.id.currentBalance, v.findViewById(R.id.currentBalance));
            v.setTag(R.id.receiptDate, v.findViewById(R.id.receiptDate));

        }
        narration = (TextView) v.getTag(R.id.narration);
        accountNo = (TextView) v.getTag(R.id.accountNo);
        amountPaid = (TextView) v.getTag(R.id.amountPaid);
        discountAmount = (TextView) v.getTag(R.id.discountAmount);
        currentBalance = (TextView) v.getTag(R.id.currentBalance);
        receiptDate = (TextView) v.getTag(R.id.receiptDate);

        narration.setText(item.Narration);
        accountNo.setText(item.AccountNo);
        amountPaid.setText(item.AmountPaid);
        discountAmount.setText(item.DiscountAmount);
        currentBalance.setText(item.CurrentBalance);

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat.parse(item.ReceiptDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        receiptDate.setText(convertedDate.toString());

        return v;
    }

    private class Item {

        final String Narration;
        final String AccountNo;
        final String AmountPaid;
        final String DiscountAmount;
        final String CurrentBalance;
        final String PreviousBalance;
        final String ReceiptDate;

        public Item(String narration, String accountNo, String amountPaid, String discountAmount, String currentBalance, String previousBalance, String receiptDate) {
            this.Narration = narration;
            AccountNo = accountNo;
            AmountPaid = amountPaid;
            DiscountAmount = discountAmount;
            CurrentBalance = currentBalance;
            PreviousBalance = previousBalance;
            ReceiptDate = receiptDate;
        }
    }
}
