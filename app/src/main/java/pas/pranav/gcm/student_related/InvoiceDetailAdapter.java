package pas.pranav.gcm.student_related;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import pas.pranav.gcm.R;

public class InvoiceDetailAdapter extends BaseAdapter {
    private Context mContext;
    private List<Item> items = new ArrayList<Item>();
    private LayoutInflater inflater;

    public InvoiceDetailAdapter(Context context) {
        inflater = LayoutInflater.from(context);
        mContext = context;

    }

    public void addItem(String sn, String particular, String amount) {
        items.add(new Item( sn,  particular,  amount));

    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = view;
        TextView particular, sn,amount;

        if (v == null) {
            v = inflater.inflate(R.layout.activity_invoice_detail_adapter, viewGroup, false);
            v.setTag(R.id.sn, v.findViewById(R.id.sn));
            v.setTag(R.id.particular, v.findViewById(R.id.particular));
            v.setTag(R.id.amount, v.findViewById(R.id.amount));

        }

        particular = (TextView) v.getTag(R.id.particular);
        amount = (TextView) v.getTag(R.id.amount);
        sn = (TextView) v.getTag(R.id.sn);

        Item item = (Item) getItem(i);

        sn.setText(item.sn);
        particular.setText(item.particular);
        amount.setText(item.amount);

        return v;
    }

    private class Item {
        final String sn;
        final String particular;
        final String amount;

        public Item(String sn, String particular, String amount) {
            this.sn = sn;
            this.particular = particular;
            this.amount = amount;

        }
    }
}
