package pas.pranav.gcm.student_related;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import pas.pranav.gcm.MIS;
import pas.pranav.gcm.R;
import pas.pranav.gcm.helpers.API_URL;
import pas.pranav.gcm.helpers.JSONParserCustom;
import pas.pranav.gcm.helpers.MessageViewAdapter;

public class CurrentHomework extends AppCompatActivity {

    private ProgressDialog progressBar;
    private ListView listview;
    CurrentHomeworkAdapter currentHomeworkAdapter;

    private String url, deviceId, token;

    public static final String strUserName = "userKey";
    public static final String strPassword = "passwordKey";
    public static final String strToken = "token";
    public static final String strDeviceId = "deviceId";

    SharedPreferences.Editor editor;
    SharedPreferences sharedpreferences;
    private API_URL appURL;
    private String userType;
    private int intUserType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher_message);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sharedpreferences = getSharedPreferences(MIS.MyPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();

        deviceId = sharedpreferences.getString(strDeviceId, null);
        token = sharedpreferences.getString(strToken, null);

        listview = (ListView) findViewById(R.id.listview);
        currentHomeworkAdapter = new CurrentHomeworkAdapter(this);
        listview.setAdapter(currentHomeworkAdapter);

        appURL = new API_URL();
        userType = sharedpreferences.getString("UserType", null);
        intUserType = Integer.parseInt(userType);
        appURL = new API_URL();
        if (intUserType == 2) {
            url = appURL.ServerUrlStudent + appURL.CurrentHomeWork;
        } else if (intUserType == 3) {
            url = appURL.ServerUrlParent + appURL.CurrentHomeWork;
        }

        new menuJSONParse().execute(url);


    }

    private class menuJSONParse extends android.os.AsyncTask<String, String, org.json.JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar = new ProgressDialog(CurrentHomework.this);
            progressBar.setMessage("Downloading...");
            progressBar.show();

        }

        @Override
        protected org.json.JSONObject doInBackground(String... args) {

            JSONParserCustom jParser = new JSONParserCustom();
            org.json.JSONObject json = jParser.getJSONFromUrl(url, deviceId, token);

            return json;
        }

        @Override
        protected void onPostExecute(org.json.JSONObject json) {
            String data = "";
            try {
                if (progressBar.isShowing()) {
                    progressBar.dismiss();
                }
                JSONArray jsonArray = json.optJSONArray("List");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    String AssignmentTitle = jsonObject.optString("AssignmentTitle").toString().replace("null","");
                    String Instruction = jsonObject.optString("Instruction").toString().replace("<p>","").replace("</p>","").replace("</div>","").replace("null","").replace("<br/>","");
                    String SubjectName = jsonObject.optString("SubjectName").toString().replace("null","");
                    String SummissionDate = jsonObject.optString("SummissionDate").toString().replace("null","");

                    currentHomeworkAdapter.addItem(AssignmentTitle, Instruction, SubjectName, SummissionDate);
                }

             currentHomeworkAdapter.notifyDataSetChanged();

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }
}
