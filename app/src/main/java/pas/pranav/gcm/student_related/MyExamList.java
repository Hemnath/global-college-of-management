package pas.pranav.gcm.student_related;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import pas.pranav.gcm.MIS;
import pas.pranav.gcm.R;
import pas.pranav.gcm.helpers.API_URL;
import pas.pranav.gcm.helpers.JSONParserCustom;


public class MyExamList extends AppCompatActivity {
    private ProgressDialog progressBar;
    private ExpandableListView listview;
    ExamExmdListAdapter examListAdapter;
    int lastExpandedPosition = -1;

    private String url, deviceId, token;

    public static final String strUserName = "userKey";
    public static final String strPassword = "passwordKey";
    public static final String strToken = "token";
    public static final String strDeviceId = "deviceId";

    SharedPreferences.Editor editor;
    SharedPreferences sharedpreferences;
    private API_URL appURL;
    private Intent myIntent;
    private int intUserType;
    private String userType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_exam_list);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sharedpreferences = getSharedPreferences(MIS.MyPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();

        deviceId = sharedpreferences.getString(strDeviceId, null);
        token = sharedpreferences.getString(strToken, null);

        listview = (ExpandableListView) findViewById(R.id.myExamlistview);
        examListAdapter = new ExamExmdListAdapter(this);
        listview.setAdapter(examListAdapter);

        userType = sharedpreferences.getString("UserType", null);
        intUserType = Integer.parseInt(userType);
        appURL = new API_URL();
        if (intUserType == 2) {
            url = appURL.ServerUrlStudent + appURL.MyExamList;
        } else if (intUserType == 3) {
            url = appURL.ServerUrlParent + appURL.MyExamList;
        }

        new menuJSONParse().execute(url);

        listview.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
                if (lastExpandedPosition != -1
                        && groupPosition != lastExpandedPosition) {
                    listview.collapseGroup(lastExpandedPosition);
                }
                lastExpandedPosition = groupPosition;
            }
        });

        // Listview on child click listener
        listview.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                String examRecordId = examListAdapter.examRecordId(groupPosition, childPosition);
                myIntent = new Intent(getApplicationContext(), StudentReportCard.class);
                myIntent.putExtra("examRecordId", examRecordId);
                startActivity(myIntent);
                return false;
            }
        });
    }


    private class menuJSONParse extends android.os.AsyncTask<String, String, org.json.JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar = new ProgressDialog(MyExamList.this);
            progressBar.setMessage("Downloading...");
            progressBar.show();

        }

        @Override
        protected org.json.JSONObject doInBackground(String... args) {

            JSONParserCustom jParser = new JSONParserCustom();
            org.json.JSONObject json = jParser.getJSONFromUrl(url, deviceId, token);

            return json;
        }

        @Override
        protected void onPostExecute(org.json.JSONObject json) {
            String data = "";
            try {
                if (progressBar.isShowing()) {
                    progressBar.dismiss();
                }
                JSONArray jsonArray = json.optJSONArray("List");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    String courseName = jsonObject.optString("CourseName").toString();
                    String examEndDate = jsonObject.optString("ExamEndDate").toString();
                    String examNotice = jsonObject.optString("ExamNotice").toString();
                    String examPublishNotice = jsonObject.getString("ExamPublishNotice");
                    String examRecordId = jsonObject.optString("ExamRecordId").toString();
                    String examStartDate = jsonObject.optString("ExamStartDate").toString();
                    String examTitle = jsonObject.optString("ExamTitle").toString();
                    boolean isReExam = jsonObject.getBoolean("IsReExam");
                    String partName = jsonObject.optString("PartName").toString();
                    String publishDate = jsonObject.optString("PublishDate").toString();
                    String yearName = jsonObject.optString("YearName").toString();

                    examListAdapter.addItem(courseName, examEndDate, examNotice, examPublishNotice, examRecordId, examStartDate, examTitle, isReExam, partName, publishDate, yearName);

                }
                examListAdapter.notifyDataSetChanged();

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }
}
