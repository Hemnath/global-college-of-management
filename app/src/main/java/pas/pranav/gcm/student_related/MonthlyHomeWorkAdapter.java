package pas.pranav.gcm.student_related;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import pas.pranav.gcm.R;

/**
 * Created by ik890 on 7/27/2016.
 */
public class MonthlyHomeWorkAdapter extends BaseExpandableListAdapter {

    private Context mContext;
    private HashMap<GroupItem, List<ChildItem>> childItems = new HashMap<GroupItem, List<ChildItem>>();
    private List<GroupItem> groupItems = new ArrayList<GroupItem>();
    private LayoutInflater inflater;

    public MonthlyHomeWorkAdapter(Context context) {
        this.mContext = context;
    }

    public void addItem(int sn, String monthName, String nYearName, String classTeacherGrad, String teacherGrad, String universityBordRegNo, String yearName, String totalAssignment, String totalSubmitted, String subjectName, String subTotalAssignment, String subTotalSubmitted) {

        boolean isAdded = false;
        for (int i = 0; i < groupItems.size(); i++) {
            GroupItem item = groupItems.get(i);

            if (item.sn == sn) {

                List<ChildItem> childItemList = childItems.get(groupItems.get(i));

                ChildItem childItem = new ChildItem(subjectName, subTotalAssignment, subTotalSubmitted);
                childItemList.add(childItem);
                childItems.put(groupItems.get(i), childItemList);
                isAdded = true;
            }
        }
        if (!isAdded) {

            GroupItem groupItem = new GroupItem(sn, monthName, nYearName, classTeacherGrad, teacherGrad, universityBordRegNo, yearName, totalAssignment, totalSubmitted);
            groupItems.add(groupItem);
            List<ChildItem> childItemList = new ArrayList<ChildItem>();

            ChildItem childItem = new ChildItem(subjectName, subTotalAssignment, subTotalSubmitted);
            childItemList.add(childItem);
            childItems.put(groupItem, childItemList);
        }


    }

    public boolean clearItem() {
        groupItems.clear();
        return true;
    }

    @Override
    public int getGroupCount() {
        return groupItems.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return childItems.get(groupItems.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.groupItems.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return this.childItems.get(this.groupItems.get(groupPosition))
                .get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }


    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        View v = convertView;
        TextView lblListHeader, lblListTottalAsng;

        if (v == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = infalInflater.inflate(R.layout.groupitem_mothly_hm, parent, false);
            v.setTag(R.id.lblListHeader, v.findViewById(R.id.lblListHeader));
            v.setTag(R.id.lblListTottalAsng, v.findViewById(R.id.lblListTottalAsng));
        }
        lblListHeader = (TextView) v.getTag(R.id.lblListHeader);
        lblListTottalAsng = (TextView) v.getTag(R.id.lblListTottalAsng);

        GroupItem item = (GroupItem) getGroup(groupPosition);

        lblListHeader.setText(item.yearName + " " + item.monthName + " ( " + item.nYearName + " ) ");
        lblListTottalAsng.setText(item.totalSubmitted + "/" + item.totalAssignment);

        return v;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        View v = convertView;
        TextView subjectName, grade;

        if (v == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = infalInflater.inflate(R.layout.childitem_mothly_hm, parent, false);
            v.setTag(R.id.lblListSubjectName, v.findViewById(R.id.lblListSubjectName));
            v.setTag(R.id.lblListGrade, v.findViewById(R.id.lblListGrade));
        }
        subjectName = (TextView) v.getTag(R.id.lblListSubjectName);
        grade = (TextView) v.getTag(R.id.lblListGrade);

        ChildItem item = (ChildItem) getChild(groupPosition, childPosition);

        subjectName.setText(item.subjectName);
        grade.setText(item.subTotalSubmitted + "/" + item.subTotalAssignment);

        return v;
    }


    private class GroupItem {
        int sn;
        final String monthName;
        final String nYearName;
        final String classTeacherGrad;
        final String teacherGrad;
        final String UniversityBordRegNo;
        final String yearName;
        final String totalAssignment;
        final String totalSubmitted;

        public GroupItem(int sn, String monthName, String nYearName, String classTeacherGrad, String teacherGrad, String universityBordRegNo, String yearName, String totalAssignment, String totalSubmitted) {
            this.sn = sn;
            this.monthName = monthName;
            this.nYearName = nYearName;
            this.classTeacherGrad = classTeacherGrad;
            this.teacherGrad = teacherGrad;
            UniversityBordRegNo = universityBordRegNo;
            this.yearName = yearName;
            this.totalAssignment = totalAssignment;
            this.totalSubmitted = totalSubmitted;
        }
    }

    private class ChildItem {

        final String subjectName;
        final String subTotalAssignment;
        final String subTotalSubmitted;

        public ChildItem(String subjectName, String subTotalAssignment, String subTotalSubmitted) {
            this.subjectName = subjectName;
            this.subTotalAssignment = subTotalAssignment;
            this.subTotalSubmitted = subTotalSubmitted;
        }


    }
}

