package pas.pranav.gcm.student_related;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import pas.pranav.gcm.R;

/**
 * Created by ik890 on 7/28/2016.
 */
public class VoucherDetailAdapter extends BaseAdapter {
    private Context mContext;
    private List<Item> items = new ArrayList<Item>();
    private LayoutInflater inflater;
    String transNo="";

    public VoucherDetailAdapter(Context context) {
        inflater = LayoutInflater.from(context);
        mContext = context;
    }

    public boolean addItem(String balance, String crAmount, String drAmount, String fiscialYearId, String invoiceNo, String narration, String receiptNo, String tranDate, String tranMode, String voucherNumber) {

        items.add(new Item(balance, crAmount, drAmount, fiscialYearId, invoiceNo, narration, receiptNo, tranDate, tranMode, voucherNumber));
        return true;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = view;
        final Item item = (Item) getItem(i);
        TextView narration, drAmount, crAmount, balanceAmount, dr_cr, date;
        Button transTypeNo;
        if (v == null) {
            v = inflater.inflate(R.layout.voucher_detail_item, viewGroup, false);
            v.setTag(R.id.narration, v.findViewById(R.id.narration));
            v.setTag(R.id.transTypeNo, v.findViewById(R.id.transTypeNo));
            v.setTag(R.id.drAmount, v.findViewById(R.id.drAmount));
            v.setTag(R.id.crAmount, v.findViewById(R.id.crAmount));
            v.setTag(R.id.balanceAmount, v.findViewById(R.id.balanceAmount));
            v.setTag(R.id.dr_cr, v.findViewById(R.id.dr_cr));
            v.setTag(R.id.date, v.findViewById(R.id.date));

        }
        narration = (TextView) v.getTag(R.id.narration);
        transTypeNo = (Button) v.getTag(R.id.transTypeNo);
        drAmount = (TextView) v.getTag(R.id.drAmount);
        crAmount = (TextView) v.getTag(R.id.crAmount);
        balanceAmount = (TextView) v.getTag(R.id.balanceAmount);
        dr_cr = (TextView) v.getTag(R.id.dr_cr);
        date = (TextView) v.getTag(R.id.date);

        narration.setText(item.Narration);
        drAmount.setText(item.DrAmount);
        crAmount.setText(item.CrAmount);
        balanceAmount.setText(item.Balance);
        dr_cr.setText(item.TranMode);

        if (!item.InvoiceNo.equalsIgnoreCase("null") && !item.InvoiceNo.equalsIgnoreCase("")) {
            transTypeNo.setText("Bill : " + item.InvoiceNo);
            transNo=item.InvoiceNo;
        } else if (!item.ReceiptNo.equalsIgnoreCase("null") && !item.ReceiptNo.equalsIgnoreCase("")) {
            transTypeNo.setText("Receipt : " + item.ReceiptNo);
            transNo=item.VoucherNumber;
        } else {
            transTypeNo.setText("Voucher : " + item.VoucherNumber);
            transNo=item.VoucherNumber;
        }


        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat.parse(item.TranDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        date.setText(convertedDate.toString());


        transTypeNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int type=1;
                if (!item.InvoiceNo.equalsIgnoreCase("null") && !item.InvoiceNo.equalsIgnoreCase("")) {
                  type=0;
                } else {
                    transNo=item.VoucherNumber;
                    type=1;
                }
                ((VoucherDetail) mContext).nextActivity(item.FiscialYearId, transNo,type);
            }
        });
        return v;
    }

    private class Item {

        final String Balance;
        final String CrAmount;
        final String DrAmount;
        final String FiscialYearId;
        final String InvoiceNo;
        final String Narration;
        final String ReceiptNo;
        final String TranDate;
        final String TranMode;
        final String VoucherNumber;

        public Item(String balance, String crAmount, String drAmount, String fiscialYearId, String invoiceNo, String narration, String receiptNo, String tranDate, String tranMode, String voucherNumber) {
            Balance = balance;
            CrAmount = crAmount;
            DrAmount = drAmount;
            FiscialYearId = fiscialYearId;
            InvoiceNo = invoiceNo;
            Narration = narration;
            ReceiptNo = receiptNo;
            TranDate = tranDate;
            TranMode = tranMode;
            VoucherNumber = voucherNumber;
        }
    }
}
