package pas.pranav.gcm.student_related;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import pas.pranav.gcm.R;

/**
 * Created by ik890 on 7/22/2016.
 */
public class ExamExmdListAdapter extends BaseExpandableListAdapter {

    private Context mContext;
    private HashMap<GroupItem, List<ChildItem>> childItems = new HashMap<GroupItem, List<ChildItem>>();
    private List<GroupItem> groupItems = new ArrayList<GroupItem>();
    private LayoutInflater inflater;

    public ExamExmdListAdapter(Context context) {
        this.mContext = context;
    }

    public void addItem(String courseName, String examEndDate, String examNotice, String examPublishNotice, String examRecordId, String examStartDate, String examTitle, boolean isReExam, String partName, String publishDate, String yearName) {

        boolean isAdded = false;
        for (int i = 0; i < groupItems.size(); i++) {
            GroupItem item = groupItems.get(i);

            if (item.YearName.equalsIgnoreCase(yearName)) {

                List<ChildItem> childItemList = childItems.get(groupItems.get(i));

                ChildItem childItem = new ChildItem(courseName, examEndDate, examNotice, examPublishNotice, examRecordId, examStartDate, examTitle, isReExam, partName, publishDate, yearName);
                childItemList.add(childItem);
                childItems.put(groupItems.get(i), childItemList);
                isAdded = true;
            }
        }
        if (!isAdded) {

            GroupItem groupItem = new GroupItem(Integer.parseInt(examRecordId), yearName, publishDate);
            groupItems.add(groupItem);
            List<ChildItem> childItemList = new ArrayList<ChildItem>();

            ChildItem childItem = new ChildItem(courseName, examEndDate, examNotice, examPublishNotice, examRecordId, examStartDate, examTitle, isReExam, partName, publishDate, yearName);
            childItemList.add(childItem);
            childItems.put(groupItem, childItemList);
        }


    }

    String examRecordId(int groupPosition, int childPosition) {

        ChildItem item = (ChildItem) getChild(groupPosition, childPosition);

        return item.ExamRecordId;
    }


    public boolean clearItem() {
        groupItems.clear();
        return true;
    }

    @Override
    public int getGroupCount() {
        return groupItems.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return childItems.get(groupItems.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.groupItems.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return this.childItems.get(this.groupItems.get(groupPosition))
                .get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }


    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        View v = convertView;
        TextView lblListHeader;

        if (v == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = infalInflater.inflate(R.layout.list_group, parent, false);
            v.setTag(R.id.lblListHeader, v.findViewById(R.id.lblListHeader));
        }
        lblListHeader = (TextView) v.getTag(R.id.lblListHeader);

        GroupItem item = (GroupItem) getGroup(groupPosition);

        lblListHeader.setText(item.YearName);

        return v;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        View v = convertView;
        TextView lblListItem;

        if (v == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = infalInflater.inflate(R.layout.list_item, parent, false);
            v.setTag(R.id.lblListItem, v.findViewById(R.id.lblListItem));
        }
        lblListItem = (TextView) v.getTag(R.id.lblListItem);

        ChildItem item = (ChildItem) getChild(groupPosition, childPosition);

        lblListItem.setText(item.ExamTitle);

        return v;
    }


    private class GroupItem {
        final String YearName;
        final int ExamRecordId;
        String PublishDate;

        GroupItem(int id, String YearName, String PublishDate) {
            this.ExamRecordId = id;
            this.PublishDate = PublishDate;
            this.YearName = YearName;
        }
    }

    private class ChildItem {
        final String CourseName;
        final String ExamEndDate;
        final String ExamNotice;
        final String ExamPublishNotice;
        final String ExamRecordId;
        final String ExamStartDate;
        final String ExamTitle;
        final boolean IsReExam;
        final String PartName;
        final String PublishDate;
        final String YearName;

        public ChildItem(String courseName, String examEndDate, String examNotice, String examPublishNotice, String examRecordId, String examStartDate, String examTitle, boolean isReExam, String partName, String publishDate, String yearName) {
            CourseName = courseName;
            ExamEndDate = examEndDate;
            ExamNotice = examNotice;
            ExamPublishNotice = examPublishNotice;
            ExamRecordId = examRecordId;
            ExamStartDate = examStartDate;
            ExamTitle = examTitle;
            IsReExam = isReExam;
            PartName = partName;
            PublishDate = publishDate;
            YearName = yearName;
        }

    }
}
