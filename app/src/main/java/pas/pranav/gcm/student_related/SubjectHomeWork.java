package pas.pranav.gcm.student_related;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ExpandableListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import pas.pranav.gcm.MIS;
import pas.pranav.gcm.R;
import pas.pranav.gcm.helpers.API_URL;
import pas.pranav.gcm.helpers.JSONParserCustom;

public class SubjectHomeWork extends AppCompatActivity {
    private ProgressDialog progressBar;
    private ExpandableListView listview;
    SubjectHomeWorkAdapter subjectHomeWorkAdapter;
    int lastExpandedPosition = -1;

    private String url, deviceId, token;

    public static final String strUserName = "userKey";
    public static final String strPassword = "passwordKey";
    public static final String strToken = "token";
    public static final String strDeviceId = "deviceId";

    SharedPreferences.Editor editor;
    SharedPreferences sharedpreferences;
    private API_URL appURL;
    private String userType;
    private int intUserType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subject_home_work);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sharedpreferences = getSharedPreferences(MIS.MyPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();

        deviceId = sharedpreferences.getString(strDeviceId, null);
        token = sharedpreferences.getString(strToken, null);

        listview = (ExpandableListView) findViewById(R.id.mySubHomeworklistview);
        subjectHomeWorkAdapter = new SubjectHomeWorkAdapter(this);
        listview.setAdapter(subjectHomeWorkAdapter);


        appURL = new API_URL();
        userType = sharedpreferences.getString("UserType", null);
        intUserType = Integer.parseInt(userType);
        appURL = new API_URL();
        if (intUserType == 2) {
            url = appURL.ServerUrlStudent + appURL.SubjectHomeWork;
        } else if (intUserType == 3) {
            url = appURL.ServerUrlParent + appURL.SubjectHomeWork;
        }


        new menuJSONParse().execute(url);

        listview.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
                if (lastExpandedPosition != -1
                        && groupPosition != lastExpandedPosition) {
                    listview.collapseGroup(lastExpandedPosition);
                }
                lastExpandedPosition = groupPosition;
            }
        });

    }

    private class menuJSONParse extends android.os.AsyncTask<String, String, org.json.JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar = new ProgressDialog(SubjectHomeWork.this);
            progressBar.setMessage("Downloading...");
            progressBar.show();

        }

        @Override
        protected org.json.JSONObject doInBackground(String... args) {

            JSONParserCustom jParser = new JSONParserCustom();
            org.json.JSONObject json = jParser.getJSONFromUrl(url, deviceId, token);

            return json;
        }

        @Override
        protected void onPostExecute(org.json.JSONObject json) {
            try {
                if (progressBar.isShowing()) {
                    progressBar.dismiss();
                }
                JSONArray jsonArray = json.optJSONArray("List");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    String monthName = jsonObject.optString("NepaliMonthName").toString();
                    String nYearName = jsonObject.optString("NepaliYearName").toString();
                    String studentCourseRecordId = jsonObject.optString("StudentCourseRecordId").toString();
                    String yearName = jsonObject.optString("YearName").toString();

                    JSONArray jsonSubjectList = jsonObject.optJSONArray("SubjectList");

                    for (int j = 0; j < jsonSubjectList.length(); j++) {
                        JSONObject jsonSubjectListObject = jsonSubjectList.getJSONObject(j);
                        String assignmentTitle = jsonSubjectListObject.optString("AssignmentTitle").toString();
                        String grade = jsonSubjectListObject.optString("Grade").toString();
                        String subjectName = jsonSubjectListObject.optString("SubjectName").toString();
                        String summissionDate = jsonSubjectListObject.optString("SummissionDate").toString();

                        subjectHomeWorkAdapter.addItem(i, studentCourseRecordId, yearName, nYearName, monthName, assignmentTitle, grade, subjectName, summissionDate);
                    }

                }
                subjectHomeWorkAdapter.notifyDataSetChanged();

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }
}

// "SubjectList":