package pas.pranav.gcm.student_related;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import pas.pranav.gcm.R;

/**
 * Created by hem on 8/8/2016.
 */

public class ExpAttendanceReportAdapter extends BaseExpandableListAdapter {

    private Context mContext;
    private HashMap<GroupItem, List<ChildItem>> childItems = new HashMap<GroupItem, List<ChildItem>>();
    private List<GroupItem> groupItems = new ArrayList<GroupItem>();
    private LayoutInflater inflater;

    public ExpAttendanceReportAdapter(Context context) {
        this.mContext = context;
    }

    public void addItem(int sn, String day, String dayName, String status, String absentDaysDays, String absentFeePday, String leaveDays, String monthName, String persentagePresentDays, String presentDays, String totalAbsentFee, String totalWorkingDays, String yearName) {
        System.out.println(monthName +"="+day);
        boolean isAdded = false;
        for (int i = 0; i < groupItems.size(); i++) {
            GroupItem item = groupItems.get(i);

            if (item.SN == sn) {

                List<ChildItem> childItemList = childItems.get(groupItems.get(i));

                ChildItem childItem = new ChildItem(day, dayName, status);
                childItemList.add(childItem);
                childItems.put(groupItems.get(i), childItemList);
                isAdded = true;
            }
        }

        if (!isAdded) {

            GroupItem groupItem = new GroupItem(sn, absentDaysDays, absentFeePday, leaveDays, monthName, persentagePresentDays, presentDays, totalAbsentFee, totalWorkingDays, yearName);
            groupItems.add(groupItem);
            List<ChildItem> childItemList = new ArrayList<ChildItem>();

            ChildItem childItem = new ChildItem(day, dayName, status);
            childItemList.add(childItem);
            childItems.put(groupItem, childItemList);
        }


    }


    public boolean clearItem() {
        groupItems.clear();
        return true;
    }

    @Override
    public int getGroupCount() {
        return groupItems.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return childItems.get(groupItems.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.groupItems.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return this.childItems.get(this.groupItems.get(groupPosition))
                .get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }


    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        View v = convertView;
        TextView lblListHeader, lblListLeaveSts;

        if (v == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = infalInflater.inflate(R.layout.attendance_report_groupitem, parent, false);
            v.setTag(R.id.lblListHeader, v.findViewById(R.id.lblListHeader));
            v.setTag(R.id.lblListLeaveSts, v.findViewById(R.id.lblListLeaveSts));
        }
        lblListHeader = (TextView) v.getTag(R.id.lblListHeader);
        lblListLeaveSts = (TextView) v.getTag(R.id.lblListLeaveSts);

        GroupItem item = (GroupItem) getGroup(groupPosition);

        lblListHeader.setText(item.MonthName + " ( " + item.YearName + " )");
        lblListLeaveSts.setText(item.PresentDays + "/" + item.TotalWorkingDays);

        return v;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        View v = convertView;
        TextView lblListItem, lblListStatus;

        if (v == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = infalInflater.inflate(R.layout.attendance_report_childitem, parent, false);
            v.setTag(R.id.lblListItem, v.findViewById(R.id.lblListItem));
            v.setTag(R.id.lblListStatus, v.findViewById(R.id.lblListStatus));
        }
        lblListItem = (TextView) v.getTag(R.id.lblListItem);
        lblListStatus = (TextView) v.getTag(R.id.lblListStatus);

        ChildItem item = (ChildItem) getChild(groupPosition, childPosition);

        lblListItem.setText(item.Day);

        System.out.println(item.Day);

        if (!item.Status.equalsIgnoreCase("null"))
            lblListStatus.setText(item.Status);
        else
            lblListStatus.setText("-");
        return v;
    }


    private class GroupItem {
        final int SN;
        final String AbsentDaysDays;
        final String AbsentFeePday;
        final String LeaveDays;
        final String MonthName;
        final String PersentagePresentDays;
        final String PresentDays;
        final String TotalAbsentFee;
        final String TotalWorkingDays;
        final String YearName;

        public GroupItem(int sn, String absentDaysDays, String absentFeePday, String leaveDays, String monthName, String persentagePresentDays, String presentDays, String totalAbsentFee, String totalWorkingDays, String yearName) {
            SN = sn;
            AbsentDaysDays = absentDaysDays;
            AbsentFeePday = absentFeePday;
            LeaveDays = leaveDays;
            MonthName = monthName;
            PersentagePresentDays = persentagePresentDays;
            PresentDays = presentDays;
            TotalAbsentFee = totalAbsentFee;
            TotalWorkingDays = totalWorkingDays;
            YearName = yearName;
        }
    }

    private class ChildItem {
        final String Day;
        final String DayName;
        final String Status;

        public ChildItem(String day, String dayName, String status) {
            Day = day;
            DayName = dayName;
            Status = status;
        }


    }
}
