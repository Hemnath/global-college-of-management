package pas.pranav.gcm.student_related;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import pas.pranav.gcm.MIS;
import pas.pranav.gcm.R;
import pas.pranav.gcm.helpers.API_URL;
import pas.pranav.gcm.helpers.JSONParserCustom;

public class InvoiceDetail extends AppCompatActivity {

    private ProgressDialog progressBar;
    private ListView listview;
    InvoiceDetailAdapter invoiceDetailAdapter;

    private String url, deviceId, token;

    public static final String strUserName = "userKey";
    public static final String strPassword = "passwordKey";
    public static final String strToken = "token";
    public static final String strDeviceId = "deviceId";

    SharedPreferences.Editor editor;
    SharedPreferences sharedpreferences;
    private API_URL appURL;
    private String fiscialYearId, invoiceNumber;

    TextView totalBillAmount, previousBalance, payableAmount;
    private int intUserType;
    private String userType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invoice_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sharedpreferences = getSharedPreferences(MIS.MyPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();

        deviceId = sharedpreferences.getString(strDeviceId, null);
        token = sharedpreferences.getString(strToken, null);

        Intent intent = getIntent();

        invoiceNumber = intent.getStringExtra("invoiceNumber");
        fiscialYearId = intent.getStringExtra("fiscialYearId");

        totalBillAmount = (TextView) findViewById(R.id.totalBillAmount);
        previousBalance = (TextView) findViewById(R.id.previousBalance);
        payableAmount = (TextView) findViewById(R.id.payAbleAmount);

        listview = (ListView) findViewById(R.id.listview);
        invoiceDetailAdapter = new InvoiceDetailAdapter(this);
        listview.setAdapter(invoiceDetailAdapter);


        appURL = new API_URL();
        userType = sharedpreferences.getString("UserType", null);
        intUserType = Integer.parseInt(userType);
        appURL = new API_URL();
        if (intUserType == 2) {
            url = appURL.ServerUrlStudent + appURL.Invoice + invoiceNumber + appURL.fiscialYearId + fiscialYearId;
        } else if (intUserType == 3) {
            url = appURL.ServerUrlParent + appURL.Invoice + invoiceNumber + appURL.fiscialYearId + fiscialYearId;
        }
        new reportCardJSONParse().execute(url);

    }

    public void setData(String billAmount, String prevBalance, String payAmount) {
        totalBillAmount.setText(billAmount);
        previousBalance.setText(prevBalance);
        payableAmount.setText(payAmount);
    }


    private class reportCardJSONParse extends android.os.AsyncTask<String, String, org.json.JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar = new ProgressDialog(InvoiceDetail.this);
            progressBar.setMessage("Downloading...");
            progressBar.show();

        }

        @Override
        protected org.json.JSONObject doInBackground(String... args) {

            JSONParserCustom jParser = new JSONParserCustom();
            org.json.JSONObject json = jParser.getJSONFromUrl(url, deviceId, token);

            return json;
        }

        @Override
        protected void onPostExecute(org.json.JSONObject json) {
            String data = "";
            try {
                if (progressBar.isShowing()) {
                    progressBar.dismiss();
                }

                String jsonCard = json.optString("Invoice");

                JSONObject jsonObject = new JSONObject(jsonCard);

                String CreatedDate = jsonObject.optString("CreatedDate").toString();
                String CreatedByName = jsonObject.optString("CreatedByName").toString();
                String MessageToStudent = jsonObject.optString("MessageToStudent").toString();
                String LevelAlias = jsonObject.optString("LevelAlias").toString();
                String PreviousBalance = jsonObject.optString("PreviousBalance").toString();
                String TotalBillAmount = jsonObject.optString("TotalBillAmount").toString();
                String TotalPayAbleAmount = jsonObject.optString("TotalPayAbleAmount").toString();

                setData(TotalBillAmount, PreviousBalance, TotalPayAbleAmount);

                JSONArray subjectArray = json.optJSONArray("ItemList");
                for (int j = 0; j < subjectArray.length(); j++) {
                    JSONObject subjectObject = subjectArray.getJSONObject(j);

                    String Particular = subjectObject.optString("Particular").toString();
                    String Rate = subjectObject.optString("Rate").toString();

                    invoiceDetailAdapter.addItem(Integer.toString(j), Particular, Rate);
                }


                invoiceDetailAdapter.notifyDataSetChanged();

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }
}

