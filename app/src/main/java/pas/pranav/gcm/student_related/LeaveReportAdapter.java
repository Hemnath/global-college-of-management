package pas.pranav.gcm.student_related;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import pas.pranav.gcm.R;

/**
 * Created by hem on 8/8/2016.
 */
public class LeaveReportAdapter extends BaseAdapter {
    private Context mContext;
    private List<Item> items = new ArrayList<Item>();
    private LayoutInflater inflater;

    public LeaveReportAdapter(Context context) {
        inflater = LayoutInflater.from(context);
        mContext = context;

    }

    public void addItem(String approveRejecteByName, String fromDate, String toDate, String leaveSubject, String parentComment, String requestDetail, String statusName, String teacherComment, int requestStatus) {
        items.add(new Item( approveRejecteByName, fromDate,  toDate,  leaveSubject,  parentComment,  requestDetail,  statusName,  teacherComment, requestStatus));

    }


    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = view;
        TextView title;
        TextView body,fromDate, toDate,approveBy;

        if (v == null) {
            v = inflater.inflate(R.layout.leave_report_item, viewGroup, false);
            v.setTag(R.id.listTitle, v.findViewById(R.id.listTitle));
            v.setTag(R.id.listBody, v.findViewById(R.id.listBody));
            v.setTag(R.id.fromDate, v.findViewById(R.id.fromDate));
            v.setTag(R.id.toDate, v.findViewById(R.id.toDate));
            v.setTag(R.id.approveBy, v.findViewById(R.id.approveBy));

        }

        title = (TextView) v.getTag(R.id.listTitle);
        body = (TextView) v.getTag(R.id.listBody);
        fromDate = (TextView) v.getTag(R.id.fromDate);
        toDate = (TextView) v.getTag(R.id.toDate);
        approveBy = (TextView) v.getTag(R.id.approveBy);

        Item item = (Item) getItem(i);



        title.setText(item.LeaveSubject);
        approveBy.setText(item.ApproveRejecteByName);
        body.setText(item.RequestDetail.replace("</p>","").replace("<p>",""));

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat.parse(item.FromDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if(convertedDate.toString().length() > 11)
            fromDate.setText(convertedDate.toString().substring(0,11));

        try {
            convertedDate = dateFormat.parse(item.ToDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if(convertedDate.toString().length() > 11)
           toDate.setText(convertedDate.toString().substring(0,11));


        return v;
    }

    private class Item {
        final String ApproveRejecteByName, FromDate,ToDate, LeaveSubject,ParentComment,RequestDetail,StatusName,TeacherComment;
        int RequestStatus;

        public Item(String approveRejecteByName, String fromDate, String toDate, String leaveSubject, String parentComment, String requestDetail, String statusName, String teacherComment, int requestStatus) {
            ApproveRejecteByName = approveRejecteByName;
            FromDate = fromDate;
            ToDate = toDate;
            LeaveSubject = leaveSubject;
            ParentComment = parentComment;
            RequestDetail = requestDetail;
            StatusName = statusName;
            TeacherComment = teacherComment;
            RequestStatus = requestStatus;
        }
    }
}
