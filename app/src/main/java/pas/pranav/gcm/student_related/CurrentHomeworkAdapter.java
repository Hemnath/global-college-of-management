package pas.pranav.gcm.student_related;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import pas.pranav.gcm.R;

/**
 * Created by ik890 on 7/29/2016.
 */
public class CurrentHomeworkAdapter extends BaseAdapter {
    private Context mContext;
    private List<Item> items = new ArrayList<Item>();
    private LayoutInflater inflater;

    public CurrentHomeworkAdapter(Context context) {
        inflater = LayoutInflater.from(context);
        mContext = context;

    }

    public void addItem(String assignmentTitle, String instruction, String subjectName, String summissionDate) {
        items.add(new Item( assignmentTitle,  instruction,  subjectName,  summissionDate));



    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = view;
        TextView title;
        TextView body, date,subject;

        if (v == null) {
            v = inflater.inflate(R.layout.current_homework_item, viewGroup, false);
            v.setTag(R.id.listTitle, v.findViewById(R.id.listTitle));
            v.setTag(R.id.listBody, v.findViewById(R.id.listBody));
            v.setTag(R.id.listDate, v.findViewById(R.id.listDate));
            v.setTag(R.id.listSubject, v.findViewById(R.id.listSubject));

        }

        title = (TextView) v.getTag(R.id.listTitle);
        body = (TextView) v.getTag(R.id.listBody);
        date = (TextView) v.getTag(R.id.listDate);
        subject = (TextView) v.getTag(R.id.listSubject);

        Item item = (Item) getItem(i);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat.parse(item.SummissionDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        title.setText(item.AssignmentTitle);
        body.setText(item.Instruction);
        date.setText(convertedDate.toString());
        subject.setText(item.SubjectName);

        return v;
    }

    private class Item {
        final String AssignmentTitle;
        final String Instruction;
        final String SubjectName;
        final String SummissionDate;

        public Item(String assignmentTitle, String instruction, String subjectName, String summissionDate) {
            AssignmentTitle = assignmentTitle;
            Instruction = instruction;
            SubjectName = subjectName;
            SummissionDate = summissionDate;
        }
    }
}
