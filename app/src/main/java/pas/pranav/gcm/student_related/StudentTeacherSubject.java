package pas.pranav.gcm.student_related;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.GridView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import pas.pranav.gcm.MIS;
import pas.pranav.gcm.R;
import pas.pranav.gcm.helpers.API_URL;
import pas.pranav.gcm.helpers.JSONParserCustom;

/**
 * Created by ik890 on 7/14/2016.
 */
public class StudentTeacherSubject extends AppCompatActivity {

    private GridView gridView;
    private StudentTeacherSubjectAdapter teacherSubjectAdapter;

    private String url, deviceId, token;

    public static final String strUserName = "userKey";
    public static final String strPassword = "passwordKey";
    public static final String strToken = "token";
    public static final String strDeviceId = "deviceId";

    SharedPreferences.Editor editor;
    SharedPreferences sharedpreferences;

    private ProgressDialog progressBar;
    private API_URL appURL;
    private int intUserType;
    private String userType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher_subject);
       Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sharedpreferences = getSharedPreferences(MIS.MyPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();
        userType = sharedpreferences.getString("UserType", null);
        intUserType = Integer.parseInt(userType);

        deviceId= sharedpreferences.getString(strDeviceId, null);
        token = sharedpreferences.getString(strToken, null);

        gridView=(GridView) findViewById(R.id.itemGridView);
        teacherSubjectAdapter=new StudentTeacherSubjectAdapter(this);
        gridView.setAdapter(teacherSubjectAdapter);

        appURL = new API_URL();
        if (intUserType == 2) {
            url = appURL.ServerUrlStudent+appURL.TeacherSubjectClass;
        } else if (intUserType == 3) {
            url = appURL.ServerUrlParent + appURL.TeacherSubjectClass;
        }
        new menuJSONParse().execute();

    }

    public boolean messageDialog(String TeacherEmail, String TeacherName,String TeacherPhoneNo) {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.teacher_faculty_detail, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setTitle(TeacherName);

        final TextView phone = (TextView) dialogView.findViewById(R.id.phone);
        phone.setText(TeacherPhoneNo);

        final TextView email = (TextView) dialogView.findViewById(R.id.email);
        email.setText(TeacherEmail);

        dialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }

        });
        AlertDialog b = dialogBuilder.create();
        b.show();
        return true;
    }
    private class menuJSONParse extends android.os.AsyncTask<String, String, org.json.JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar = new ProgressDialog(StudentTeacherSubject.this);
            progressBar.setMessage("Downloading...");
            progressBar.show();

        }

        @Override
        protected org.json.JSONObject doInBackground(String... args) {

            JSONParserCustom jParser = new JSONParserCustom();
            org.json.JSONObject json = jParser.getJSONFromUrl(url, deviceId, token);

            return json;
        }

        @Override
        protected void onPostExecute(org.json.JSONObject json) {
            String data = "";
            try {
                if (progressBar.isShowing()) {
                    progressBar.dismiss();
                }
                JSONArray jsonArray = json.optJSONArray("List");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    String academicYearTitle = jsonObject.optString("AcademicYearTitle").toString();
                    String classDates = jsonObject.optString("ClassDates").toString();
                    String classDays = jsonObject.optString("ClassDays").toString();
                    String classTimes = jsonObject.optString("ClassTimes").toString();
                    String courseTitle = jsonObject.optString("CourseTitle").toString();

                    String isCompleted = jsonObject.optString("IsCompleted").toString();
                    String partId = jsonObject.optString("PartId").toString();
                    String section = jsonObject.optString("Section").toString();
                    String shiftTitle = jsonObject.optString("ShiftTitle").toString();
                    String subjectTitle = jsonObject.optString("SubjectTitle").toString();
                    String yearAlias = jsonObject.optString("YearAlias").toString();
                    String yearId = jsonObject.optString("YearId").toString();

                    String employeeRecordId = jsonObject.optString("EmployeeRecordId").toString();
                    String teacherEmail = jsonObject.optString("TeacherEmail").toString();
                    String teacherName = jsonObject.optString("TeacherName").toString();
                    String teacherPhoneNo = jsonObject.optString("TeacherPhoneNo").toString();

                    teacherSubjectAdapter.addItem(academicYearTitle, classDates, classDays, classTimes, courseTitle, isCompleted, section, partId, shiftTitle, yearAlias, subjectTitle, yearId, employeeRecordId, teacherEmail, teacherName, teacherPhoneNo);
                    teacherSubjectAdapter.notifyDataSetChanged();
                }
                teacherSubjectAdapter.notifyDataSetChanged();

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }
    private class TeacherDetailJSONParse extends android.os.AsyncTask<String, String, org.json.JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar = new ProgressDialog(StudentTeacherSubject.this);
            progressBar.setMessage("Downloading...");
            progressBar.show();

        }

        @Override
        protected org.json.JSONObject doInBackground(String... args) {

            JSONParserCustom jParser = new JSONParserCustom();
            org.json.JSONObject json = jParser.getJSONFromUrl(url, deviceId, token);

            return json;
        }

        @Override
        protected void onPostExecute(org.json.JSONObject json) {
            String data = "";
            if (progressBar.isShowing()) {
                progressBar.dismiss();
            }
            String Award = json.optString("Award").toString();
            String BookArticles = json.optString("BookArticles").toString();
            String EmailAddress = json.optString("EmailAddress").toString();
            String EmployeeName = json.optString("EmployeeName").toString();
            String EmployeeRecordId = json.optString("EmployeeRecordId").toString();

            String LatestDegree = json.optString("LatestDegree").toString();
            String MobileNo = json.optString("MobileNo").toString();
            String NoOfExperence = json.optString("NoOfExperence").toString();
            String PermanentAddress = json.optString("PermanentAddress").toString();
            String PhoneNo = json.optString("PhoneNo").toString();
            String TemporaryAddress = json.optString("TemporaryAddress").toString();

        }
    }

}
