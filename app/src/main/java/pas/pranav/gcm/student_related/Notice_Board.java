package pas.pranav.gcm.student_related;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import pas.pranav.gcm.MIS;
import pas.pranav.gcm.R;
import pas.pranav.gcm.admin.AdminMessageItem;
import pas.pranav.gcm.helpers.API_URL;
import pas.pranav.gcm.helpers.JSONParserCustom;
import pas.pranav.gcm.helpers.NoticeBoardAdapter;

/**
 * Created by dev on 11/4/15.
 */

public class Notice_Board extends AppCompatActivity {

    private ListView listview;
    private NoticeBoardAdapter noticeAdapter;

    private ProgressDialog progressBar;
    private String url, deviceId, token;

    public static final String strUserName = "userKey";
    public static final String strPassword = "passwordKey";
    public static final String strToken = "token";
    public static final String strDeviceId = "deviceId";

    SharedPreferences.Editor editor;
    SharedPreferences sharedpreferences;
    private API_URL appURL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_notice_board);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sharedpreferences = getSharedPreferences(MIS.MyPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();

        deviceId = sharedpreferences.getString(strDeviceId, null);
        token = sharedpreferences.getString(strToken, null);

        listview = (ListView) findViewById(R.id.listview);
        noticeAdapter = new NoticeBoardAdapter(this);
        listview.setAdapter(noticeAdapter);

        appURL =new API_URL();
        url = appURL.ServerUrlStudent+appURL.NoticeBoard;

        new menuJSONParse().execute(url);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                android.content.Intent intent = new android.content.Intent(Notice_Board.this, StudentMessageReportItem.class);
                intent.putExtra("c_date", noticeAdapter.getCDate(position));
                intent.putExtra("summary", noticeAdapter.getNoticeSubject(position));
                intent.putExtra("description", noticeAdapter.getNoticeDetail(position));
                startActivity(intent);
            }
        });

    }

    private class menuJSONParse extends android.os.AsyncTask<String, String, org.json.JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar = new ProgressDialog(Notice_Board.this);
            progressBar.setMessage("Downloading...");
            progressBar.show();

        }

        @Override
        protected org.json.JSONObject doInBackground(String... args) {

            JSONParserCustom jParser = new JSONParserCustom();
            org.json.JSONObject json = jParser.getJSONFromUrl(url, deviceId, token);

            return json;
        }

        @Override
        protected void onPostExecute(org.json.JSONObject json) {
            String data = "";
            try {
                if (progressBar.isShowing()) {
                    progressBar.dismiss();
                }
                JSONArray jsonArray = json.optJSONArray("List");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    int RecordsId = jsonObject.getInt("NoticBoardRecordsId");
                    String NoticeDetail = jsonObject.optString("NoticeDetail").toString();
                    String NoticeSubject = jsonObject.optString("NoticeSubject").toString();

                    noticeAdapter.addItem(RecordsId, NoticeSubject, NoticeDetail, "");
                }
                noticeAdapter.notifyDataSetChanged();

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }
}