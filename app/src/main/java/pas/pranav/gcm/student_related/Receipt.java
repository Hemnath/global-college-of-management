package pas.pranav.gcm.student_related;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import pas.pranav.gcm.MIS;
import pas.pranav.gcm.R;
import pas.pranav.gcm.helpers.API_URL;
import pas.pranav.gcm.helpers.JSONParserCustom;

public class Receipt extends AppCompatActivity {

    private ListView listview;
    ReceiptAdapter receiptAdapter;
    private String url, deviceId, token;

    public static final String strUserName = "userKey";
    public static final String strPassword = "passwordKey";
    public static final String strToken = "token";
    public static final String strDeviceId = "deviceId";

    SharedPreferences.Editor editor;
    SharedPreferences sharedpreferences;

    private API_URL appURL;
    private String userType;
    private int intUserType;
    private String invoiceNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receipt);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sharedpreferences = getSharedPreferences(MIS.MyPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();

        deviceId = sharedpreferences.getString(strDeviceId, null);
        token = sharedpreferences.getString(strToken, null);

        Intent intent = getIntent();
        invoiceNumber = intent.getStringExtra("invoiceNumber");

        listview = (ListView) findViewById(R.id.listview);
        receiptAdapter = new ReceiptAdapter(this);
        listview.setAdapter(receiptAdapter);

        appURL = new API_URL();
        userType = sharedpreferences.getString("UserType", null);
        intUserType = Integer.parseInt(userType);
        if (intUserType == 2) {
            url = appURL.ServerUrlStudent + appURL.Receipt + invoiceNumber;
        } else if (intUserType == 3) {
            url = appURL.ServerUrlParent + appURL.Receipt + invoiceNumber;
        }
        new ReceiptJSONParse().execute(url);

    }

    private class ReceiptJSONParse extends android.os.AsyncTask<String, String, org.json.JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected org.json.JSONObject doInBackground(String... args) {

            JSONParserCustom jParser = new JSONParserCustom();
            org.json.JSONObject json = jParser.getJSONFromUrl(url, deviceId, token);

            System.out.print(json);

            return json;
        }

        @Override
        protected void onPostExecute(org.json.JSONObject json) {
            String data = "";
            try {
                String jsonCard = json.optString("Receipt");

                JSONObject jsonObject = new JSONObject(jsonCard);

                String narration = jsonObject.optString("Narration").toString();
                String accountNo = jsonObject.optString("AccountNo").toString();
                String amountPaid = jsonObject.optString("AmountPaid").toString();
                String discountAmount = jsonObject.getString("DiscountAmount");
                String currentBalance = jsonObject.optString("CurrentBalance").toString();
                String previousBalance = jsonObject.optString("PreviousBalance").toString();
                String receiptDate = jsonObject.optString("ReceiptDate").toString();

                receiptAdapter.addItem(narration, accountNo, amountPaid, discountAmount, currentBalance, previousBalance, receiptDate);
                receiptAdapter.notifyDataSetChanged();

            } catch (JSONException e1) {
                e1.printStackTrace();
            }
        }
    }
}
