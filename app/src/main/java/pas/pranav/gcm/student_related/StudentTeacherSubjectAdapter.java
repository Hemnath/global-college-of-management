package pas.pranav.gcm.student_related;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import pas.pranav.gcm.R;

/**
 * Created by ik890 on 7/14/2016.
 */
public class StudentTeacherSubjectAdapter extends BaseAdapter {
    private Context mContext;
    private List<Item> items = new ArrayList<Item>();
    private LayoutInflater inflater;
    Button teacherName;
    TextView  subjectTitle, classDays, classTimes;

    public StudentTeacherSubjectAdapter(Context context) {
        inflater = LayoutInflater.from(context);
        mContext = context;
    }

    public boolean addItem(String academicYearTitle, String classDates, String classDays, String classTimes, String courseTitle, String isCompleted, String section, String partId, String shiftTitle, String yearAlias, String subjectTitle, String yearId, String employeeRecordId, String teacherEmail, String teacherName, String teacherPhoneNo) {

        items.add(new Item(academicYearTitle, classDates, classDays, classTimes, courseTitle, isCompleted, section, partId, shiftTitle, yearAlias, subjectTitle, yearId, employeeRecordId, teacherEmail, teacherName, teacherPhoneNo));
        return true;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = view;
        final Item item = (Item) getItem(i);

        if (v == null) {
            v = inflater.inflate(R.layout.student_teacher_sub_item, viewGroup, false);
            v.setTag(R.id.teacherName, v.findViewById(R.id.teacherName));
            v.setTag(R.id.subjectTitle, v.findViewById(R.id.subjectTitle));
            v.setTag(R.id.classDays, v.findViewById(R.id.classDays));
            v.setTag(R.id.classTimes, v.findViewById(R.id.classTimes));

        }
        teacherName = (Button) v.getTag(R.id.teacherName);
        subjectTitle = (TextView) v.getTag(R.id.subjectTitle);
        classDays = (TextView) v.getTag(R.id.classDays);
        classTimes = (TextView) v.getTag(R.id.classTimes);

        teacherName.setText(item.TeacherName);
        subjectTitle.setText(item.SubjectTitle);
        classDays.setText(item.ClassDays);
        classTimes.setText(item.ClassTimes);

        teacherName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((StudentTeacherSubject) mContext).messageDialog(item.TeacherEmail,item.TeacherName,item.TeacherPhoneNo);
            }
        });
        return v;
    }

    private class Item {

        String AcademicYearTitle, ClassDates, ClassDays, ClassTimes, CourseTitle, IsCompleted, PartId, Section, ShiftTitle, SubjectTitle, YearAlias, YearId, EmployeeRecordId, TeacherEmail, TeacherName, TeacherPhoneNo;

        public Item(String academicYearTitle, String classDates, String classDays, String classTimes, String courseTitle, String isCompleted, String section, String partId, String shiftTitle, String yearAlias, String subjectTitle, String yearId, String employeeRecordId, String teacherEmail, String teacherName, String teacherPhoneNo) {

            System.out.println(subjectTitle);

            this.AcademicYearTitle = academicYearTitle;
            this.ClassDates = classDates;
            this.ClassDays = classDays;
            this.ClassTimes = classTimes;
            this.CourseTitle = courseTitle;
            this.IsCompleted = isCompleted;
            this.Section = section;
            this.PartId = partId;
            this.ShiftTitle = shiftTitle;
            this.YearAlias = yearAlias;
            this.SubjectTitle = subjectTitle;
            this.YearId = yearId;
            this.EmployeeRecordId = employeeRecordId;
            this.TeacherEmail = teacherEmail;
            this.TeacherName = teacherName;
            this.TeacherPhoneNo = teacherPhoneNo;
        }


    }
}
