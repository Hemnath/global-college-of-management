package pas.pranav.gcm.student_related;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import pas.pranav.gcm.MIS;
import pas.pranav.gcm.R;
import pas.pranav.gcm.helpers.API_URL;
import pas.pranav.gcm.helpers.JSONParserCustom;

public class DeleteLeave extends AppCompatActivity {

    private ProgressDialog progressBar;
    private ListView listview;
    DeleteLeaveAdapter deleteLeaveAdapter;

    private String url, deviceId, token;

    public static final String strUserName = "userKey";
    public static final String strPassword = "passwordKey";
    public static final String strToken = "token";
    public static final String strDeviceId = "deviceId";

    SharedPreferences.Editor editor;
    SharedPreferences sharedpreferences;
    private API_URL appURL;
    private Intent myIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete_leave);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sharedpreferences = getSharedPreferences(MIS.MyPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();

        deviceId = sharedpreferences.getString(strDeviceId, null);
        token = sharedpreferences.getString(strToken, null);

        listview = (ListView) findViewById(R.id.listview);
        deleteLeaveAdapter = new DeleteLeaveAdapter(this);
        listview.setAdapter(deleteLeaveAdapter);

        appURL = new API_URL();
        url = appURL.ServerUrlStudent + appURL.LeaveReport;

        new menuJSONParse().execute(url);

    }

    public void deleteLeaveItem(String leaveId) {

        url = appURL.ServerUrlStudent + appURL.DeleteLeave + leaveId;
        new DetailLeaveJSONParse().execute(url);

    }
    public void editLeaveItem(String leaveId) {
        myIntent = new Intent(DeleteLeave.this, EditLeave.class);
        startActivity(myIntent);

    }

    private class menuJSONParse extends android.os.AsyncTask<String, String, org.json.JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar = new ProgressDialog(DeleteLeave.this);
            progressBar.setMessage("Downloading...");
            progressBar.show();

        }

        @Override
        protected org.json.JSONObject doInBackground(String... args) {

            JSONParserCustom jParser = new JSONParserCustom();
            org.json.JSONObject json = jParser.getJSONFromUrl(url, deviceId, token);

            return json;
        }

        @Override
        protected void onPostExecute(org.json.JSONObject json) {
            String data = "";
            try {
                if (progressBar.isShowing()) {
                    progressBar.dismiss();
                }
                JSONArray jsonArray = json.optJSONArray("List");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);


                    int requestStatus = jsonObject.getInt("RequestStatus");

                    String approveRejecteByName = jsonObject.optString("ApproveRejecteByName").toString();
                    String fromDate = jsonObject.optString("FromDate").toString();
                    String leaveSubject = jsonObject.optString("LeaveSubject").toString();
                    String parentComment = jsonObject.optString("ParentComment").toString();
                    String requestDetail = jsonObject.optString("RequestDetail").toString();
                    String StudentLeaveRecordId = jsonObject.optString("StudentLeaveRecordId").toString();
                    String statusName = jsonObject.optString("StatusName").toString();
                    String teacherComment = jsonObject.optString("TeacherComment").toString();
                    String toDate = jsonObject.optString("ToDate").toString();

                    deleteLeaveAdapter.addItem(approveRejecteByName, fromDate, toDate, leaveSubject, parentComment, requestDetail, statusName, teacherComment, requestStatus, StudentLeaveRecordId);
                }
                deleteLeaveAdapter.notifyDataSetChanged();

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private class DetailLeaveJSONParse extends android.os.AsyncTask<String, String, org.json.JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar = new ProgressDialog(DeleteLeave.this);
            progressBar.setMessage("Deleting...");
            progressBar.show();

        }

        @Override
        protected org.json.JSONObject doInBackground(String... args) {

            JSONParserCustom jParser = new JSONParserCustom();

            System.out.println("URL = " + url);
            org.json.JSONObject json = jParser.getJSONFromUrl(url, deviceId, token);

            return json;
        }

        @Override
        protected void onPostExecute(org.json.JSONObject json) {
            String data = "";
            if (progressBar.isShowing()) {
                progressBar.dismiss();
            }
            String Message = json.optString("Message").toString();
            String MsgType = json.optString("MsgType").toString();

            url = appURL.ServerUrlStudent + appURL.LeaveReport;

        }
    }
}
