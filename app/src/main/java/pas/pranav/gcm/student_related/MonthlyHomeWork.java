package pas.pranav.gcm.student_related;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ExpandableListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import pas.pranav.gcm.MIS;
import pas.pranav.gcm.R;
import pas.pranav.gcm.helpers.API_URL;
import pas.pranav.gcm.helpers.JSONParserCustom;

public class MonthlyHomeWork extends AppCompatActivity {
    private ProgressDialog progressBar;
    private ExpandableListView listview;
    MonthlyHomeWorkAdapter monthlyHomeWorkAdapter ;
    int lastExpandedPosition = -1;

    private String url, deviceId, token;

    public static final String strUserName = "userKey";
    public static final String strPassword = "passwordKey";
    public static final String strToken = "token";
    public static final String strDeviceId = "deviceId";

    SharedPreferences.Editor editor;
    SharedPreferences sharedpreferences;
    private API_URL appURL;
    private String userType;
    private int intUserType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_monthly_home_work);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sharedpreferences = getSharedPreferences(MIS.MyPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();

        deviceId = sharedpreferences.getString(strDeviceId, null);
        token = sharedpreferences.getString(strToken, null);
        appURL = new API_URL();
        userType = sharedpreferences.getString("UserType", null);
        intUserType = Integer.parseInt(userType);

        listview = (ExpandableListView) findViewById(R.id.mySubHomeworklistview);
        monthlyHomeWorkAdapter = new MonthlyHomeWorkAdapter(this);
        listview.setAdapter(monthlyHomeWorkAdapter);

        if (intUserType == 2) {
            url = appURL.ServerUrlStudent + appURL.MonthlyHomeWork;
        } else if (intUserType == 3) {
            url = appURL.ServerUrlParent + appURL.MonthlyHomeWork;
        }

        new menuJSONParse().execute(url);

        listview.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
                if (lastExpandedPosition != -1
                        && groupPosition != lastExpandedPosition) {
                    listview.collapseGroup(lastExpandedPosition);
                }
                lastExpandedPosition = groupPosition;
            }
        });

    }

    private class menuJSONParse extends android.os.AsyncTask<String, String, org.json.JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar = new ProgressDialog(MonthlyHomeWork.this);
            progressBar.setMessage("Downloading...");
            progressBar.show();

        }

        @Override
        protected org.json.JSONObject doInBackground(String... args) {

            JSONParserCustom jParser = new JSONParserCustom();
            org.json.JSONObject json = jParser.getJSONFromUrl(url, deviceId, token);

            return json;
        }

        @Override
        protected void onPostExecute(org.json.JSONObject json) {
            try {
                if (progressBar.isShowing()) {
                    progressBar.dismiss();
                }
                JSONArray jsonArray = json.optJSONArray("List");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    String monthName = jsonObject.optString("MonthName").toString();
                    String nYearName = jsonObject.optString("NepaliYear").toString();
                    String classTeacherGrad = jsonObject.optString("ClassTeacherGrad").toString();
                    String teacherGrad = jsonObject.optString("TeacherGrad").toString();
                    String universityBordRegNo = jsonObject.optString("UniversityBordRegNo").toString();
                    String yearName = jsonObject.optString("YearName").toString();
                    String totalAssignment = jsonObject.optString("TotalAssignment").toString();
                    String totalSubmitted = jsonObject.optString("TotalSubmitted").toString();

                    JSONArray jsonSubjectList = jsonObject.optJSONArray("SubjectReportList");

                    for (int j = 0; j < jsonSubjectList.length(); j++) {
                        JSONObject jsonSubjectListObject = jsonSubjectList.getJSONObject(j);
                        String subjectName = jsonSubjectListObject.optString("SubjectName").toString();
                        String subTotalAssignment = jsonSubjectListObject.optString("TotalAssignment").toString();
                        String subTotalSubmitted = jsonSubjectListObject.optString("TotalSubmitted").toString();

                        monthlyHomeWorkAdapter.addItem(i,  monthName,  nYearName,  classTeacherGrad,  teacherGrad,  universityBordRegNo,  yearName,  totalAssignment,  totalSubmitted, subjectName,  subTotalAssignment, subTotalSubmitted);
                    }

                }
                monthlyHomeWorkAdapter.notifyDataSetChanged();

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }
}