package pas.pranav.gcm.student_related;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ExpandableListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import pas.pranav.gcm.MIS;
import pas.pranav.gcm.R;
import pas.pranav.gcm.helpers.API_URL;
import pas.pranav.gcm.helpers.JSONParserCustom;

public class AttendamceReport extends AppCompatActivity {
    private ProgressDialog progressBar;
    private ExpandableListView listview;
    ExpAttendanceReportAdapter expAttendanceReportAdapter;
    int lastExpandedPosition = -1;

    private String url, deviceId, token;

    public static final String strUserName = "userKey";
    public static final String strPassword = "passwordKey";
    public static final String strToken = "token";
    public static final String strDeviceId = "deviceId";

    SharedPreferences.Editor editor;
    SharedPreferences sharedpreferences;
    private API_URL appURL;
    private Intent myIntent;
    private int intUserType;
    private String userType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendamce_report);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sharedpreferences = getSharedPreferences(MIS.MyPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();

        deviceId = sharedpreferences.getString(strDeviceId, null);
        token = sharedpreferences.getString(strToken, null);

        listview = (ExpandableListView) findViewById(R.id.myExamlistview);
        expAttendanceReportAdapter = new ExpAttendanceReportAdapter(this);
        listview.setAdapter(expAttendanceReportAdapter);

        userType = sharedpreferences.getString("UserType", null);
        intUserType = Integer.parseInt(userType);
        appURL = new API_URL();
        if (intUserType == 2) {
            url = appURL.ServerUrlStudent + appURL.AttendamceReport;
        } else if (intUserType == 3) {
            url = appURL.ServerUrlParent + appURL.AttendamceReport;
        }


        new menuJSONParse().execute(url);

        listview.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
                if (lastExpandedPosition != -1
                        && groupPosition != lastExpandedPosition) {
                    listview.collapseGroup(lastExpandedPosition);
                }
                lastExpandedPosition = groupPosition;
            }
        });

    }


    private class menuJSONParse extends android.os.AsyncTask<String, String, org.json.JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar = new ProgressDialog(AttendamceReport.this);
            progressBar.setMessage("Downloading...");
            progressBar.show();

        }

        @Override
        protected org.json.JSONObject doInBackground(String... args) {

            JSONParserCustom jParser = new JSONParserCustom();
            org.json.JSONObject json = jParser.getJSONFromUrl(url, deviceId, token);

            return json;
        }

        @Override
        protected void onPostExecute(org.json.JSONObject json) {
            String data = "";
            try {
                if (progressBar.isShowing()) {
                    progressBar.dismiss();
                }

                JSONArray jsonArray = json.optJSONArray("List");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    String absentDaysDays = jsonObject.optString("AbsentDaysDays").toString();
                    String absentFeePday = jsonObject.optString("AbsentFeePday").toString();
                    String leaveDays = jsonObject.optString("LeaveDays").toString();
                    String monthName = jsonObject.getString("MonthName");
                    String persentagePresentDays = jsonObject.optString("PersentagePresentDays").toString();
                    String presentDays = jsonObject.optString("PresentDays").toString();
                    String totalAbsentFee = jsonObject.optString("TotalAbsentFee").toString();
                    String totalWorkingDays = jsonObject.optString("TotalWorkingDays").toString();
                    String yearName = jsonObject.optString("YearName").toString();


                    JSONArray jsonArrayDay = jsonObject.optJSONArray("DayList");
                    for (int j = 0; j < jsonArrayDay.length(); j++) {
                        JSONObject jsonObjectDay = jsonArrayDay.getJSONObject(j);

                        String day = jsonObjectDay.optString("Day").toString();
                        String dayName = jsonObjectDay.optString("DayName").toString();
                        String status = jsonObjectDay.optString("Status").toString();

                        expAttendanceReportAdapter.addItem(i, day,  dayName,  status,  absentDaysDays,  absentFeePday,  leaveDays,  monthName,  persentagePresentDays,  presentDays,  totalAbsentFee,  totalWorkingDays, yearName);

                    }

                }
                expAttendanceReportAdapter.notifyDataSetChanged();

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }
}
