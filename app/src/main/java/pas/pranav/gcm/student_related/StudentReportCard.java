package pas.pranav.gcm.student_related;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import pas.pranav.gcm.MIS;
import pas.pranav.gcm.R;
import pas.pranav.gcm.helpers.API_URL;
import pas.pranav.gcm.helpers.JSONParserCustom;
import pas.pranav.gcm.helpers.MessageViewAdapter;

public class StudentReportCard extends AppCompatActivity {

    private ProgressDialog progressBar;
    private ListView listview;
    SubjectMarksAdapter subjectMarksAdapter;

    private String url, deviceId, token;

    public static final String strUserName = "userKey";
    public static final String strPassword = "passwordKey";
    public static final String strToken = "token";
    public static final String strDeviceId = "deviceId";

    SharedPreferences.Editor editor;
    SharedPreferences sharedpreferences;
    private API_URL appURL;
    private String examRecordId;

    TextView txtName, txtRollNo, txtCourse, txtShift, txtLevel, txtSection, txtExamType;
    TextView txtAcademicYear, txtResult, txtPercentage, txtDivision, txtRank, txtTotalWorkingDays, txtTotalPresentDays;
    private String userType;
    private int intUserType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_report_card);

        sharedpreferences = getSharedPreferences(MIS.MyPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();

        deviceId = sharedpreferences.getString(strDeviceId, null);
        token = sharedpreferences.getString(strToken, null);
        userType = sharedpreferences.getString("UserType", null);
        intUserType = Integer.parseInt(userType);

        Intent intent = getIntent();

        examRecordId = intent.getStringExtra("examRecordId");

        txtName = (TextView) findViewById(R.id.name);
        txtRollNo = (TextView) findViewById(R.id.rollNo);
        txtCourse = (TextView) findViewById(R.id.course);
        txtShift = (TextView) findViewById(R.id.shift);
        txtLevel = (TextView) findViewById(R.id.level);
        txtSection = (TextView) findViewById(R.id.section);
        txtExamType = (TextView) findViewById(R.id.examType);
        txtAcademicYear = (TextView) findViewById(R.id.academicYear);
        txtResult = (TextView) findViewById(R.id.result);
        txtPercentage = (TextView) findViewById(R.id.percentage);
        txtDivision = (TextView) findViewById(R.id.division);
        txtRank = (TextView) findViewById(R.id.rank);
        txtTotalWorkingDays = (TextView) findViewById(R.id.totalWorkingDays);
        txtTotalPresentDays = (TextView) findViewById(R.id.totalPresentDays);

        listview = (ListView) findViewById(R.id.listview);
        subjectMarksAdapter = new SubjectMarksAdapter(this);
        listview.setAdapter(subjectMarksAdapter);

        appURL = new API_URL();
        if (intUserType == 2) {
            url = appURL.ServerUrlStudent + appURL.MyReportCard + examRecordId;
        } else if (intUserType == 3) {
            url = appURL.ServerUrlParent + appURL.MyReportCard + examRecordId;
        }
        new reportCardJSONParse().execute(url);

    }

    public void setData(String name, String rollNo, String course, String shift, String level, String section, String examType, String academicYear, String result, String percentage, String division, String rank, String totalWorkingDays, String totalPresentDays) {
        txtName.setText(name);
        txtRollNo.setText(rollNo);
        txtCourse.setText(course);
        txtShift.setText(shift);
        txtLevel.setText(level);
        txtSection.setText(section);
        txtExamType.setText(examType);
        txtAcademicYear.setText(academicYear);
        txtResult.setText(result);
        txtPercentage.setText(percentage);
        txtDivision.setText(division);
        txtRank.setText(rank);
        txtTotalWorkingDays.setText(totalWorkingDays);
        txtTotalPresentDays.setText(totalPresentDays);

    }


    private class reportCardJSONParse extends android.os.AsyncTask<String, String, org.json.JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar = new ProgressDialog(StudentReportCard.this);
            progressBar.setMessage("Downloading...");
            progressBar.show();

        }

        @Override
        protected org.json.JSONObject doInBackground(String... args) {

            JSONParserCustom jParser = new JSONParserCustom();
            org.json.JSONObject json = jParser.getJSONFromUrl(url, deviceId, token);

            return json;
        }

        @Override
        protected void onPostExecute(org.json.JSONObject json) {
            String data = "";
            try {
                if (progressBar.isShowing()) {
                    progressBar.dismiss();
                }

                String jsonCard = json.optString("Card");

                JSONObject jsonObject = new JSONObject(jsonCard);

                String StudentName = jsonObject.optString("StudentCourseRecordName").toString();
                String RollNo = jsonObject.optString("RollNo").toString();
                String AcademicYearTitle = jsonObject.optString("AcademicYearCourseRecordTitle").toString();
                String ShiftName = jsonObject.optString("ShiftName").toString();
                String LevelAlias = jsonObject.optString("LevelAlias").toString();
                String Section = jsonObject.optString("Section").toString();
                String ExamRecordTitle = jsonObject.optString("ExamRecordTitle").toString();
                String AcademicYearRecordTitle = jsonObject.optString("AcademicYearRecordTitle").toString();


                String Result = jsonObject.optString("Result").toString();
                String ObtainedGraid = jsonObject.optString("ObtainedGraid").toString();
                String SubjectRank = jsonObject.optString("SubjectRank").toString();
                String TotalPercentage = jsonObject.optString("TotalPercentage").toString();
                String TotalPresentDays = jsonObject.optString("TotalPresentDays").toString();
                String TotalWorkingDays = jsonObject.optString("TotalWorkingDays").toString();

                String Remarks = jsonObject.optString("Remarks").toString();


                setData(StudentName, RollNo, AcademicYearTitle, ShiftName, LevelAlias, Section, ExamRecordTitle, AcademicYearRecordTitle, Result, TotalPercentage, ObtainedGraid, SubjectRank, TotalWorkingDays, TotalPresentDays);

                JSONArray subjectArray = jsonObject.optJSONArray("SubjectList");

                if (subjectArray!=null) {
                    for (int j = 0; j < subjectArray.length(); j++) {
                        JSONObject subjectObject = subjectArray.getJSONObject(j);

                        String Subject = subjectObject.optString("AcademicYearCourseSubjectRecordTitle").toString();
                        String FullMarks = subjectObject.optString("FullMarks").toString();
                        String PassMarks = subjectObject.optString("PassMark").toString();
                        String MarksObtained = subjectObject.optString("ObtainedMarks").toString();
                        String MaxObtainedMarks = subjectObject.optString("MaxObtainedMarks").toString();

                        subjectMarksAdapter.addItem(Subject, FullMarks, PassMarks, MarksObtained, MaxObtainedMarks);
                    }
                }
                subjectMarksAdapter.notifyDataSetChanged();

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }
}

