package pas.pranav.gcm.student_related;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import pas.pranav.gcm.MIS;
import pas.pranav.gcm.R;
import pas.pranav.gcm.helpers.API_URL;

public class EditLeave extends AppCompatActivity {

    private Calendar calendar;
    private int year, month, day;
    Button btnDate1, btnDate2, leaveRqst, LeaveReport, leaveDelete;
    String subject, details, strFrom, strTo;
    TextView etSubject, etDetails;
    private API_URL appURL;

    private String deviceId, token;

    public static final String strUserName = "userKey";
    public static final String strPassword = "passwordKey";
    public static final String strToken = "token";
    public static final String strDeviceId = "deviceId";

    SharedPreferences.Editor editor;
    SharedPreferences sharedpreferences;
    private Intent myIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_leave);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sharedpreferences = getSharedPreferences(MIS.MyPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();

        deviceId = sharedpreferences.getString(strDeviceId, null);
        token = sharedpreferences.getString(strToken, null);

        btnDate1 = (Button) findViewById(R.id.button1);
        btnDate2 = (Button) findViewById(R.id.button2);

        LeaveReport = (Button) findViewById(R.id.LeaveReport);
        leaveDelete = (Button) findViewById(R.id.leaveDelete);

        leaveRqst = (Button) findViewById(R.id.leaveRequest);
        etSubject = (TextView) findViewById(R.id.etSubject);
        etDetails = (TextView) findViewById(R.id.etDesc);

        appURL = new API_URL();

        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);

        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        showDate(year, month + 1, day, btnDate1);
        showDate(year, month + 1, day, btnDate2);

        leaveRqst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                subject = etSubject.getText().toString();
                details = etDetails.getText().toString();
                strFrom = btnDate1.getText().toString();
                strTo = btnDate2.getText().toString();

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                Date convertedDate = new Date();

                try {
                    convertedDate = dateFormat.parse(strFrom);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                strFrom=convertedDate.toString();

                try {
                    convertedDate = dateFormat.parse(strTo);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                strTo=convertedDate.toString();

                new EditLeaveParse().execute();
            }
        });
    }

    @SuppressWarnings("deprecation")
    public void setDate(View view) {
        showDialog(999);
    }

    @SuppressWarnings("deprecation")
    public void setDate1(View view) {
        showDialog(888);
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        // TODO Auto-generated method stub
        if (id == 999) {
            return new DatePickerDialog(this, myDateListener, year, month, day);
        } else if (id == 888) {
            return new DatePickerDialog(this, myDateListener1, year, month, day);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub

            showDate(arg1, arg2 + 1, arg3, btnDate1);
        }
    };

    private DatePickerDialog.OnDateSetListener myDateListener1 = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
            showDate(arg1, arg2 + 1, arg3, btnDate2);
        }
    };

    private void showDate(int year, int month, int day, Button btn) {
        btn.setText(new StringBuilder().append(day).append("/")
                .append(month).append("/").append(year));
    }

    private class EditLeaveParse extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(Void... params) {

            InputStream inputStream = null;
            String result = "";

            String strUrl = appURL.ServerUrlStudent + appURL.CreateLeave;

            try {
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(strUrl);
                String json = "";
                JSONObject jsonObject = new JSONObject();

                jsonObject.accumulate("ApproveRejecteByName", "");
                jsonObject.accumulate("FromDate", "/Date(928128900000+0545)/");
                jsonObject.accumulate("LeaveSubject", subject);
                jsonObject.accumulate("ParentComment", "");
                jsonObject.accumulate("RequestDetail", details);
                jsonObject.accumulate("RequestStatus", 1);
                jsonObject.accumulate("StatusName", "");
                jsonObject.accumulate("StudentLeaveRecordId", 2);
                jsonObject.accumulate("TeacherComment", "");
                jsonObject.accumulate("ToDate", "/Date(928128900000+0545)/");


                json = jsonObject.toString();
                System.out.println(" Request of JSON : " + json);
                StringEntity se = new StringEntity(json);

                httpPost.setEntity(se);

                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");
                httpPost.setHeader("Token", token);
                httpPost.setHeader("DeviceId", deviceId);


                HttpResponse httpResponse = httpclient.execute(httpPost);

                inputStream = httpResponse.getEntity().getContent();

                if (inputStream != null)
                    result = convertInputStreamToString(inputStream);

                System.out.println(" Result of JSON : " + result);

                return result;

            } catch (Exception e) {
                System.out.println(" Result of JSON : " + e);
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {

            try {
                JSONObject json = new JSONObject(result);

                String Message = json.optString("Message").toString();
                String MsgType = json.optString("MsgType").toString();

                if (Message.equalsIgnoreCase("Success") && MsgType.equalsIgnoreCase("Success")) {
                    Toast.makeText(getApplicationContext(), "Message Send " + Message,
                            Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), Message,
                            Toast.LENGTH_SHORT).show();
                }
            } catch (
                    JSONException e
                    )

            {
                e.printStackTrace();
            }
        }
    }

    private String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder out = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            out.append(line);
        }
        reader.close();
        return out.toString();
    }

}
