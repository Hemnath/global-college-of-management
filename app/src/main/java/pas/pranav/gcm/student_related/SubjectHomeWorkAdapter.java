package pas.pranav.gcm.student_related;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import pas.pranav.gcm.R;

public class SubjectHomeWorkAdapter extends BaseExpandableListAdapter {

    private Context mContext;
    private HashMap<GroupItem, List<ChildItem>> childItems = new HashMap<GroupItem, List<ChildItem>>();
    private List<GroupItem> groupItems = new ArrayList<GroupItem>();
    private LayoutInflater inflater;

    public SubjectHomeWorkAdapter(Context context) {
        this.mContext = context;
    }

    public void addItem(int sn, String id, String yearName, String nYearName, String monthName, String assignmentTitle, String grade, String subjectName, String summissionDate) {

        boolean isAdded = false;
        for (int i = 0; i < groupItems.size(); i++) {
            GroupItem item = groupItems.get(i);

            if (item.sn==sn) {

                List<ChildItem> childItemList = childItems.get(groupItems.get(i));

                ChildItem childItem = new ChildItem(assignmentTitle, grade, subjectName, summissionDate);
                childItemList.add(childItem);
                childItems.put(groupItems.get(i), childItemList);
                isAdded = true;
            }
        }
        if (!isAdded) {

            GroupItem groupItem = new GroupItem(sn, Integer.parseInt(id), yearName, nYearName, monthName);
            groupItems.add(groupItem);
            List<ChildItem> childItemList = new ArrayList<ChildItem>();

            ChildItem childItem = new ChildItem(assignmentTitle, grade, subjectName, summissionDate);
            childItemList.add(childItem);
            childItems.put(groupItem, childItemList);
        }


    }

    public boolean clearItem() {
        groupItems.clear();
        return true;
    }

    @Override
    public int getGroupCount() {
        return groupItems.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return childItems.get(groupItems.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.groupItems.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return this.childItems.get(this.groupItems.get(groupPosition))
                .get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }


    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        View v = convertView;
        TextView lblListHeader;

        if (v == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = infalInflater.inflate(R.layout.groupitem_subject_home_work, parent, false);
            v.setTag(R.id.lblListHeader, v.findViewById(R.id.lblListHeader));
        }
        lblListHeader = (TextView) v.getTag(R.id.lblListHeader);

        GroupItem item = (GroupItem) getGroup(groupPosition);

        lblListHeader.setText(item.YearName + " " + item.MonthName + " ( " + item.NYearName + " ) ");

        return v;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        View v = convertView;
        TextView subjectName,title,grade;

        if (v == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = infalInflater.inflate(R.layout.childitem_subject_home_work, parent, false);
            v.setTag(R.id.lblListSubjectName, v.findViewById(R.id.lblListSubjectName));
            v.setTag(R.id.lblListTitle, v.findViewById(R.id.lblListTitle));
            v.setTag(R.id.lblListGrade, v.findViewById(R.id.lblListGrade));
        }
        subjectName = (TextView) v.getTag(R.id.lblListSubjectName);
        title = (TextView) v.getTag(R.id.lblListTitle);
        grade = (TextView) v.getTag(R.id.lblListGrade);

        ChildItem item = (ChildItem) getChild(groupPosition, childPosition);

        subjectName.setText(item.SubjectName);
        title.setText(item.AssignmentTitle);
        if (item.Grade.equalsIgnoreCase("null"))
            grade.setText("-");
        else
            grade.setText(item.Grade);

        return v;
    }


    private class GroupItem {

        final String YearName;
        final String MonthName;
        final int StudentRecordId;
        final int sn;
        final String NYearName;

        GroupItem(int sn, int id, String yearName, String nYearName, String monthName) {
            this.sn = sn;
            this.StudentRecordId = id;
            this.YearName = nYearName;
            this.NYearName = yearName;
            this.MonthName = monthName;
        }
    }

    private class ChildItem {
        public ChildItem(String assignmentTitle, String grade, String subjectName, String summissionDate) {
            AssignmentTitle = assignmentTitle;
            Grade = grade;
            SubjectName = subjectName;
            SummissionDate = summissionDate;
        }

        final String AssignmentTitle;
        final String Grade;
        final String SubjectName;
        final String SummissionDate;


    }
}
