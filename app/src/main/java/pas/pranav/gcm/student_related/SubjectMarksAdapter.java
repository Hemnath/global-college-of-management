package pas.pranav.gcm.student_related;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import pas.pranav.gcm.R;

/**
 * Created by ik890 on 7/21/2016.
 */
public class SubjectMarksAdapter extends BaseAdapter {
    private Context mContext;
    private List<Item> items = new ArrayList<Item>();
    private LayoutInflater inflater;


    public SubjectMarksAdapter(Context context) {
        inflater = LayoutInflater.from(context);
        mContext = context;
    }

    public boolean addItem(String subject, String fullMarks, String passMarks, String marksObtained, String highestMarks) {

        items.add(new Item(subject, fullMarks, passMarks, marksObtained, highestMarks));
        return true;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = view;
        final Item item = (Item) getItem(i);
        TextView subject, fullMarks, passMarks, marksObtained, highestmarks;

        if (v == null) {
            v = inflater.inflate(R.layout.subject_marks_list, viewGroup, false);
            v.setTag(R.id.subject, v.findViewById(R.id.subject));
            v.setTag(R.id.fullMarks, v.findViewById(R.id.fullMarks));
            v.setTag(R.id.passMarks, v.findViewById(R.id.passMarks));
            v.setTag(R.id.marksObtained, v.findViewById(R.id.marksObtained));
            v.setTag(R.id.highestmarks, v.findViewById(R.id.highestmarks));

        }
        subject = (TextView) v.getTag(R.id.subject);
        fullMarks = (TextView) v.getTag(R.id.fullMarks);
        passMarks = (TextView) v.getTag(R.id.passMarks);
        marksObtained = (TextView) v.getTag(R.id.marksObtained);
        highestmarks = (TextView) v.getTag(R.id.highestmarks);

        subject.setText(item.Subject);
        fullMarks.setText(item.FullMarks);
        passMarks.setText(item.PassMarks);
        marksObtained.setText(item.MarksObtained);
        highestmarks.setText(item.HighestMarks);

        return v;
    }

    private class Item {

        public Item(String subject, String fullMarks, String passMarks, String marksObtained, String highestMarks) {
            this.Subject = subject;
            this.FullMarks = fullMarks;
            this.PassMarks = passMarks;
            this.MarksObtained = marksObtained;
            this.HighestMarks = highestMarks;
        }

        String Subject, FullMarks, PassMarks, MarksObtained, HighestMarks;


    }
}
