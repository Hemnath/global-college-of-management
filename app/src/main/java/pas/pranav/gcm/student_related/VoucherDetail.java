package pas.pranav.gcm.student_related;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import pas.pranav.gcm.MIS;
import pas.pranav.gcm.R;
import pas.pranav.gcm.helpers.API_URL;
import pas.pranav.gcm.helpers.JSONParserCustom;

public class VoucherDetail extends AppCompatActivity {
    private ProgressDialog progressBar;
    private ListView listview;
    VoucherDetailAdapter voucherDetailAdapter;

    private String url, deviceId, token;

    public static final String strUserName = "userKey";
    public static final String strPassword = "passwordKey";
    public static final String strToken = "token";
    public static final String strDeviceId = "deviceId";

    SharedPreferences.Editor editor;
    SharedPreferences sharedpreferences;
    private API_URL appURL;
    private Intent myIntent;
    private String userType;
    private int intUserType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voucher_detail);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sharedpreferences = getSharedPreferences(MIS.MyPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();

        deviceId = sharedpreferences.getString(strDeviceId, null);
        token = sharedpreferences.getString(strToken, null);

        listview = (ListView) findViewById(R.id.listview);
        voucherDetailAdapter = new VoucherDetailAdapter(this);
        listview.setAdapter(voucherDetailAdapter);


        appURL = new API_URL();
        userType = sharedpreferences.getString("UserType", null);
        intUserType = Integer.parseInt(userType);
        if (intUserType == 2) {
            url = appURL.ServerUrlStudent + appURL.LedgerReport;
        } else if (intUserType == 3) {
            url = appURL.ServerUrlParent + appURL.LedgerReport;
        }

        new menuJSONParse().execute(url);

    }

    public void nextActivity(String FiscialYearId, String InvoiceNo, int type) {
        if (type == 0) {
            myIntent = new Intent(getApplicationContext(), InvoiceDetail.class);
        } else {
            myIntent = new Intent(getApplicationContext(), Receipt.class);
        }
        myIntent.putExtra("fiscialYearId", FiscialYearId);
        myIntent.putExtra("invoiceNumber", InvoiceNo);
        startActivity(myIntent);
    }

    private class menuJSONParse extends android.os.AsyncTask<String, String, org.json.JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar = new ProgressDialog(VoucherDetail.this);
            progressBar.setMessage("Downloading...");
            progressBar.show();

        }

        @Override
        protected org.json.JSONObject doInBackground(String... args) {

            JSONParserCustom jParser = new JSONParserCustom();
            org.json.JSONObject json = jParser.getJSONFromUrl(url, deviceId, token);

            return json;
        }

        @Override
        protected void onPostExecute(org.json.JSONObject json) {
            String data = "";
            try {
                if (progressBar.isShowing()) {
                    progressBar.dismiss();
                }
                JSONArray jsonArray = json.optJSONArray("List");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    String balance = jsonObject.optString("Balance").toString();
                    String crAmount = jsonObject.optString("CrAmount").toString();
                    String drAmount = jsonObject.optString("DrAmount").toString();
                    String fiscialYearId = jsonObject.getString("FiscialYearId");
                    String invoiceNo = jsonObject.optString("InvoiceNo").toString();
                    String narration = jsonObject.optString("Narration").toString();
                    String receiptNo = jsonObject.optString("ReceiptNo").toString();
                    String tranDate = jsonObject.optString("TranDate").toString();
                    String tranMode = jsonObject.optString("TranMode").toString();
                    String voucherNumber = jsonObject.optString("VoucherNumber").toString();

                    voucherDetailAdapter.addItem(balance, crAmount, drAmount, fiscialYearId, invoiceNo, narration, receiptNo, tranDate, tranMode, voucherNumber);

                }
                voucherDetailAdapter.notifyDataSetChanged();

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }
}
