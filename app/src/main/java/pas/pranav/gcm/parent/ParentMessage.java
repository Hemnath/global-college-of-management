package pas.pranav.gcm.parent;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import pas.pranav.gcm.MIS;
import pas.pranav.gcm.R;
import pas.pranav.gcm.admin.AdminMessageItem;
import pas.pranav.gcm.helpers.API_URL;
import pas.pranav.gcm.helpers.JSONParserCustom;
import pas.pranav.gcm.helpers.MessageViewAdapter;
import pas.pranav.gcm.student_related.StudentMessageReportItem;

/**
 * Created by ik890 on 7/14/2016.
 */
public class ParentMessage extends AppCompatActivity {

    private ProgressDialog progressBar;
    private ListView listview;
    MessageViewAdapter messageAdapter;

    private String url, deviceId, token;

    public static final String strUserName = "userKey";
    public static final String strPassword = "passwordKey";
    public static final String strToken = "token";
    public static final String strDeviceId = "deviceId";

    SharedPreferences.Editor editor;
    SharedPreferences sharedpreferences;
    private API_URL appURL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher_message);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sharedpreferences = getSharedPreferences(MIS.MyPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();

        deviceId= sharedpreferences.getString(strDeviceId, null);
        token = sharedpreferences.getString(strToken, null);

        listview=(ListView)findViewById(R.id.listview);
        messageAdapter=new MessageViewAdapter(this);
        listview.setAdapter(messageAdapter);

        appURL =new API_URL();
        url = appURL.ServerUrlParent+appURL.Message;

        new menuJSONParse().execute(url);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                android.content.Intent intent = new android.content.Intent(ParentMessage.this, StudentMessageReportItem.class);
                intent.putExtra("c_date", messageAdapter.getCDate(position));
                intent.putExtra("summary", messageAdapter.getMessageSubject(position));
                intent.putExtra("description", messageAdapter.getMessageBody(position));
                startActivity(intent);
            }
        });

    }

    private class menuJSONParse extends android.os.AsyncTask<String, String, org.json.JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar = new ProgressDialog(ParentMessage.this);
            progressBar.setMessage("Downloading...");
            progressBar.show();

        }

        @Override
        protected org.json.JSONObject doInBackground(String... args) {

            JSONParserCustom jParser = new JSONParserCustom();
            org.json.JSONObject json = jParser.getJSONFromUrl(url, deviceId, token);

            return json;
        }

        @Override
        protected void onPostExecute(org.json.JSONObject json) {
            String data = "";
            try {
                if (progressBar.isShowing()) {
                    progressBar.dismiss();
                }
                JSONArray jsonArray = json.optJSONArray("MessageList");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    boolean IsRead = jsonObject.getBoolean("IsRead");
                    int RecordsId = jsonObject.getInt("MessageInboxRecordsId");
                    String MessageSubject = jsonObject.optString("MessageSubject").toString();
                    String MessageBody = jsonObject.optString("MessageBody").toString();
                    String MenuSuggestion = jsonObject.optString("CreatedByName").toString();
                    String CreatedDate = jsonObject.optString("CreatedDate").toString();

                    messageAdapter.addItem(IsRead, RecordsId, MessageSubject, MessageBody, CreatedDate);
                }
                messageAdapter.notifyDataSetChanged();

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }
}