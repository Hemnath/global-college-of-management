package pas.pranav.gcm;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.media.RingtoneManager;
import android.net.Uri;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.HashMap;

import pas.pranav.gcm.helpers.DBHelper;
import pas.pranav.gcm.helpers.JSONParser;
import pas.pranav.gcm.helpers.NotificationListViewAdapter;

public class Notifications extends AppCompatActivity {
    org.json.JSONObject jsonobject;
    org.json.JSONArray jsonarray;
    android.widget.ListView listview;
    NotificationListViewAdapter adapter;
    java.util.ArrayList<java.util.HashMap<String, String>> arraylist;
    private ProgressDialog progressBar;
    private DBHelper dbHelper;

    @Override
    public void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listview_main);
        android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(pas.pranav.gcm.R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        dbHelper = new DBHelper(this);
        DBDataInsert();
    }

    private void DBDataInsert() {
        java.util.Calendar c = java.util.Calendar.getInstance(java.util.TimeZone.getDefault());
        int month = c.get(c.MONTH) + 1;
        String getDate = c.get(c.YEAR) + "-" + month + "-" + c.get(c.DAY_OF_MONTH);
        if (isConnected()) {
            new DownloadJSON().execute();
            progressBar = new ProgressDialog(this);
            progressBar.setMessage("Downloading...");
            progressBar.setCanceledOnTouchOutside(false);
            progressBar.show();
        } else {
            arraylist = new java.util.ArrayList<HashMap<String, String>>();
            ArrayList<Integer> array_list;
            array_list = dbHelper.getAllNotifications();
            int count = array_list.size();
            for (int i = 0; i < count; i++) {
                java.util.HashMap<String, String> map = new java.util.HashMap<String, String>();
                Cursor rs = dbHelper.getNotifications(array_list.get(i));
                rs.moveToFirst();

                map.put("title", rs.getString(rs.getColumnIndex("title")));
                map.put("summary", rs.getString(rs.getColumnIndex("summary")));
                map.put("description", rs.getString(rs.getColumnIndex("description")));
                map.put("c_date", rs.getString(rs.getColumnIndex("created_date")));

                arraylist.add(map);
                rs.close();

            }
            listview = (android.widget.ListView) findViewById(R.id.listview);
            adapter = new NotificationListViewAdapter(Notifications.this, arraylist);
            listview.setAdapter(adapter);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.refresh_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {

            if (isConnected()) {
                DBDataInsert();
            } else {
                showAlertDialog(this, "No Internet Connection",
                        "You don't have internet connection.", false);
            }

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    boolean isConnected() {
        ConnectivityManager cm = (ConnectivityManager) this.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
        return isConnected;
    }

    public void showAlertDialog(Context context, String title, String message, Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();

        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setIcon((status) ? R.drawable.error : R.drawable.error);
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alertDialog.show();
    }

    private class DownloadJSON extends android.os.AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            // Create an array
            arraylist = new java.util.ArrayList<HashMap<String, String>>();
            pas.pranav.gcm.helpers.API_URL url = new pas.pranav.gcm.helpers.API_URL();
            jsonobject = JSONParser.getJSONFromUrl(url.notificationUrl1);
            progressBar.cancel();

            try {
                jsonarray = jsonobject.getJSONArray("notifications");
                dbHelper.deleteAllNotifications();
                for (int i = 0; i < jsonarray.length(); i++) {
                    java.util.HashMap<String, String> map = new java.util.HashMap<String, String>();
                    jsonobject = jsonarray.getJSONObject(i);

                    map.put("title", jsonobject.getString("title"));
                    map.put("summary", jsonobject.getString("summary"));
                    map.put("description", jsonobject.getString("description"));
                    map.put("c_date", jsonobject.getString("created_date"));

                    String dateMatch = jsonobject.getString("created_date");
                    String title = jsonobject.getString("title");
                    String summary = jsonobject.getString("summary");

                    dbHelper.insertNotifications(Integer.parseInt(jsonobject.getString("id")), title, summary, jsonobject.getString("description"), "1", jsonobject.getString("created_date"), jsonobject.getString("created_date"));
                    arraylist.add(map);
                }
            } catch (org.json.JSONException e) {
                android.util.Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void args) {
            listview = (android.widget.ListView) findViewById(R.id.listview);
            adapter = new NotificationListViewAdapter(Notifications.this, arraylist);
            listview.setAdapter(adapter);
        }
    }

    @Override
    public void onBackPressed() {
        Intent launchNextActivity = new Intent(Notifications.this, MainActivity.class);
        launchNextActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        launchNextActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        launchNextActivity.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(launchNextActivity);
    }
}