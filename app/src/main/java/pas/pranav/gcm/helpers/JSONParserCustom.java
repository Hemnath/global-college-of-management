package pas.pranav.gcm.helpers;

public class JSONParserCustom {

    static java.io.InputStream is = null;
    static org.json.JSONObject jObj = null;
    static String json = "";

    // constructor
    public JSONParserCustom() {

    }

    public org.json.JSONObject getJSONFromUrl(String url, String uid, String token) {
        System.out.println("json parser called... ");
        // Making HTTP request
        try {
            // defaultHttpClient
            org.apache.http.impl.client.DefaultHttpClient httpClient = new org.apache.http.impl.client.DefaultHttpClient();
            org.apache.http.client.methods.HttpGet httpGet = new org.apache.http.client.methods.HttpGet(url);

            httpGet.setHeader("Token", token.replace('"', ' '));
            httpGet.setHeader("UserId", uid);
            System.out.println(token.replace('"', ' '));
            org.apache.http.HttpResponse httpResponse = httpClient.execute(httpGet);
            org.apache.http.HttpEntity httpEntity = httpResponse.getEntity();
            is = httpEntity.getContent();

        } catch (java.io.UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (org.apache.http.client.ClientProtocolException e) {
            e.printStackTrace();
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }

        try {
            java.io.BufferedReader reader = new java.io.BufferedReader(new java.io.InputStreamReader(is, "utf-8"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "n");
            }
            is.close();
            json = sb.toString();
            System.out.println("contents of http request in first JSON parse : " + json);
        } catch (Exception e) {
            android.util.Log.e("Buffer Error", "Error converting result " + e.toString());
        }

        // try parse the string to a JSON object
        try {
            jObj = new org.json.JSONObject(json);
        } catch (org.json.JSONException e) {
            android.util.Log.e("JSON Parser", "Error parsing data " + e.toString());
        }

        // return JSON String
        return jObj;

    }
}