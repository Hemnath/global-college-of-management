package pas.pranav.gcm.helpers;

/**
 * Created by hemnath on 3/2/2016.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

public class DBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "GCM.db";
    public static final String TABLE_NAME_Notifications = "notifications";
    public static final String TABLE_NAME_Publications = "publications";
    public static final String TABLE_NAME_NewsEvents = "newsevents";
    public static final String TABLE_NAME_AboutUs = "aboutUs";
    public static final String COLUMN_ID = "id";

    public static final String COLUMN_NotifID = "notifId";
    public static final String COLUMN_Title = "title";
    public static final String COLUMN_Menu_Name = "menu_name";
    public static final String COLUMN_Menu_Title = "menu_title";
    public static final String COLUMN_Summary = "summary";
    public static final String COLUMN_Description = "description";
    public static final String COLUMN_Status = "status";
    public static final String COLUMN_Sreated_date = "created_date";
    public static final String COLUMN_Updated_date = "updated_date";
    public static final String COLUMN_FileName = "file_name";
    public static final String COLUMN_File = "file";
    public static final String COLUMN_Image = "image";
    public static final String COLUMN_ImageName = "image_name";




    public static final String COLUMN_PublicationsID = "publicationsId";
    public static final String COLUMN_NewsEventsID = "newsId";


    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        db.execSQL(
                "create table notifications" +
                        "(id integer primary key,notifId integer, title text, summary text, description text,status text, created_date text, updated_date text)"
        );

        db.execSQL(
                "create table publications" +
                        "(id integer primary key, publicationsId integer, title text, summary text, file_name text, file text, image text, status text, created_date text, updated_date text)"
        );

        db.execSQL(
                "create table newsevents" +
                        "(id integer primary key, newsId integer, title text, summary text, description text, image text, status text, created_date text, updated_date text)"
        );

        db.execSQL(
                "create table aboutUs" +
                        "(id integer primary key, menu_name text, menu_title text, summary text, description text, image text, image_name text, file_name text, file text, status text, created_date text, updated_date text)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        db.execSQL("DROP TABLE IF EXISTS notifications");
        db.execSQL("DROP TABLE IF EXISTS publications");
        db.execSQL("DROP TABLE IF EXISTS newsevents");
        db.execSQL("DROP TABLE IF EXISTS aboutUs");
        onCreate(db);
    }

    public boolean insertNotifications(int notifId, String title, String summary, String description, String status, String created_date, String updated_date) {
        SQLiteDatabase dbRead = this.getReadableDatabase();
        Boolean have = false;
        // onUpgrade(dbRead,1,2);
        int notificationsId, ID = 2;
        Cursor res = dbRead.rawQuery("select * from notifications", null);
        res.moveToFirst();
        while (res.isAfterLast() == false) {
            notificationsId = res.getInt(res.getColumnIndex(COLUMN_NotifID));
            if (notificationsId == notifId) {
                have = true;
                ID = res.getInt(res.getColumnIndex(COLUMN_ID));
            }
            res.moveToNext();
        }
        res.close();
        if (have) {
            return updateNotifications(ID, title, summary, description, status, created_date, updated_date);
        } else {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put("notifId", notifId);
            contentValues.put("title", title);
            contentValues.put("summary", summary);
            contentValues.put("description", description);
            contentValues.put("status", status);
            contentValues.put("created_date", created_date);
            contentValues.put("updated_date", updated_date);
            db.insert("notifications", null, contentValues);
            return true;
        }

    }

    public boolean insertPublications(int publicationsId, String title, String summary, String file_name, String file, String image, String status, String created_date, String updated_date) {
        SQLiteDatabase dbRead = this.getReadableDatabase();
        Boolean have = false;
        // onUpgrade(dbRead,1,2);
        int publicatId, ID = 2;
        Cursor res = dbRead.rawQuery("select * from publications", null);
        res.moveToFirst();
        while (res.isAfterLast() == false) {
            publicatId = res.getInt(res.getColumnIndex(COLUMN_PublicationsID));
            if (publicatId == publicationsId) {
                have = true;
                ID = res.getInt(res.getColumnIndex(COLUMN_ID));
            }
            res.moveToNext();
        }
        res.close();
        if (have) {
            return updatePublications(ID, title, summary, file_name, file, image, status, created_date, updated_date);
        } else {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put("publicationsId", publicationsId);
            contentValues.put("title", title);
            contentValues.put("summary", summary);
            contentValues.put("file_name", file_name);
            contentValues.put("file", file);
            contentValues.put("image", image);
            contentValues.put("status", status);
            contentValues.put("created_date", created_date);
            contentValues.put("updated_date", updated_date);
            db.insert("publications", null, contentValues);
            return true;
        }
    }

    public boolean insertNewsEvents(int newsId, String title, String summary, String description, String image, String status, String created_date, String updated_date) {
        SQLiteDatabase dbRead = this.getReadableDatabase();
        Boolean have = false;
        // onUpgrade(dbRead,1,2);
        int nId, ID = 2;
        Cursor res = dbRead.rawQuery("select * from newsevents", null);
        res.moveToFirst();
        while (res.isAfterLast() == false) {
            nId = res.getInt(res.getColumnIndex(COLUMN_NewsEventsID));
            if (nId == newsId) {
                have = true;
                ID = res.getInt(res.getColumnIndex(COLUMN_ID));
            }
            res.moveToNext();
        }
        res.close();
        if (have) {
            return updateNewsEvents(ID, title, summary, description, image, status, created_date, updated_date);
        } else {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put("newsId", newsId);
            contentValues.put("title", title);
            contentValues.put("summary", summary);
            contentValues.put("description", description);
            contentValues.put("image", image);
            contentValues.put("status", status);
            contentValues.put("created_date", created_date);
            contentValues.put("updated_date", updated_date);
            db.insert("newsevents", null, contentValues);
            return true;
        }

    }

    public boolean insertAboutUs(String menu_name, String menu_title, String summary, String description, String image, String image_name, String file_name, String file, String status, String created_date, String updated_date) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("menu_name", menu_name);
        contentValues.put("menu_title", menu_title);
        contentValues.put("summary", summary);
        contentValues.put("description", description);
        contentValues.put("file_name", file_name);
        contentValues.put("image_name", image_name);
        contentValues.put("file", file);
        contentValues.put("image", image);
        contentValues.put("status", status);
        contentValues.put("created_date", created_date);
        contentValues.put("updated_date", updated_date);
        db.insert("aboutUs", null, contentValues);
        return true;
    }


    public boolean updateNotifications(int Id, String title, String summary, String description, String status, String created_date, String updated_date) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("title", title);
        contentValues.put("summary", summary);
        contentValues.put("description", description);
        contentValues.put("status", status);
        contentValues.put("created_date", created_date);
        contentValues.put("updated_date", updated_date);
        db.update("notifications", contentValues, "id = ? ", new String[]{Integer.toString(Id)});
        return true;
    }

    public boolean updatePublications(int Id, String title, String summary, String file_name, String file, String image, String status, String created_date, String updated_date) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("title", title);
        contentValues.put("summary", summary);
        contentValues.put("file_name", file_name);
        contentValues.put("file", file);
        contentValues.put("image", image);
        contentValues.put("status", status);
        contentValues.put("created_date", created_date);
        contentValues.put("updated_date", updated_date);
        db.update("publications", contentValues, "id = ? ", new String[]{Integer.toString(Id)});
        return true;
    }

    public boolean updateNewsEvents(int Id, String title, String summary, String description, String image, String status, String created_date, String updated_date) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("newsId", Id);
        contentValues.put("title", title);
        contentValues.put("summary", summary);
        contentValues.put("description", description);
        contentValues.put("image", image);
        contentValues.put("status", status);
        contentValues.put("created_date", created_date);
        contentValues.put("updated_date", updated_date);
        db.update("newsevents", contentValues, "id = ? ", new String[]{Integer.toString(Id)});
        return true;
    }

    public Integer deleteNotifications(Integer id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete("notifications",
                "id = ? ",
                new String[]{Integer.toString(id)});
    }
    public void deleteAllNotifications() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from notifications", null);
        res.moveToFirst();
        while (res.isAfterLast() == false) {
            db.delete("notifications", "id = ? ", new String[]{res.getString(res.getColumnIndex(DBHelper.COLUMN_ID))});
            res.moveToNext();
        }
        res.close();
    }

    public ArrayList<Integer> getAllNotifications() {
        ArrayList<Integer> array_list = new ArrayList<Integer>();

        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from notifications", null);
        res.moveToFirst();

        while (res.isAfterLast() == false) {
            array_list.add(res.getInt(res.getColumnIndex(COLUMN_ID)));
            res.moveToNext();
        }
        res.close();
        return array_list;
    }

    public Cursor getNotifications(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from notifications where id=" + Integer.toString(id) + "", null);
        return res;
    }

    public Cursor getAboutUs(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from aboutUs where id=" + Integer.toString(id) + "", null);
        return res;
    }

    public ArrayList<Integer> getAllAboutUs() {

        ArrayList<Integer> array_list = new ArrayList<Integer>();

        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from aboutUs", null);
        res.moveToFirst();

        while (res.isAfterLast() == false) {
            array_list.add(res.getInt(res.getColumnIndex(COLUMN_ID)));
            res.moveToNext();
        }
        res.close();
        return array_list;
    }

    public Integer deletePublications(Integer id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete("potifications",
                "id = ? ",
                new String[]{Integer.toString(id)});
    }

    public ArrayList<Integer> getAllPublications() {
        ArrayList<Integer> array_list = new ArrayList<Integer>();
        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from publications", null);
        res.moveToFirst();

        while (res.isAfterLast() == false) {
            array_list.add(res.getInt(res.getColumnIndex(COLUMN_ID)));
            res.moveToNext();
        }
        res.close();
        return array_list;
    }
    public Cursor getPublications(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from publications where id=" + Integer.toString(id) + "", null);
        return res;
    }
    public void deleteAllPublications() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from publications", null);
        res.moveToFirst();
        while (res.isAfterLast() == false) {
            db.delete("publications", "id = ? ", new String[]{res.getString(res.getColumnIndex(DBHelper.COLUMN_ID))});
            res.moveToNext();
        }
        res.close();
    }
    public Integer deleteNewsEvents(Integer id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete("newsevents",
                "id = ? ",
                new String[]{Integer.toString(id)});
    }

    public ArrayList<Integer> getAllNewsEvents() {
        ArrayList<Integer> array_list = new ArrayList<Integer>();

        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from newsevents", null);
        res.moveToFirst();

        while (res.isAfterLast() == false) {
            array_list.add(res.getInt(res.getColumnIndex(COLUMN_ID)));
            res.moveToNext();
        }
        res.close();
        return array_list;
    }
    public Cursor getNewsEvents(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from newsevents where id=" + Integer.toString(id) + "", null);
        return res;
    }
    public void deleteAllNewsevents() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from newsevents", null);
        res.moveToFirst();
        while (res.isAfterLast() == false) {
            db.delete("newsevents", "id = ? ", new String[]{res.getString(res.getColumnIndex(DBHelper.COLUMN_ID))});
            res.moveToNext();
        }
        res.close();
    }
}