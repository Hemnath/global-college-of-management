package pas.pranav.gcm.helpers;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import pas.pranav.gcm.R;

public class ListViewAdapter extends android.widget.BaseAdapter {


    android.content.Context context;
    android.view.LayoutInflater inflater;
    java.util.ArrayList<java.util.HashMap<String, String>> data;
    ImageLoader imageLoader;
    java.util.HashMap<String, String> resultp = new java.util.HashMap<String, String>();

    public ListViewAdapter(android.content.Context context, java.util.ArrayList<java.util.HashMap<String, String>> arraylist) {
        this.context = context;
        data = arraylist;
        imageLoader = new ImageLoader(context);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public android.view.View getView(final int position, android.view.View convertView, android.view.ViewGroup parent) {

        android.widget.TextView title, date;
        android.widget.TextView description;
        android.widget.ImageView image;

        inflater = (android.view.LayoutInflater) context.getSystemService(android.content.Context.LAYOUT_INFLATER_SERVICE);

        android.view.View itemView = inflater.inflate(pas.pranav.gcm.R.layout.listview_item, parent, false);
        // Get the position
        resultp = data.get(position);

        title = (android.widget.TextView) itemView.findViewById(pas.pranav.gcm.R.id.listTitle);
        description = (android.widget.TextView) itemView.findViewById(pas.pranav.gcm.R.id.listBody);
        date = (android.widget.TextView) itemView.findViewById(pas.pranav.gcm.R.id.listDate);
        image = (android.widget.ImageView) itemView.findViewById(pas.pranav.gcm.R.id.listImage);


        title.setText(android.text.Html.fromHtml(resultp.get("title")));

        String str = android.text.Html.fromHtml(resultp.get("description")).toString();
        if (str.length() < 40)
            description.setText(str + "...");
        else
            description.setText(str.substring(0, 40) + "...");

        date.setText(resultp.get("c_date"));

        if (!resultp.get("image").equalsIgnoreCase("") || !resultp.get("image").isEmpty()) {
            Picasso.get().load(resultp.get("image"))
                    .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                    .error(R.drawable.no_image)
                    .into(image);
        }
        itemView.setOnClickListener(new android.view.View.OnClickListener() {
            @Override
            public void onClick(android.view.View arg0) {

                resultp = data.get(position);
                android.content.Intent intent = new android.content.Intent(context, pas.pranav.gcm.helpers.NewsSingleItemView.class);

                intent.putExtra("title", resultp.get("title"));
                intent.putExtra("c_date", resultp.get("c_date"));
                intent.putExtra("summary", resultp.get("summary"));
                intent.putExtra("description", resultp.get("description"));
                intent.putExtra("image", resultp.get("image"));
                context.startActivity(intent);


            }
        });
        return itemView;
    }
}
