package pas.pranav.gcm.helpers;

/**
 * Created by dev on 11/1/15.
 */
public class ServerString {
    private String username;
    private String password;

    public String getPassword() {
        //System.out.println("password in getter: " + password);
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public String getUsername() {
        //System.out.println("username in getter: " + username);
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
}
