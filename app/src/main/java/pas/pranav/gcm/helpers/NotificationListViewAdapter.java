package pas.pranav.gcm.helpers;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import pas.pranav.gcm.R;

public class NotificationListViewAdapter extends android.widget.BaseAdapter {
    android.content.Context context;
    android.view.LayoutInflater inflater;
    java.util.ArrayList<java.util.HashMap<String, String>> data;
    ImageLoader imageLoader;
    java.util.HashMap<String, String> resultp = new java.util.HashMap<String, String>();
    private ProgressDialog progressBar;

    public NotificationListViewAdapter(android.content.Context context, java.util.ArrayList<java.util.HashMap<String, String>> arraylist) {
        this.context = context;
        data = arraylist;
        //imageLoader = new ImageLoader(context);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public android.view.View getView(final int position, android.view.View convertView, android.view.ViewGroup parent) {
        // Declare Variables
        android.widget.TextView title, listDate;
        android.widget.TextView description;
        android.widget.ImageView image;

        inflater = (android.view.LayoutInflater) context.getSystemService(android.content.Context.LAYOUT_INFLATER_SERVICE);

        android.view.View itemView = inflater.inflate(pas.pranav.gcm.R.layout.notification_listview_item, parent, false);
        // Get the position
        resultp = data.get(position);

        // Locate the TextViews in listview_item.xml
        title = (android.widget.TextView) itemView.findViewById(pas.pranav.gcm.R.id.listTitle);
        description = (android.widget.TextView) itemView.findViewById(pas.pranav.gcm.R.id.listBody);
        listDate = (android.widget.TextView) itemView.findViewById(R.id.listDate);

        // Capture position and set results to the TextViews
        title.setText(android.text.Html.fromHtml(resultp.get("title")));
        listDate.setText(resultp.get("c_date"));
        String str = android.text.Html.fromHtml(resultp.get("description")).toString();
        if (str.length() < 40)
            description.setText(str + "...");
        else
            description.setText(str.substring(0, 40) + "...");

        itemView.setOnClickListener(new android.view.View.OnClickListener() {
            @Override
            public void onClick(android.view.View arg0) {

                resultp = data.get(position);
                android.content.Intent intent = new android.content.Intent(context, pas.pranav.gcm.helpers.NotificationSingleItemView.class);
                intent.putExtra("c_date", resultp.get("c_date"));
                intent.putExtra("summary", resultp.get("summary"));
                intent.putExtra("description", resultp.get("description"));
                context.startActivity(intent);

            }
        });
        return itemView;
    }


}
