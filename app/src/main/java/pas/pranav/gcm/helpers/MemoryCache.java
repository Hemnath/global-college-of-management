package pas.pranav.gcm.helpers;

public class MemoryCache {

	private static final String TAG = "MemoryCache";

	// Last argument true for LRU ordering
	private java.util.Map<String, android.graphics.Bitmap> cache = java.util.Collections
			.synchronizedMap(new java.util.LinkedHashMap<String, android.graphics.Bitmap>(10, 1.5f, true));

	// Current allocated size
	private long size = 0;

	// Max memory in bytes
	private long limit = 1000000;

	public MemoryCache() {
		// Use 25% of available heap size
		setLimit(Runtime.getRuntime().maxMemory() / 4);
	}

	public void setLimit(long new_limit) {
		limit = new_limit;
		android.util.Log.i(TAG, "MemoryCache will use up to " + limit / 1024. / 1024. + "MB");
	}

	public android.graphics.Bitmap get(String id) {
		try {
			if (!cache.containsKey(id))
				return null;
			return cache.get(id);
		} catch (NullPointerException ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public void put(String id, android.graphics.Bitmap bitmap) {
		try {
			if (cache.containsKey(id))
				size -= getSizeInBytes(cache.get(id));
			cache.put(id, bitmap);
			size += getSizeInBytes(bitmap);
			checkSize();
		} catch (Throwable th) {
			th.printStackTrace();
		}
	}

	private void checkSize() {
		android.util.Log.i(TAG, "cache size=" + size + " length=" + cache.size());
		if (size > limit) {
			// Least recently accessed item will be the first one iterated
			java.util.Iterator<java.util.Map.Entry<String, android.graphics.Bitmap>> iter = cache.entrySet().iterator();
			while (iter.hasNext()) {
				java.util.Map.Entry<String, android.graphics.Bitmap> entry = iter.next();
				size -= getSizeInBytes(entry.getValue());
				iter.remove();
				if (size <= limit)
					break;
			}
			android.util.Log.i(TAG, "Clean cache. New size " + cache.size());
		}
	}

	public void clear() {
		try {
			cache.clear();
			size = 0;
		} catch (NullPointerException ex) {
			ex.printStackTrace();
		}
	}

	long getSizeInBytes(android.graphics.Bitmap bitmap) {
		if (bitmap == null)
			return 0;
		return bitmap.getRowBytes() * bitmap.getHeight();
	}
}