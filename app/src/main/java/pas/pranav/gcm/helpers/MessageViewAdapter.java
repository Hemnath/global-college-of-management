package pas.pranav.gcm.helpers;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import pas.pranav.gcm.R;

public class MessageViewAdapter extends BaseAdapter {
    private Context mContext;
    private List<Item> items = new ArrayList<Item>();
    private LayoutInflater inflater;

    public MessageViewAdapter(Context context) {
        inflater = LayoutInflater.from(context);
        mContext = context;

    }

    public void addItem(boolean IsRead, int RecordsId, String MessageSubject, String MessageBody, String CreatedDate) {
        items.add(new Item(IsRead, RecordsId, MessageSubject, MessageBody, CreatedDate));

    }

    public String getCDate(int i) {
        return items.get(i).CreatedDate;
    }

    public int getRecordsId(int i) {
        return items.get(i).RecordsId;
    }

    public String getMessageSubject(int i) {
        return items.get(i).MessageSubject;
    }

    public String getMessageBody(int i) {
        return items.get(i).MessageBody;
    }

    public boolean getIsRead(int i) {
        return items.get(i).IsRead;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = view;
        TextView title;
        TextView body, date;

        if (v == null) {
            v = inflater.inflate(R.layout.notification_listview_item, viewGroup, false);
            v.setTag(R.id.listTitle, v.findViewById(R.id.listTitle));
            v.setTag(R.id.listBody, v.findViewById(R.id.listBody));
            v.setTag(R.id.listDate, v.findViewById(R.id.listDate));

        }

        title = (TextView) v.getTag(R.id.listTitle);
        body = (TextView) v.getTag(R.id.listBody);
        date = (TextView) v.getTag(R.id.listDate);

        Item item = (Item) getItem(i);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat.parse(item.CreatedDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        title.setText(item.MessageSubject);
        if(convertedDate.toString().length() > 11)
            date.setText(convertedDate.toString().substring(0,11));

        if (item.MessageBody.length() < 40)
            body.setText(item.MessageBody + "...");
        else
            body.setText(item.MessageBody.substring(0, 40) + "...");

        return v;
    }

    private class Item {
        final String MessageSubject, MessageBody, CreatedDate;
        int RecordsId;
        boolean IsRead;

        Item(boolean IsRead, int RecordsId, String MessageSubject, String MessageBody, String CreatedDate) {
            this.IsRead = IsRead;
            this.RecordsId = RecordsId;
            this.MessageSubject = MessageSubject;
            this.MessageBody = MessageBody;
            this.CreatedDate = CreatedDate;
        }
    }
}
