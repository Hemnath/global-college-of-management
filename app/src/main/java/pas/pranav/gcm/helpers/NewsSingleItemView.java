package pas.pranav.gcm.helpers;

public class NewsSingleItemView extends android.support.v7.app.AppCompatActivity  {

    String title;
    String c_date;
    String summary;
    String image;
    String description;
    ImageLoader imageLoader = new ImageLoader(this);
    android.widget.ImageView ivImage;
    @Override
    protected void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(pas.pranav.gcm.R.layout.news_single_item_view);
        android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(pas.pranav.gcm.R.id.toolbar);
        setSupportActionBar(toolbar);

        android.content.Intent i = getIntent();

        title = i.getStringExtra("title");
        c_date = i.getStringExtra("c_date");
        summary = i.getStringExtra("summary");
        description = i. getStringExtra("description");
        image = i.getStringExtra("image");

        String messageBody = "<body style = \"text-align:justify\">" + "<font fize = \"6\">" + "</font>" + "<strong>" + title + "</strong>" + "<br>" + c_date + "<br><br>" + description + "</body>";


        android.webkit.WebView wvDetails = (android.webkit.WebView) findViewById(pas.pranav.gcm.R.id.wvlistItemDetails);

        wvDetails.loadData(messageBody, "text/html", "utf-8");
    }
}