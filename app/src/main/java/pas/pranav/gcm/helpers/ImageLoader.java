package pas.pranav.gcm.helpers;

public class ImageLoader {

	MemoryCache memoryCache = new MemoryCache();
	FileCache fileCache;
	private java.util.Map<android.widget.ImageView, String> imageViews = java.util.Collections
			.synchronizedMap(new java.util.WeakHashMap<android.widget.ImageView, String>());
	java.util.concurrent.ExecutorService executorService;
	// Handler to display images in UI thread
	android.os.Handler handler = new android.os.Handler();

	public ImageLoader(android.content.Context context) {
		fileCache = new FileCache(context);
		executorService = java.util.concurrent.Executors.newFixedThreadPool(5);
	}

	final int stub_id = pas.pranav.gcm.R.drawable.contact;

	public void DisplayImage(String url, android.widget.ImageView imageView) {
		imageViews.put(imageView, url);
		android.graphics.Bitmap bitmap = memoryCache.get(url);
		if (bitmap != null)
			imageView.setImageBitmap(bitmap);
		else {
			queuePhoto(url, imageView);
			imageView.setImageResource(stub_id);
		}
	}

	private void queuePhoto(String url, android.widget.ImageView imageView) {
		PhotoToLoad p = new PhotoToLoad(url, imageView);
		executorService.submit(new PhotosLoader(p));
	}

	private android.graphics.Bitmap getBitmap(String url) {
		java.io.File f = fileCache.getFile(url);

		android.graphics.Bitmap b = decodeFile(f);
		if (b != null)
			return b;

		// Download Images from the Internet
		try {
			android.graphics.Bitmap bitmap = null;
			java.net.URL imageUrl = new java.net.URL(url);
			java.net.HttpURLConnection conn = (java.net.HttpURLConnection) imageUrl
					.openConnection();
			conn.setConnectTimeout(30000);
			conn.setReadTimeout(30000);
			conn.setInstanceFollowRedirects(true);
			java.io.InputStream is = conn.getInputStream();
			java.io.OutputStream os = new java.io.FileOutputStream(f);
			Utils.CopyStream(is, os);
			os.close();
			conn.disconnect();
			bitmap = decodeFile(f);
			return bitmap;
		} catch (Throwable ex) {
			ex.printStackTrace();
			if (ex instanceof OutOfMemoryError)
				memoryCache.clear();
			return null;
		}
	}

	// Decodes image and scales it to reduce memory consumption
	private android.graphics.Bitmap decodeFile(java.io.File f) {
		try {
			// Decode image size
			android.graphics.BitmapFactory.Options o = new android.graphics.BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			java.io.FileInputStream stream1 = new java.io.FileInputStream(f);
			android.graphics.BitmapFactory.decodeStream(stream1, null, o);
			stream1.close();

			// Find the correct scale value. It should be the power of 2.
			// Recommended Size 512
			final int REQUIRED_SIZE = 70;
			int width_tmp = o.outWidth, height_tmp = o.outHeight;
			int scale = 1;
			while (true) {
				if (width_tmp / 2 < REQUIRED_SIZE
						|| height_tmp / 2 < REQUIRED_SIZE)
					break;
				width_tmp /= 2;
				height_tmp /= 2;
				scale *= 2;
			}

			// Decode with inSampleSize
			android.graphics.BitmapFactory.Options o2 = new android.graphics.BitmapFactory.Options();
			o2.inSampleSize = scale;
			java.io.FileInputStream stream2 = new java.io.FileInputStream(f);
			android.graphics.Bitmap bitmap = android.graphics.BitmapFactory.decodeStream(stream2, null, o2);
			stream2.close();
			return bitmap;
		} catch (java.io.FileNotFoundException e) {
		} catch (java.io.IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	// Task for the queue
	private class PhotoToLoad {
		public String url;
		public android.widget.ImageView imageView;

		public PhotoToLoad(String u, android.widget.ImageView i) {
			url = u;
			imageView = i;
		}
	}

	class PhotosLoader implements Runnable {
		PhotoToLoad photoToLoad;

		PhotosLoader(PhotoToLoad photoToLoad) {
			this.photoToLoad = photoToLoad;
		}

		@Override
		public void run() {
			try {
				if (imageViewReused(photoToLoad))
					return;
				android.graphics.Bitmap bmp = getBitmap(photoToLoad.url);
				memoryCache.put(photoToLoad.url, bmp);
				if (imageViewReused(photoToLoad))
					return;
				BitmapDisplayer bd = new BitmapDisplayer(bmp, photoToLoad);
				handler.post(bd);
			} catch (Throwable th) {
				th.printStackTrace();
			}
		}
	}

	boolean imageViewReused(PhotoToLoad photoToLoad) {
		String tag = imageViews.get(photoToLoad.imageView);
		if (tag == null || !tag.equals(photoToLoad.url))
			return true;
		return false;
	}

	// Used to display bitmap in the UI thread
	class BitmapDisplayer implements Runnable {
		android.graphics.Bitmap bitmap;
		PhotoToLoad photoToLoad;

		public BitmapDisplayer(android.graphics.Bitmap b, PhotoToLoad p) {
			bitmap = b;
			photoToLoad = p;
		}

		public void run() {
			if (imageViewReused(photoToLoad))
				return;
			if (bitmap != null)
				photoToLoad.imageView.setImageBitmap(bitmap);
			else
				photoToLoad.imageView.setImageResource(stub_id);
		}
	}

	public void clearCache() {
		memoryCache.clear();
		fileCache.clear();
	}

}