package pas.pranav.gcm.helpers;

/**
 * Created by hem on 11/23/15.
 */
public class API_URL {

    // API for +2 stream >>>>>>>>>>>
    public String aboutPlus2Url = "http://27.111.21.62/global/api/app/child_menu/parent/7/id/11";
    public String admissionPlus2Url = "http://27.111.21.62/global/api/app/child_menu/parent/7/id/12";
    public String classHoursPlus2Url = "http://27.111.21.62/global/api/app/child_menu/parent/7/id/14";
    public String contactPlus2Url = "http://27.111.21.62/global/api/app/child_menu/parent/7/id/18";
    public String coursesPlus2Url = "http://27.111.21.62/global/api/app/child_menu/parent/7/id/13";
    public String facultiesPlus2Url = "http://27.111.21.62/global/api/app/child_menu/parent/7/id/15";
    public String feePlus2Url = "http://27.111.21.62/global/api/app/child_menu/parent/7/id/16";
    public String scholarshipPlus2Url = "http://27.111.21.62/global/api/app/child_menu/parent/7/id/17";
    //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    //API for Bachelors stream >>>>>>>>>>>>>
    public String bachelorsAboutUrl = "http://27.111.21.62/global/api/app/child_menu/parent/9/id/19";
    public String bachelorsAdmissionUrl = "http://27.111.21.62/global/api/app/child_menu/parent/9/id/20";
    public String bachelorsContatUrl = "http://27.111.21.62/global/api/app/child_menu/parent/9/id/26";
    public String bachelorsCourseUrl = "http://27.111.21.62/global/api/app/child_menu/parent/9/id/21";
    public String bachelorsFacultiesUrl = "http://27.111.21.62/global/api/app/child_menu/parent/9/id/23";
    public String bachelorsFeeUrl = "http://27.111.21.62/global/api/app/child_menu/parent/9/id/24";
    public String bachelorsScholarshipUrl = "http://27.111.21.62/global/api/app/child_menu/parent/7/id/17";
    // <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

    //API for Mastesr stream >>>>>>>>>>>>>>>>>>>>
    public String mastersAboutUrl = "http://27.111.21.62/global/api/app/child_menu/parent/10/id/27";
    public String mastersAdmissionUrl = "http://27.111.21.62/global/api/app/child_menu/parent/10/id/28";
    public String mastersClassHourUrl = "http://27.111.21.62/global/api/app/child_menu/parent/10/id/30";
    public String mastersContactUrl = "http://27.111.21.62/global/api/app/child_menu/parent/10/id/34";
    public String mastersCourseUrl = "http://27.111.21.62/global/api/app/child_menu/parent/10/id/29";
    public String mastersFacultiesUrl = "http://27.111.21.62/global/api/app/child_menu/parent/10/id/31";
    public String mastersFeeUrl = "http://27.111.21.62/global/api/app/child_menu/parent/10/id/32";
    public String mastersScholarshipUrl = "http://27.111.21.62/global/api/app/child_menu/parent/10/id/33";


    public String notificationUrl1 = "http://27.111.21.62/global/api/app/notifications";

   public String domainName = "http://api.globalcollege.edu.np/";
   //public  String domainName="http://10.10.22.23:8003/";

    public String ServerUrlPublic = domainName + "CollegeMisApi.svc/";
    public String ServerUrlAdmin = domainName + "Employee.svc/";
    public String ServerUrlStudent = domainName + "Student.svc/";
    public String ServerUrlParent = domainName + "Parent.svc/";
    public String ServerUrlTeacher = domainName + "Teacher.svc/";


    public String UserLogin = "UserLogin";
    public String MyProfile = "MyProfile";
    public String NoticeBoard = "NoticeBoard";
    public String Message = "Message";
    public String CreateLeave = "CreateLeave";
    public String LeaveReport = "LeaveReport";
    public String DetailLeave = "DetailLeave";
    public String DeleteLeave = "DeleteLeave?leaveId=";
    public String TeacherSubjectClass = "TeacherSubjectClass";
    public String Attendance = "Attendance";
    public String AttendamceReport = "AttendamceReport";
    public String Calender = "Calender";
    public String MyExamList = "MyExamList";
    public String FacultyDetail = "FacultyDetail?id=";
    public String MyReportCard = "MyReportCard?id=";
    public String news_events = "news_events";
    public String SubjectHomeWork = "SubjectHomeWork";
    public String MonthlyHomeWork = "MonthlyHomeWork";
    public String CurrentHomeWork = "CurrentHomeWork";
    public String LedgerReport = "LedgerReport";
    public String Invoice = "Invoice?invoiceNumber=";
    public String fiscialYearId = "&fiscialYearId=";
    public String Receipt = "Receipt?tranid=";

}
