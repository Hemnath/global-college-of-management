package pas.pranav.gcm.helpers;

/**
 * Created by dev on 11/10/15.
 */
public class CommonGetSet {

    String uid;
    String pwd;
    String resultId;


    public String getUid () {
        return uid;
    }
    public void setUid (String uid) {
        this.uid = uid;
    }

    public String getPwd () {
        return pwd;
    }
    public void setPwd (String pwd) {
        this.pwd = pwd;
    }
    public String getResultId () {
        return resultId;
    }
    public void setResultId (String resultId) {
        this.resultId = resultId;
    }
}
