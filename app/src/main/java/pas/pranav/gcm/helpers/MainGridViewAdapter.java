package pas.pranav.gcm.helpers;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import pas.pranav.gcm.R;

/**
 * Created by ik890 on 5/11/2016.
 */
public class MainGridViewAdapter extends BaseAdapter {
    private Context mContext;
    private List<Item> items = new ArrayList<Item>();
    private LayoutInflater inflater;

    public MainGridViewAdapter(Context context) {
        inflater = LayoutInflater.from(context);
        mContext = context;

    }
    public void addItem(String name, int image){
        items.add(new Item(name,image));

    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = view;
        ImageView picture;
        TextView name;

        if (v == null) {
            v = inflater.inflate(R.layout.main_grid_view_layout, viewGroup, false);
            v.setTag(R.id.itemName, v.findViewById(R.id.itemName));
            v.setTag(R.id.picture, v.findViewById(R.id.picture));

        }
        picture = (ImageView) v.getTag(R.id.picture);
        name = (TextView) v.getTag(R.id.itemName);

        Item item = (Item) getItem(i);

        name.setText(item.itemName);
        picture.setImageResource(item.drawableId);

        return v;
    }

    private class Item {
        final String itemName;
        int drawableId;

        Item(String name, int drawableId) {
            this.itemName = name;
            this.drawableId = drawableId;
        }
    }
}
