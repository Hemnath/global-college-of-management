package pas.pranav.gcm.helpers;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;

public class SingleItemView extends android.support.v7.app.AppCompatActivity {
	// Declare Variables
    String title;
	String c_date;
	String summary;
	String image;
	String file;
	String file_name;
	ImageLoader imageLoader = new ImageLoader(this);
    android.widget.ImageView ivImage;
	Button PdfView;

	@Override
	public void onCreate(android.os.Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(pas.pranav.gcm.R.layout.singleitemview);
		android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(pas.pranav.gcm.R.id.toolbar);
		setSupportActionBar(toolbar);
		PdfView = (Button) findViewById(pas.pranav.gcm.R.id.pdflistItem);
		android.content.Intent i = getIntent();
		//rank = i.getStringExtra("id");
        title = i.getStringExtra("title");
        c_date = i.getStringExtra("c_date");
		summary = i.getStringExtra("summary");
        file = i. getStringExtra("file");
		file_name = i. getStringExtra("file_name");
		image = i.getStringExtra("image");
		download(PdfView);

		String messageBody = "<body style = \"text-align:justify\">" + "<font fize = \"6\">" + "</font>" + "<strong>" + title + "</strong>" + "<br>" + c_date + "<br><br>" + summary + "<br>"+"<strong>" + file_name + "</strong>" + "</body>";


		android.webkit.WebView wvDetails = (android.webkit.WebView) findViewById(pas.pranav.gcm.R.id.wvlistItemDetails);

		wvDetails.loadData(messageBody, "text/html", "utf-8");

	}
	public void view(View v)
	{
		File pdfFile = new File(Environment.getExternalStorageDirectory() + "/gcm/" + file_name);  // -> filename = maven.pdf
		Uri path = Uri.fromFile(pdfFile);
		Intent pdfIntent = new Intent(Intent.ACTION_VIEW);
		pdfIntent.setDataAndType(path, "application/pdf");
		pdfIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

		try{
			startActivity(pdfIntent);
		}catch(ActivityNotFoundException e){
			Toast.makeText(SingleItemView.this, "No Application available to view PDF", Toast.LENGTH_SHORT).show();
		}
	}
	public void download(View v)
	{
		new DownloadFile().execute(file, file_name);
	}
	private class DownloadFile extends AsyncTask<String, Void, Void> {

		@Override
		protected Void doInBackground(String... strings) {
			String fileUrl = strings[0];
			String fileName = strings[1];
			String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
			File folder = new File(extStorageDirectory, "gcm");
			folder.mkdir();

			File pdfFile = new File(folder, fileName);

			try{
				pdfFile.createNewFile();
			}catch (IOException e){
				e.printStackTrace();
			}
			FileDownloader.downloadFile(fileUrl, pdfFile);
			return null;
		}
	}
}