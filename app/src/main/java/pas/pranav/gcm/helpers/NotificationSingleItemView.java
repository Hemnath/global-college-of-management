package pas.pranav.gcm.helpers;

public class NotificationSingleItemView extends android.support.v7.app.AppCompatActivity {
	// Declare Variables
	String c_date;
	String summary;
	String image;
	String description;
	pas.pranav.gcm.helpers.ImageLoader imageLoader = new pas.pranav.gcm.helpers.ImageLoader(this);

	@Override
	public void onCreate(android.os.Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Get the view from singleitemview.xml
		setContentView(pas.pranav.gcm.R.layout.notification_singleitemview);
		android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(pas.pranav.gcm.R.id.toolbar);
		setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		android.content.Intent i = getIntent();
        c_date = i.getStringExtra("c_date");
		summary = i.getStringExtra("summary");
        description = i. getStringExtra("description");

		android.widget.TextView txtC_Date = (android.widget.TextView) findViewById(pas.pranav.gcm.R.id.tvDate);
		android.widget.TextView txtSummary = (android.widget.TextView) findViewById(pas.pranav.gcm.R.id.tvSummary);
		android.widget.TextView txtDescription = (android.widget.TextView) findViewById(pas.pranav.gcm.R.id.tvDescription);

		txtC_Date.setText(c_date);
		txtSummary.setText(android.text.Html.fromHtml(summary));
		txtDescription.setText(android.text.Html.fromHtml(description));

	}
}