package pas.pranav.gcm.helpers;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import pas.pranav.gcm.R;

/**
 * Created by ik890 on 7/8/2016.
 */
public class NoticeBoardAdapter extends BaseAdapter {
    private Context mContext;
    private List<Item> items = new ArrayList<Item>();
    private LayoutInflater inflater;

    public NoticeBoardAdapter(Context context) {
        inflater = LayoutInflater.from(context);
        mContext = context;

    }

    public void addItem(int RecordsId, String NoticeSubject, String NoticeDetail, String CreatedDate) {
        items.add(new Item(RecordsId, NoticeSubject, NoticeDetail, CreatedDate));

    }

    public String getCDate(int i) {
        return items.get(i).CreatedDate;
    }

    public int getRecordsId(int i) {
        return items.get(i).RecordsId;
    }

    public String getNoticeDetail(int i) {
        return items.get(i).NoticeDetail;
    }

    public String getNoticeSubject(int i) {
        return items.get(i).NoticeSubject;
    }

    public boolean getIsRead(int i) {
        return items.get(i).IsRead;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = view;
        TextView title;
        TextView body;

        if (v == null) {
            v = inflater.inflate(R.layout.notice_board, viewGroup, false);
            v.setTag(R.id.listTitle, v.findViewById(R.id.listTitle));
            v.setTag(R.id.listBody, v.findViewById(R.id.listBody));

        }

        title = (TextView) v.getTag(R.id.listTitle);
        body = (TextView) v.getTag(R.id.listBody);

        Item item = (Item) getItem(i);

        title.setText(item.NoticeSubject);

        if (item.NoticeDetail.length() < 40)
            body.setText(item.NoticeDetail + "...");
        else
            body.setText(item.NoticeDetail.substring(0, 40) + "...");

        return v;
    }

    private class Item {
        final String NoticeSubject, NoticeDetail, CreatedDate;
        int RecordsId;
        boolean IsRead;

        Item(int RecordsId, String NoticeSubject, String NoticeDetail, String CreatedDate) {
            this.RecordsId = RecordsId;
            this.NoticeSubject = NoticeSubject;
            this.NoticeDetail = NoticeDetail;
            this.CreatedDate = CreatedDate;
        }
    }
}
