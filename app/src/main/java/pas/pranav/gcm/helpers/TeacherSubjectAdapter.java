package pas.pranav.gcm.helpers;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import pas.pranav.gcm.R;

/**
 * Created by ik890 on 7/13/2016.
 */
public class TeacherSubjectAdapter extends BaseAdapter {
    private Context mContext;
    private List<Item> items = new ArrayList<Item>();
    private LayoutInflater inflater;

    public TeacherSubjectAdapter(Context context) {
        inflater = LayoutInflater.from(context);
        mContext = context;
    }

    public boolean addItem(String academicYearTitle, String classDates, String classDays, String classTimes, String courseTitle, String isCompleted, String partId, String section, String shiftTitle, String subjectTitle, String yearAlias, String yearId) {

        items.add(new Item(academicYearTitle, classDates, classDays, classTimes, courseTitle, isCompleted, partId, section, shiftTitle, subjectTitle, yearAlias, yearId));
        return true;
    }

    public boolean clearItem() {
        items.clear();

        return true;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = view;
        TextView academicTitle, subjectTitle, classDays, classTimes;

        if (v == null) {
            v = inflater.inflate(R.layout.teacher_subject_item, viewGroup, false);
            v.setTag(R.id.academicTitle, v.findViewById(R.id.academicTitle));
            v.setTag(R.id.subjectTitle, v.findViewById(R.id.subjectTitle));
            v.setTag(R.id.classDays, v.findViewById(R.id.classDays));
            v.setTag(R.id.classTimes, v.findViewById(R.id.classTimes));

        }
        academicTitle = (TextView) v.getTag(R.id.academicTitle);
        subjectTitle = (TextView) v.getTag(R.id.subjectTitle);
        classDays = (TextView) v.getTag(R.id.classDays);
        classTimes = (TextView) v.getTag(R.id.classTimes);

        Item item = (Item) getItem(i);

        academicTitle.setText(item.AcademicYearTitle + " " + item.Section + " " + item.ShiftTitle);
        subjectTitle.setText(item.SubjectTitle);
        classDays.setText(item.ClassDays);
        classTimes.setText(item.ClassTimes);

        return v;
    }

    private class Item {
        String AcademicYearTitle, ClassDates, ClassDays, ClassTimes, CourseTitle, IsCompleted, PartId, Section, ShiftTitle, SubjectTitle, YearAlias, YearId;

        Item(String academicYearTitle, String classDates, String classDays, String classTimes, String courseTitle, String isCompleted, String partId, String section, String shiftTitle, String subjectTitle, String yearAlias, String yearId) {
            AcademicYearTitle = academicYearTitle;
            ClassDates = classDates;
            ClassDays = classDays;
            ClassTimes = classTimes;
            CourseTitle = courseTitle;
            IsCompleted = isCompleted;
            PartId = partId;
            Section = section;
            ShiftTitle = shiftTitle;
            SubjectTitle = subjectTitle;
            YearAlias = yearAlias;
            YearId = yearId;
        }

    }
}