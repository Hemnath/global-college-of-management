package pas.pranav.gcm.helpers;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import pas.pranav.gcm.R;

/**
 * Created by ik890 on 7/20/2016.
 */
public class CalendarAdapter extends BaseAdapter {
    private Context mContext;
    private List<Item> items = new ArrayList<Item>();
    private LayoutInflater inflater;

    public CalendarAdapter(Context context) {
        inflater = LayoutInflater.from(context);
        mContext = context;

    }

    public void addItem(String backgroundColor, String borderColor, String end, String start, String title, String url) {
        items.add(new Item(backgroundColor, borderColor, end, start, title, url));

    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = view;
        TextView title;
        TextView endDate, startDate;
        if (v == null) {
            v = inflater.inflate(R.layout.calendar_view_item, viewGroup, false);
            v.setTag(R.id.listTitle, v.findViewById(R.id.listTitle));
            v.setTag(R.id.startDate, v.findViewById(R.id.startDate));
            v.setTag(R.id.endDate, v.findViewById(R.id.endDate));

        }

        title = (TextView) v.getTag(R.id.listTitle);
        endDate = (TextView) v.getTag(R.id.endDate);
        startDate = (TextView) v.getTag(R.id.startDate);

        Item item = (Item) getItem(i);

        title.setText(item.title);
        endDate.setText(item.end);
        startDate.setText(item.start);

        return v;
    }

    private class Item {
        final String backgroundColor, borderColor, end, start, title, url;

        public Item(String backgroundColor, String borderColor, String end, String start, String title, String url) {
            this.backgroundColor = backgroundColor;
            this.borderColor = borderColor;
            this.end = end;
            this.start = start;
            this.title = title;
            this.url = url;
        }


    }
}
