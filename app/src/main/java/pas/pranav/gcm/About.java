package pas.pranav.gcm;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

import pas.pranav.gcm.helpers.DBHelper;
import pas.pranav.gcm.helpers.ImageLoader;
import pas.pranav.gcm.helpers.JSONParser;

public class About extends AppCompatActivity {

    WebView about;
    private static String url = "http://27.111.21.62/global/api/app/child_menu/parent/0/id/1";

    android.widget.ImageView titleImage;

    DBHelper dbHelper;

    JSONArray user = null;
    private ProgressDialog progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(pas.pranav.gcm.R.layout.about_global);
        about = (WebView) findViewById(pas.pranav.gcm.R.id.wvAboutGlobal);
        titleImage = (android.widget.ImageView) findViewById(pas.pranav.gcm.R.id.ivTitle);

        android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(pas.pranav.gcm.R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(About.this, MainActivity.class));
            }

            //
        });

        dbHelper = new DBHelper(this);
        DBDataInsert();
    }

    private void DBDataInsert() {

        if (isConnected()) {
            new JSONParse().execute();
            progressBar = new ProgressDialog(About.this);
            progressBar.setMessage("Downloading...");
            progressBar.setCanceledOnTouchOutside(false);
            progressBar.show();
        } else {

            ArrayList<Integer> array_list;
            array_list = dbHelper.getAllAboutUs();
            int count = array_list.size();
            for (int i = 0; i < count; i++) {
                Cursor rs = dbHelper.getAboutUs(array_list.get(i));
                rs.moveToFirst();
                String image = rs.getString(rs.getColumnIndex("image"));
                String description = rs.getString(rs.getColumnIndex("description"));

                Picasso.get().load(image).into(titleImage);
                if (!image.equalsIgnoreCase("") || !image.isEmpty()) {
                    Picasso.get().load(image)
                            .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                            .error(R.drawable.no_image)
                            .into(titleImage);
                }
                about.loadData(description, "text/html", "utf-8");

                rs.close();

            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.refresh_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {

            if (isConnected()) {
                DBDataInsert();
            } else {
                showAlertDialog(About.this, "No Internet Connection",
                        "You don't have internet connection.", false);
            }

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent launchNextActivity = new Intent(About.this, MainActivity.class);
        launchNextActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        launchNextActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        launchNextActivity.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(launchNextActivity);
    }
    private class JSONParse extends android.os.AsyncTask<String, String, org.json.JSONObject> {
        private android.app.ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {

            System.out.println("inside pre execute");

        }

        @Override
        protected org.json.JSONObject doInBackground(String... args) {
            JSONParser jParser = new JSONParser();

            // Getting JSON from URL
            org.json.JSONObject json = jParser.getJSONFromUrl(url);
            System.out.println("inside background");
            return json;
        }

        @Override
        protected void onPostExecute(org.json.JSONObject json) {
            progressBar.cancel();
            try {
                user = json.getJSONArray("child_menu");
                org.json.JSONObject c = user.getJSONObject(0);

                String menu_name = c.getString("menu_name");
                String menu_title = c.getString("menu_title");
                String summary = c.getString("summary");
                String image_name = c.getString("image_name");
                String file_name = c.getString("file_name");
                String file = c.getString("file");
                String description = c.getString("description");
                String status = c.getString("status");
                String created_date = c.getString("created_date");
                String updated_date = c.getString("updated_date");
                String image = c.getString("image");

                dbHelper.insertAboutUs(menu_name, menu_title, summary, description, image, image_name, file_name, file, status, created_date, updated_date);

                if (!image.equalsIgnoreCase("") || !image.isEmpty()) {
                    Picasso.get().load(image)
                            .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                            .error(R.drawable.no_image)
                            .into(titleImage);
                }
                about.loadData(description, "text/html", "utf-8");

            } catch (org.json.JSONException e) {
                e.printStackTrace();
            }

        }
    }

    boolean isConnected() {
        ConnectivityManager cm = (ConnectivityManager) this.getBaseContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
        return isConnected;
    }

    public void showAlertDialog(Context context, String title, String message, Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();

        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setIcon((status) ? R.drawable.error : R.drawable.error);
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alertDialog.show();
    }
}

