package pas.pranav.gcm.admin;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import pas.pranav.gcm.Attendance_Calendar;
import pas.pranav.gcm.CalendarView;
import pas.pranav.gcm.MIS;
import pas.pranav.gcm.MainActivity;
import pas.pranav.gcm.R;
import pas.pranav.gcm.helpers.MainGridViewAdapter;

public class AdminMainActivity extends AppCompatActivity {


    private GridView gridview;
    MainGridViewAdapter gr;

    Intent myIntent;
    private String url, deviceId, token;

    public static final String strUserName = "userKey";
    public static final String strPassword = "passwordKey";
    public static final String strToken = "token";
    public static final String strDeviceId = "deviceId";

    SharedPreferences.Editor editor;
    SharedPreferences sharedpreferences;
    private TextView tvId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        sharedpreferences = getSharedPreferences(MIS.MyPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();

        deviceId = sharedpreferences.getString(strDeviceId, null);
        token = sharedpreferences.getString(strToken, null);

        tvId = (TextView) findViewById(pas.pranav.gcm.R.id.tvRecord);
        tvId.setText("Welcome " + sharedpreferences.getString(strUserName, null).replace("@globalcollege.edu.np", "").replace(".", " "));

        gridview = (GridView) findViewById(R.id.gridview);
        gr = new MainGridViewAdapter(this);
        gridview.setAdapter(gr);

        gr.addItem("Message", R.drawable.message);
        gr.addItem("Attendance", R.drawable.attendance);
        gr.addItem("Calendar", R.drawable.calender);
        gr.addItem("Notice board", R.drawable.notice_board);

        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bundle bundle = new Bundle();
                switch (position) {
                    case 0:
                        myIntent = new Intent(AdminMainActivity.this, AdminMessage.class);
                        startActivity(myIntent);
                        break;
                    case 1:
                        myIntent = new Intent(AdminMainActivity.this, Attendance_Calendar.class);
                        startActivity(myIntent);
                        break;
                    case 2:
                        myIntent = new Intent(AdminMainActivity.this, CalendarView.class);
                        startActivity(myIntent);
                        break;
                    case 3:
                        myIntent = new Intent(AdminMainActivity.this, AdminNoticeBoard.class);
                        startActivity(myIntent);
                        break;
                }
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(pas.pranav.gcm.R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(pas.pranav.gcm.R.id.action_settings);
        item.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_signOut) {
            editor.clear();
            editor.commit();
            myIntent = new Intent(this, MIS.class);
            startActivity(myIntent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        myIntent = new Intent(this, MainActivity.class);
        startActivity(myIntent);
    }
}
