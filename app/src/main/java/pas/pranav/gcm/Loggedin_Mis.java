package pas.pranav.gcm;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.support.v7.widget.Toolbar;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import pas.pranav.gcm.helpers.API_URL;
import pas.pranav.gcm.helpers.JSONParserCustom;
import pas.pranav.gcm.helpers.MainGridViewAdapter;
import pas.pranav.gcm.parent.ParentMessage;
import pas.pranav.gcm.parent.ParentNotice;
import pas.pranav.gcm.student_related.*;

public class Loggedin_Mis extends AppCompatActivity {

    String token, deviceId;
    String userType = "";
    Intent myIntent;

    private GridView gridview;
    MainGridViewAdapter gr;
    int intUserType;
    ImageView ivLogo;
    TextView tvName;

    public static final String strUserName = "userKey";
    public static final String strPassword = "passwordKey";
    public static final String strToken = "token";
    public static final String strDeviceId = "deviceId";

    SharedPreferences.Editor editor;
    SharedPreferences sharedpreferences;
    private API_URL appURL;
    private String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(pas.pranav.gcm.R.layout.loggedin_mis);
        Toolbar toolbar = (Toolbar) findViewById(pas.pranav.gcm.R.id.toolbar);
        setSupportActionBar(toolbar);

        sharedpreferences = getSharedPreferences(MIS.MyPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();

        deviceId = sharedpreferences.getString(strDeviceId, null);
        token = sharedpreferences.getString(strToken, null);
        userType = sharedpreferences.getString("UserType", null);


        ivLogo = (ImageView) findViewById(pas.pranav.gcm.R.id.ivLogo);

        tvName = (TextView) findViewById(pas.pranav.gcm.R.id.tvName);
        tvName.setText("Welcome " + sharedpreferences.getString(strUserName, null).replace("@globalcollege.edu.np", "").replace(".", " "));

        gridview = (GridView) findViewById(R.id.gridview);
        gr = new MainGridViewAdapter(this);
        gridview.setAdapter(gr);
        intUserType = Integer.parseInt(userType);


        appURL = new API_URL();
        url = appURL.ServerUrlStudent + appURL.MyProfile;

        if (intUserType == 2) {
            new jsonMyProfile().execute(url);
        }

        switch (intUserType) {
            case 2:
                gr.addItem("Message", R.drawable.message);
                gr.addItem("Attendance", R.drawable.attendance);
                gr.addItem("Calendar", R.drawable.calender);
                gr.addItem("Notice board", R.drawable.notice_board);
                gr.addItem("Teacher Subject Class", R.drawable.teachersubject);
                gr.addItem("Exam Results", R.drawable.exam);
                gr.addItem("Homework Subject Report", R.drawable.homeworksubject);
                gr.addItem("Homework Monthly report", R.drawable.monthlyhw);
                gr.addItem("Current Homework", R.drawable.correnthw);
                gr.addItem("Fee Ledger", R.drawable.credit_card);
                /*gr.addItem("Leave", R.drawable.leave);
                gr.addItem("Attendamce Report", R.drawable.report);*/
                break;
            case 3:
                gr.addItem("Message", R.drawable.message);
                gr.addItem("Attendance", R.drawable.attendance);
                gr.addItem("Calendar", R.drawable.calender);
                gr.addItem("Notice board", R.drawable.notice_board);
                gr.addItem("Teacher Subject Class", R.drawable.teachersubject);
                gr.addItem("Exam Results", R.drawable.exam);
                gr.addItem("Homework Subject Report", R.drawable.homeworksubject);
                gr.addItem("Homework Monthly report", R.drawable.monthlyhw);
                gr.addItem("Current Homework", R.drawable.correnthw);
                gr.addItem("Fee Ledger", R.drawable.credit_card);
                break;
        }

        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                switch (position) {
                    case 0:
                        if (intUserType == 2) {
                            myIntent = new Intent(Loggedin_Mis.this, StudentMessage.class);
                        } else if (intUserType == 3) {
                            myIntent = new Intent(Loggedin_Mis.this, ParentMessage.class);
                        }
                        startActivity(myIntent);
                        break;
                    case 1:
                        myIntent = new Intent(Loggedin_Mis.this, Attendance_Calendar.class);
                        startActivity(myIntent);
                        break;
                    case 2:
                        myIntent = new Intent(Loggedin_Mis.this, CalendarView.class);
                        startActivity(myIntent);
                        break;
                    case 3:
                        if (intUserType == 2) {
                            myIntent = new Intent(Loggedin_Mis.this, Notice_Board.class);
                        } else if (intUserType == 3) {
                            myIntent = new Intent(Loggedin_Mis.this, ParentNotice.class);
                        }
                        startActivity(myIntent);
                        break;
                    case 4:
                        myIntent = new Intent(Loggedin_Mis.this, StudentTeacherSubject.class);
                        startActivity(myIntent);
                        break;
                    case 5:
                        myIntent = new Intent(Loggedin_Mis.this, MyExamList.class);
                        startActivity(myIntent);

                        break;
                    case 6:
                        myIntent = new Intent(Loggedin_Mis.this, SubjectHomeWork.class);
                        startActivity(myIntent);
                        break;
                    case 7:
                        myIntent = new Intent(Loggedin_Mis.this, MonthlyHomeWork.class);
                        startActivity(myIntent);
                        break;
                    case 8:
                        myIntent = new Intent(Loggedin_Mis.this, CurrentHomework.class);
                        startActivity(myIntent);
                        break;
                    case 9:
                        myIntent = new Intent(Loggedin_Mis.this, VoucherDetail.class);
                        startActivity(myIntent);
                        break;
                  /*  case 10:
                        myIntent = new Intent(Loggedin_Mis.this, LeaveRequest.class);
                        startActivity(myIntent);
                        break;
                    case 11:
                        myIntent = new Intent(Loggedin_Mis.this, AttendamceReport.class);
                        startActivity(myIntent);
                        break;*/
                }
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(pas.pranav.gcm.R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(pas.pranav.gcm.R.id.action_settings);
        item.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_signOut) {
            editor.clear();
            editor.commit();
            myIntent = new Intent(Loggedin_Mis.this, MIS.class);
            startActivity(myIntent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        myIntent = new Intent(this, MainActivity.class);
        startActivity(myIntent);
    }

    private class jsonMyProfile extends android.os.AsyncTask<String, String, org.json.JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected org.json.JSONObject doInBackground(String... args) {

            JSONParserCustom jParser = new JSONParserCustom();
            org.json.JSONObject json = jParser.getJSONFromUrl(url, deviceId, token);

            return json;
        }

        @Override
        protected void onPostExecute(org.json.JSONObject json) {
            try {

                String jsonCard = json.optString("MyProfile");

                JSONObject jsonObject = new JSONObject(jsonCard);

                String Name = jsonObject.optString("Name").toString();
                String StudentImageUrl = jsonObject.optString("StudentImageUrl").toString();

                StudentImageUrl = StudentImageUrl.replace(" ", "%20");
                tvName.setText("Welcome " + Name);

                if (!StudentImageUrl.equalsIgnoreCase("") || !StudentImageUrl.isEmpty()) {
                    Picasso.get().load(StudentImageUrl)
                            .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                            .error(R.drawable.gcm_logo)
                            .into(ivLogo);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

}
