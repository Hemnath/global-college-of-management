package pas.pranav.gcm;

import android.os.Bundle;
import android.view.View;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

public class Web_View extends AppCompatActivity {

    String calendar_url = "https://www.google.com/calendar/embed?src=globalcollege13%40gmail.com&ctz=Asia/Katmandu\\\" style=\\\"border: 0\\\" width=\\\"800\\\" height=\\\"600\\\" frameborder=\\\"0\\\" scrolling=\\\"no\\\"";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(pas.pranav.gcm.R.layout.web_view);
        android.webkit.WebView webView = (android.webkit.WebView) findViewById(R.id.webView);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setSupportZoom(true);
        webView.setWebViewClient(new android.webkit.WebViewClient());
        webView.loadUrl(calendar_url);
        android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new android.content.Intent(pas.pranav.gcm.Web_View.this, MainActivity.class));
            }

            //
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.action_settings);
        item.setVisible(false);
        return true;
    }


}

