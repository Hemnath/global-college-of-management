package pas.pranav.gcm;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.android.gcm.GCMBaseIntentService;

public class GCMIntentService extends GCMBaseIntentService {

    private static final String TAG = "GCMIntentService";

    //private Controller aController = null;
    pas.pranav.gcm.Controller aController = new Controller();

    public GCMIntentService() {
        // Call extended class Constructor GCMBaseIntentService
        super(Config.GOOGLE_SENDER_ID);
    }

    /**
     * Method called on device registered
     **/
    @Override protected void onRegistered(Context context, String registrationId) {

        //Get Global Controller Class object (see application tag in AndroidManifest.xml)
        // if (aController == null) aController = (Controller) getApplicationContext();

        Log.i(TAG, "Device registered: regId = " + registrationId);
        //aController.displayMessageOnScreen(context, "Your device registred with GCM");
        //Log.d("NAME", MainActivity.name);

        //original
        //aController.register(context, MainActivity.name, MainActivity.email, registrationId);

        aController.register(context, registrationId);
    }

    /**
     * Method called on device unregistred
     */
    @Override protected void onUnregistered(Context context, String registrationId) {
        if (aController == null) aController = (Controller) getApplicationContext();
        Log.i(TAG, "Device unregistered");
        aController.displayMessageOnScreen(context, getString(R.string.gcm_unregistered));
        aController.unregister(context, registrationId);
    }

    /**
     * Method called on Receiving a new message from GCM server
     */
    @Override protected void onMessage(Context context, Intent intent) {

        if (aController == null) aController = (Controller) getApplicationContext();

        Log.i(TAG, "Received message");
        String message = intent.getExtras().getString("price");

        aController.displayMessageOnScreen(context, message);
        // notifies user
        generateNotification(context, message);
    }

    /**
     * Method called on receiving a deleted message
     */
    @Override protected void onDeletedMessages(Context context, int total) {

        if (aController == null) aController = (Controller) getApplicationContext();

        Log.i(TAG, "Received deleted messages notification");
        String message = getString(R.string.gcm_deleted, total);
        aController.displayMessageOnScreen(context, message);
        // notifies user
        generateNotification(context, message);
    }

    /**
     * Method called on Error
     */
    @Override public void onError(Context context, String errorId) {

        if (aController == null) aController = (Controller) getApplicationContext();

        Log.i(TAG, "Received error: " + errorId);
        aController.displayMessageOnScreen(context, getString(R.string.gcm_error, errorId));
    }

    @Override protected boolean onRecoverableError(Context context, String errorId) {

        if (aController == null) aController = (Controller) getApplicationContext();

        // log message
        Log.i(TAG, "Received recoverable error: " + errorId);
        aController.displayMessageOnScreen(context, getString(R.string.gcm_recoverable_error, errorId));
        return super.onRecoverableError(context, errorId);
    }

    /**
     * Create a notification to inform the user that server has sent a message.
     */
    private void generateNotification(Context context, String message) {
        //Latest >>>>>>>>>>>>>>>>>>>>>>>>>>>>>

        Intent notificationIntent = new android.content.Intent(GCMIntentService.this, pas.pranav.gcm.Notifications.class);
        //notificationIntent.setData(android.net.Uri.parse("www.google.com"));
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

        Notification notification = new android.support.v4.app.NotificationCompat.Builder(this).setCategory(android.app.Notification.CATEGORY_ALARM).setContentTitle("Global College of Management").setContentText(message).setSmallIcon(pas.pranav.gcm.R.drawable.notification_icon).setAutoCancel(true).addAction(0, null, contentIntent).setContentIntent(contentIntent).setPriority(android.app.Notification.PRIORITY_HIGH).setVibrate(new long[]{1000, 1000, 1000, 1000}).build();
        NotificationManager notificationManager1 = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager1.notify(0, notification);


    }

}
