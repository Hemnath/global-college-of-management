package pas.pranav.gcm;

public interface Config {
    String YOUR_SERVER_URL = "http://27.111.21.62/global/api/app/device_id/";
    String GOOGLE_SENDER_ID = "690918842798";
    String TAG = "Notification";

    String DISPLAY_MESSAGE_ACTION = "pas.pranav.gcm.DISPLAY_MESSAGE";
    String EXTRA_MESSAGE = "message";
}
