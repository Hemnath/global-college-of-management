package pas.pranav.gcm;

import android.content.Intent;
import android.webkit.WebView;

/**
 * Created by dev on 10/30/15.
 */

public class Twitter extends android.support.v7.app.AppCompatActivity {
    String url = "https://twitter.com/GlobalCollegeNP";
    private android.webkit.WebView webView;

    @Override
    protected void onCreate(android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(pas.pranav.gcm.R.layout.twitter);

        webView = (android.webkit.WebView) findViewById(pas.pranav.gcm.R.id.wvTwitter);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setSupportZoom(true);
        webView.setWebViewClient(new android.webkit.WebViewClient());
        webView.loadUrl(url);

    }

    @Override
    public void onBackPressed() {
        if (webView.canGoBack()) {
            webView.goBack();
        } else {
            Intent launchNextActivity = new Intent(Twitter.this, MainActivity.class);
            launchNextActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            launchNextActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            launchNextActivity.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(launchNextActivity);
        }

    }
}
