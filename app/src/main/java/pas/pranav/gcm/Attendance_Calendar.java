package pas.pranav.gcm;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import android.content.SharedPreferences;

import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.text.SimpleDateFormat;

import pas.pranav.gcm.admin.AdminMainActivity;
import pas.pranav.gcm.helpers.API_URL;
import pas.pranav.gcm.helpers.JSONParserCustom;
import pas.pranav.gcm.teacher.TeacherMainActivity;

/**
 * Created by hem on 11/9/15.
 */

public class Attendance_Calendar extends AppCompatActivity {

    private CaldroidFragment caldroidFragment;
    private CaldroidFragment dialogCaldroidFragment;

    private ProgressDialog progressBar;
    private String url, deviceId, token,userType;

    public static final String strUserName = "userKey";
    public static final String strPassword = "passwordKey";
    public static final String strToken = "token";
    public static final String strDeviceId = "deviceId";

    SharedPreferences.Editor editor;
    SharedPreferences sharedpreferences;
    private API_URL appURL;
    private int intUserType;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.attendance_calendar);
        android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(pas.pranav.gcm.R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sharedpreferences = getSharedPreferences(MIS.MyPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();

        deviceId = sharedpreferences.getString(strDeviceId, null);
        token = sharedpreferences.getString(strToken, null);
        userType = sharedpreferences.getString("UserType", null);

        intUserType = Integer.parseInt(userType);

        final SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");

        caldroidFragment = new CaldroidFragment();

        if (savedInstanceState != null) {
            caldroidFragment.restoreStatesFromKey(savedInstanceState,
                    "CALDROID_SAVED_STATE");
        } else {
            Bundle args = new Bundle();
            Calendar cal = Calendar.getInstance();
            args.putInt(CaldroidFragment.MONTH, cal.get(Calendar.MONTH) + 1);
            args.putInt(CaldroidFragment.YEAR, cal.get(Calendar.YEAR));
            args.putBoolean(CaldroidFragment.ENABLE_SWIPE, true);
            args.putBoolean(CaldroidFragment.SIX_WEEKS_IN_CALENDAR, true);
            caldroidFragment.setArguments(args);
        }
        int userTyp=Integer.parseInt(userType);
        appURL =new API_URL();
        url="";
        if (userTyp == 1) {
            url = appURL.ServerUrlAdmin+appURL.Attendance;
        } else if (userTyp == 2) {
            url = appURL.ServerUrlStudent+appURL.Attendance;
            new JSONParse().execute();
        } else if (userTyp == 3) {
            url = appURL.ServerUrlParent+appURL.Attendance;
            new JSONParse().execute();
        } else if (userTyp == 4) {
            url = appURL.ServerUrlTeacher+appURL.Attendance;
            new JSONParse().execute();
        }

        // Attach to the activity
        FragmentTransaction t = getSupportFragmentManager().beginTransaction();
        t.replace(R.id.calendar1, caldroidFragment);
        t.commit();

        // Setup listener
        final CaldroidListener listener = new CaldroidListener() {

            @Override
            public void onSelectDate(Date date, View view) {
                Toast.makeText(getApplicationContext(), formatter.format(date),
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onChangeMonth(int month, int year) {
                String text = "Month: " + month + " Year: " + year;
                Toast.makeText(getApplicationContext(), text,
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLongClickDate(Date date, View view) {
                Toast.makeText(getApplicationContext(),
                        "Long click " + formatter.format(date),
                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCaldroidViewCreated() {
                if (caldroidFragment.getLeftArrowButton() != null) {
                    Toast.makeText(getApplicationContext(),
                            "Caldroid view is created", Toast.LENGTH_SHORT)
                            .show();
                }
            }

        };

        // Setup Caldroid
        caldroidFragment.setCaldroidListener(listener);

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            intent = new Intent(Attendance_Calendar.this, Loggedin_Mis.class);
            switch (intUserType) {

                case 1:
                    intent = new Intent(Attendance_Calendar.this, AdminMainActivity.class);
                    break;
                case 2:
                    intent = new Intent(Attendance_Calendar.this, Loggedin_Mis.class);
                    break;
                case 3:
                    intent = new Intent(Attendance_Calendar.this, Loggedin_Mis.class);
                    break;
                case 4:
                    intent = new Intent(Attendance_Calendar.this, TeacherMainActivity.class);
                    break;


            }
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    private void setCustomResourceForDates(String backgroundColor, String start, String title, String borderColor) {

        Toolbar toolbar = (Toolbar) findViewById(pas.pranav.gcm.R.id.toolbar);
        setSupportActionBar(toolbar);

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat.parse(start);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (caldroidFragment != null) {

            ColorDrawable bgColor = new ColorDrawable(getResources().getColor(R.color.red));

            if (title.equalsIgnoreCase("Present")) {
                bgColor = new ColorDrawable(getResources().getColor(R.color.present));
            } else if (title.equalsIgnoreCase("Absent")) {
                bgColor = new ColorDrawable(getResources().getColor(R.color.absent));
            } else if (title.equalsIgnoreCase("Leave")) {
                bgColor = new ColorDrawable(getResources().getColor(R.color.leave));
            }

            caldroidFragment.setBackgroundDrawableForDate(bgColor, convertedDate);
            caldroidFragment.setTextColorForDate(R.color.white, convertedDate);

        }
        caldroidFragment.refreshView();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        // TODO Auto-generated method stub
        super.onSaveInstanceState(outState);

        if (caldroidFragment != null) {
            caldroidFragment.saveStatesToKey(outState, "CALDROID_SAVED_STATE");
        }

        if (dialogCaldroidFragment != null) {
            dialogCaldroidFragment.saveStatesToKey(outState,
                    "DIALOG_CALDROID_SAVED_STATE");
        }
    }

    private class JSONParse extends android.os.AsyncTask<String, String, org.json.JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar = new ProgressDialog(Attendance_Calendar.this);
            progressBar.setMessage("Downloading...");
            progressBar.show();

        }

        @Override
        protected org.json.JSONObject doInBackground(String... args) {

            JSONParserCustom jParser = new JSONParserCustom();
            org.json.JSONObject json = jParser.getJSONFromUrl(url, deviceId, token);

            return json;
        }

        @Override
        protected void onPostExecute(org.json.JSONObject json) {
            String data = "";
            try {
                if (progressBar.isShowing()) {
                    progressBar.dismiss();
                }
                JSONArray jsonArray = json.optJSONArray("List");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    String backgroundColor = jsonObject.getString("backgroundColor");
                    String start = jsonObject.optString("start").toString();
                    String end = jsonObject.optString("end").toString();
                    String title = jsonObject.optString("title").toString();
                    String borderColor = jsonObject.optString("borderColor").toString();

                    setCustomResourceForDates(backgroundColor, start, title, borderColor);

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }
}
